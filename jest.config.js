module.exports = {
  projects: [
    '<rootDir>/apps/web',
    '<rootDir>/libs/environment',
    '<rootDir>/libs/auth',
    '<rootDir>/libs/react-shared',
    '<rootDir>/libs/types',
    '<rootDir>/libs/next-shared',
    '<rootDir>/apps/api',
    '<rootDir>/libs/data',
    '<rootDir>/libs/nest-utils',
    '<rootDir>/libs/types-shared',
    '<rootDir>/libs/shared-utils',
    '<rootDir>/libs/coinbase',
  ],
  verbose: true,
};
