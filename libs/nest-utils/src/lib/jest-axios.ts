import axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios';
import { isEmpty, isNil } from 'lodash';

export async function request(
  method: Method,
  endpoint: string,
  body?: any,
  headers?: {
    accessToken?: string;
  },
  contentType?: string
): Promise<AxiosResponse> {
  headers = headers ?? {};

  try {
    if (!isNil(headers?.accessToken)) {
      headers['Authorization'] = `Bearer ${headers.accessToken}`;
    }

    const config: AxiosRequestConfig = {
      method,
      url: endpoint,
      data: body,
      headers: headers ?? {},
    };

    return await axios(config);
  } catch (e) {
    const error = e?.response?.data?.message ?? e;
    throw error;
  }
}
