import * as requestIp from 'request-ip';

export const getClientIP = (req) => requestIp.getClientIp(req);
