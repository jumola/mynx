import axios from 'axios';
import { googleConfig } from '@mynx/environment';

export const getGoogleTokenFromCode = async (code) => {
  return new Promise((resolve, reject) => {
    axios
      .post(`https://oauth2.googleapis.com/token`, {
        client_id: googleConfig.clientId,
        client_secret: googleConfig.clientSecret,
        redirect_uri: googleConfig.redirectURL,
        grant_type: 'authorization_code',
        code,
      })
      .then((response) => resolve(response))
      .catch((error) => reject(error));
  });
};
