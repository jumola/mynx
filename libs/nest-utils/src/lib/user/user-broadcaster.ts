import { User } from '@mynx/data';

export const userBroadcaster = (
  gateway,
  user: User,
  eventName: string,
  payload: any
) => {
  const sockets = gateway.connectedUsers.get(String(user.id)) ?? [];
  for (const socketId of sockets.keys()) {
    gateway.server.to(socketId).emit(eventName, payload);
  }
};
