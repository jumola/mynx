import axios from 'axios';
import { GoogleAuthUser } from '@mynx/types-shared';

export const getGoogleInfo = async (accessToken): Promise<GoogleAuthUser> => {
  const { data } = await axios({
    url: 'https://www.googleapis.com/oauth2/v2/userinfo',
    method: 'get',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
  return data as GoogleAuthUser;
};
