import { googleConfig } from '@mynx/environment';
import queryString from 'query-string';

export const getGoogleLoginURL = (): string => {
  const stringifiedParams = queryString.stringify({
    client_id: googleConfig.clientId,
    redirect_uri: googleConfig.redirectURL,
    scope: [
      'https://www.googleapis.com/auth/userinfo.email',
      'https://www.googleapis.com/auth/userinfo.profile',
    ].join(' '), // space seperated string
    response_type: 'code',
    access_type: 'offline',
    prompt: 'consent',
  });

  return `https://accounts.google.com/o/oauth2/v2/auth?${stringifiedParams}`;
};
