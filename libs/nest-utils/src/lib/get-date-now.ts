import { DateTime } from 'luxon';

export const getDateNow = () => {
  return DateTime.local().toJSDate();
};
