export interface AdminDashboardResponse {
  coins: number;
  games: number;
  users: number;
  winners: number;
}
