export interface CoinbaseNewAddress {
  id: string;
  address: string;
  name: string;
  created_at: string;
  updated_at: string;
  network: string;
  resource: string;
  resource_path: string;
}

export interface CoinbaseNewAddressResponseData {
  data: CoinbaseNewAddress;
}

export interface CoinbaseNewAddressResponse {
  data: CoinbaseNewAddressResponseData;
}
