export interface CoinbaseSpotPrice {
  base: string;
  currency: string;
  amount: string;
}

export interface CoinbaseSpotPriceResponseData {
  data: CoinbaseSpotPrice;
}

export interface CoinbaseSpotPriceResponse {
  data: CoinbaseSpotPriceResponseData;
}
