import { coinbaseRequest } from './coinbase-request';
import { Coin } from '@mynx/data';

// doc: https://developers.coinbase.com/api/v2#show-a-transaction
// GET https://api.coinbase.com/v2/accounts/:account_id/transactions/:transaction_id
export const coinbaseGetTransaction = async (coin: Coin, transactionId) => {
  const path = `${coin.coinbaseResourcePath}/transactions/${transactionId}`;
  const response = (await coinbaseRequest('GET', path)) as any;
  return response?.data?.data || undefined;
};

// doc: https://developers.coinbase.com/api/v2?javascript#list-notifications
// GET https://api.coinbase.com/v2/notifications
export const coinbaseNotificationsList = async (nextPath?) => {
  const path = nextPath || `/v2/notifications`;
  const response = (await coinbaseRequest('GET', path)) as any;
  return response?.data || undefined;
};
