import { coinbaseRequest } from './coinbase-request';
import { Coin } from '@mynx/data';
import { CoinbaseNewAddressResponse } from '@mynx/types';

export const coinbaseCreateAddress = async (coin: Coin) => {
  const path = `${coin.coinbaseResourcePath}/addresses`;
  const response = (await coinbaseRequest(
    'POST',
    path,
    {}
  )) as CoinbaseNewAddressResponse;
  const result = response?.data?.data || undefined;
  return result;
};
