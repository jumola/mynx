import axios from 'axios';
import { coinbaseConfig } from '@mynx/environment';
import { isEmpty } from 'lodash';
import * as CryptoJS from 'crypto-js';

export const coinbaseRequest = async (method, path, params = {}) => {
  return await new Promise((resolve, reject) => {
    const timestamp = Math.floor(Date.now() / 1000);
    const message =
      timestamp +
      method +
      path +
      (method === 'GET' && isEmpty(params) ? '' : JSON.stringify(params));

    const signature = CryptoJS.HmacSHA256(
      message,
      coinbaseConfig.secret
    ).toString(CryptoJS.enc.Hex);

    // Set config defaults when creating the instance
    const instance = axios.create({
      baseURL: coinbaseConfig.URL,
    });

    // Alter defaults after instance has been created
    instance.defaults.headers.common['Authorization'] =
      'Bearer abd90df5f27a7b170cd775abf89d632b350b7c1c9d53e08b340cd9832ce52c2c';
    instance.defaults.headers.common['CB-ACCESS-SIGN'] = signature;
    instance.defaults.headers.common['CB-ACCESS-TIMESTAMP'] = timestamp;
    instance.defaults.headers.common['CB-ACCESS-KEY'] = coinbaseConfig.key;
    instance.defaults.headers.common['CB-VERSION'] = '2018-07-11';

    if (method === 'GET') {
      instance
        .get(path)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    } else if (method === 'POST') {
      console.log('path, params', path, params);
      instance
        .post(path, params)
        .then((response) => resolve(response))
        .catch((error) => reject(error));
    }
  });
};
