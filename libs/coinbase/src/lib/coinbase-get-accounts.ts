import { coinbaseRequest } from './coinbase-request';

// doc: https://developers.coinbase.com/api/v2#list-accounts
// GET https://api.coinbase.com/v2/accounts
export const coinbaseGetAccounts = async (nextPath?) => {
  const path = nextPath || `/v2/accounts`;
  const response = (await coinbaseRequest('GET', path)) as any;
  return response?.data || undefined;
};
