import { coinbaseRequest } from './coinbase-request';
import BigNumber from 'bignumber.js';
import { Coin, User } from '@mynx/data';
import { convertCryptoToUnit } from '@mynx/shared-utils';
import { app } from '@mynx/environment';

export const coinbaseSend = async (
  user: User,
  coin: Coin,
  amount: BigNumber
) => {
  const path = `${coin.coinbaseResourcePath}/transactions`;
  const description = `${convertCryptoToUnit(coin, amount)?.toFormat()} ${
    coin.unitName
  } from ${app.appName}`;

  const decimals: string = coin.unitDecimal.toFixed().split('.')[1];
  const decimalPlaces = decimals.split('')?.length;

  const params = {
    type: 'send',
    to: 'myelvenus@gmail.com',
    amount: amount.toFixed(decimalPlaces),
    currency: coin.name,
    description,
  };

  const response = (await coinbaseRequest('POST', path, params)) as any;

  const result = response?.data?.data || undefined;
  return result;
};
