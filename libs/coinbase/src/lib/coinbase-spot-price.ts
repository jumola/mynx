import { Coin } from '@mynx/data';
import { CoinbaseSpotPriceResponse } from '@mynx/types';
import { coinbaseRequest } from './coinbase-request';

export const coinbaseSpotPrice = async (coin: Coin) => {
  const path = `/v2/prices/${coin.name}-USD/spot`;
  const response = (await coinbaseRequest(
    'GET',
    path
  )) as CoinbaseSpotPriceResponse;
  const result = response?.data?.data || undefined;
  return result;
};
