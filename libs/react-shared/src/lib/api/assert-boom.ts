import boom from 'boom';

export type boomTypes =
  | 'badRequest'
  | 'badImplementation'
  | 'unauthorized'
  | 'forbidden'
  | 'notFound'
  | 'expectationFailed'
  | 'internal';

/**
 * check the provided predicate and if it is false, throw a bad request Boom exception.
 */
function assertBoom(
  predicate: any,
  message: string,
  func: boomTypes = 'badRequest'
) {
  if (!predicate) {
    throw boom[func](message);
  }
}

export { assertBoom };
