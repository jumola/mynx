export const getApiErrorMessage = async (error: any) => {
  return (
    JSON.parse(await error?.response?.text())?.message ??
    'An error has occurred'
  );
};
