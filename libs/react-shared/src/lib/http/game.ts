import { useQuery } from 'react-query';
import { httpRequest } from './api';
import { Game } from '@mynx/data';

const fetchGame = async (id: number) =>
  id ? ((await httpRequest.get(`game/${id}`).json()) as Game) : undefined;

export const useGame = (id: number) =>
  useQuery(['game', id], async () => fetchGame(id));

export const useGames = () => {
  return useQuery(
    'games',
    async () => (await httpRequest.get(`game`).json()) as Game[]
  );
};
