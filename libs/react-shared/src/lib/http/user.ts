import { useQuery } from 'react-query';
import { Bank, Match, Player, Transaction, User } from '@mynx/data';
import { httpRequest } from './api';
import { PaginationMeta } from '@mynx/types-shared';

export const fetchWithdraw = async (bankId: number) => {
  return (await httpRequest.post(`me/withdraw/${bankId}`).json()) as Bank;
};

export const fetchUser = async () =>
  (await httpRequest.post('auth/check-user').json()) as User;

export const fetchMe = async () => (await httpRequest.get('me').json()) as User;

export const fetchMeClaimBonus = async () =>
  await httpRequest.post('me/claim-welcome-bonus')?.json();

export const fetchMeFaucetBonus = async () =>
  await httpRequest.post('me/claim-faucet-bonus')?.json();

export const fetchMeFavoriteBank = async (coinId: number): Promise<void> => {
  await httpRequest.post(`me/favorite-bank/${coinId}`);
};

export const useCheckUser = () => useQuery('user-check', async () => fetchUser);

export const useUser = () => {
  return useQuery('user', async () => {
    return (await httpRequest.get('me').json()) as User;
  });
};

export const useMyTransactions = (page: number) => {
  return useQuery(
    'transactions',
    async () =>
      (await httpRequest
        .get('me/transactions', {
          searchParams: {
            page,
          },
        })
        .json()) as {
        items: Transaction[];
        meta: PaginationMeta;
      }
  );
};

export const useMyBankDepositAddress = (coinId: number) =>
  useQuery(
    ['me', 'transactions', coinId],
    async () =>
      (await httpRequest
        .get(`me/deposit-address/${coinId}`)
        .json()) as Transaction
  );

export const useMyPlays = () =>
  useQuery(
    ['me', 'plays'],
    async () => (await httpRequest.get(`me/plays`).json()) as Player[]
  );

export const useMyLiveMatches = () =>
  useQuery(
    ['me', 'live-matches'],
    async () => (await httpRequest.get('me/live-matches').json()) as Match[]
  );
