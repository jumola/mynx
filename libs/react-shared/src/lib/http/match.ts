import { useQuery } from 'react-query';
import { httpRequest } from './api';
import { Match } from '@mynx/data';

const fetchMatch = async (id: number) =>
  id ? ((await httpRequest.get(`match/${id}`).json()) as Match) : undefined;

const fetchLatestWinners = async () =>
  (await httpRequest.get(`match/latest-winners`).json()) as Match[];

export const useMatch = (id: number) =>
  useQuery(['match', id], async () => fetchMatch(id));

export const useLatestWinners = () =>
  useQuery(['match', 'latest-winners'], fetchLatestWinners);
