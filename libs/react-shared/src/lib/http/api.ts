import ky, { AfterResponseHook, BeforeRequestHook } from 'ky';
import { app } from '@mynx/environment';
import { storeRetrieve } from '../services/storage-service';
import { isNil } from 'lodash';

// const afterResponse: AfterResponseHook = (request, options, response) => {
//   if (response.status === 401) router.push("/login");
// };

const beforeRequest: BeforeRequestHook = (request) => {
  const data = storeRetrieve('user');

  const header = request.headers.get('Authorization');
  if (!header?.startsWith('Basic') && !isNil(data?.accessToken)) {
    request.headers.append('Authorization', `Bearer ${data?.accessToken}`);
  }
};

export const httpRequest = ky.create({
  prefixUrl: app.secureApiBaseUrl,
  hooks: {
    // afterResponse: [afterResponse],
    beforeRequest: [beforeRequest],
  },
});
