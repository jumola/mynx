import { User } from '@mynx/data';
import ky from 'ky';
import { app } from '@mynx/environment';
import { fetchMe } from './user';
import { storeSave } from '../services/storage-service';

export const fetchLogin = async (
  json: Partial<User>
): Promise<{ accessToken: string; user: User }> => {
  const { accessToken } = (await ky
    .post(`${app.secureApiBaseUrl}/auth/login`, { json })
    .json()) as { accessToken: string };

  storeSave('user', { accessToken });

  const user = await fetchMe();

  return {
    accessToken,
    user,
  };
};

export const fetchRegister = async (
  json: Partial<User>
): Promise<{ accessToken: string; user: User }> => {
  const { accessToken } = (await ky
    .post(`${app.secureApiBaseUrl}/auth/register`, { json })
    .json()) as { accessToken: string };

  storeSave('user', { accessToken });

  const user = await fetchMe();

  return {
    accessToken,
    user,
  };
};
