import { Player, Transaction, User } from '@mynx/data';
import { AdminDashboardResponse } from '@mynx/types';
import { PaginationMeta } from '@mynx/types-shared';
import { useQuery } from 'react-query';
import { httpRequest } from './api';

export const fetchAdminCreate = async (tableName: string, json: any) =>
  (await httpRequest.post(tableName, { json }).json()) as any;

export const fetchAdminUpdate = async (
  tableName: string,
  id: number,
  json: any
) => (await httpRequest.put(`${tableName}/${id}`, { json }).json()) as any;

export const useAdminDashboard = () => {
  return useQuery('admin-dashboard', async () => {
    return (await httpRequest
      .get('admin/dashboard')
      .json()) as AdminDashboardResponse;
  });
};

export const useAdminTable = (tableName: string) =>
  useQuery(['admin-dashboard', tableName], async () => {
    return (await httpRequest.get(tableName).json()) as any;
  });

export const useAdminDocument = (tableName: string, id: number) => {
  return useQuery(['admin', tableName, id], async () => {
    return (await httpRequest.get(`${tableName}/${id}`).json()) as any;
  });
};

export const useAdminCashouts = () => {
  return useQuery(['admin', 'cashouts'], async () => {
    return (await httpRequest.get('admin/cashouts').json()) as {
      items: Transaction[];
      meta: PaginationMeta;
    };
  });
};

export const fetchAdminUserStats = async (userId: number) => {
  return (await httpRequest.get(`admin/user-stats/${userId}`).json()) as {
    sameIP: User[];
    samePass: User[];
    plays: Player[];
    withdrawals: Transaction[];
    deposits: Transaction[];
    faucets: Transaction[];
    referrals: User[];
  };
};
