import { useQuery } from 'react-query';
import { httpRequest } from './api';
import { Transaction } from '@mynx/data';

export const useTransactions = () => {
  return useQuery(
    'transactions',
    async () => (await httpRequest.get('transaction').json()) as Transaction[]
  );
};

export const useWithdrawals = () => {
  return useQuery(
    ['transactions', 'withdrawals'],
    async () =>
      (await httpRequest.get('transaction/withdrawals').json()) as Transaction[]
  );
};
