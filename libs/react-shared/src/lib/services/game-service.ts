import { useEffect, useState } from 'react';
import io, { Socket } from 'socket.io-client';
import { app } from '@mynx/environment';

export const useGameSocket = (accessToken: string) => {
  const [socketClient, setSocketClient] = useState<Socket | null>(null);
  const [socketError, setSocketError] = useState<string | undefined>(undefined);

  useEffect(() => {
    const socket = io(app.gameGateway, {
      transports: ['websocket'],
      reconnectionDelay: 1000,
      reconnection: true,
      query: {
        token: accessToken,
      },
    });

    socket.on('error', (message) => setSocketError(message));

    setSocketClient(socket);

    return () => {
      socket.disconnect();
    };
  }, [accessToken]);

  return { socket: socketClient, socketError };
};
