import { toast, ToastOptions } from 'react-toastify';

const notifySharedSettings: ToastOptions = {
  position: 'top-center',
  autoClose: 3000,
  hideProgressBar: true,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: false,
  progress: undefined,
};

export const alertSuccess = (message: string) =>
  toast.success(message, notifySharedSettings);

export const alertError = (message: string) =>
  toast.error(message, notifySharedSettings);
