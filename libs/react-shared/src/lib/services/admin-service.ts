import { app } from '@mynx/environment';
import ky from 'ky';
import { useQuery } from 'react-query';
import { find } from 'lodash';
import { Coin } from '@mynx/data';

export const useCoin = (id: number) => {
  const { data: items } = useCoins();
  const item = find(items, (d: Coin) => d.id === id);
  if (!item) {
    return undefined;
  }
  return item;
};

export const useCoins = () =>
  useQuery('coins', async () => {
    const endpoint = `${app.apiBaseUrl}/coin`;
    return (await ky.get(endpoint, {}).json()) as Coin[];
  });

export const createCoin = async (body: Partial<Coin>) => {
  const endpoint = `${app.apiBaseUrl}/coin`;
  return await ky.post(endpoint, { json: body }).json();
};

export const updateCoin = async (id: number, body: Partial<Coin>) => {
  const endpoint = `${app.apiBaseUrl}/coin/${id}`;
  return await ky.put(endpoint, { json: body }).json();
};
