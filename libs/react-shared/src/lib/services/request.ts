import { app } from '@mynx/environment';
import ky from 'ky';

const SESSION_EXPIRED_STATUS_CODE = 401;

const baseApiClient = ky.create({
  headers: {
    rainbow: 'rainbow',
    unicorn: 'unicorn',
  },
  prefixUrl: app.secureApiBaseUrl,
});

const apiClient = ({ ...options }) => {
  const onSuccess = (response) => response;

  const onError = (error) => {
    if (error.response.status === SESSION_EXPIRED_STATUS_CODE) {
      // Navigate to Login screen
    }
    return Promise.reject(error);
  };

  return baseApiClient(options).then(onSuccess).catch(onError);
};

export default apiClient;
