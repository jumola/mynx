import { User } from '@mynx/data';
import { fetchMe } from '../http/user';
import { isNil, once } from 'lodash';
import create from 'zustand';
import { combine } from 'zustand/middleware';
import { fetchLogin, fetchRegister } from '../http/auth';
import { storeRemove, storeRetrieve, storeSave } from './storage-service';

export const useAuth = create(
  combine(
    {
      user: undefined as User | undefined,
      isLoading: true,
      isLoaded: false,
      accessToken: undefined as string | undefined,
    },
    (set, get) => ({
      /**
       * Load saved data from logic storage, make
       * sure this function is called during the app
       * startup phase
       */
      init: once(async () => {
        const data = storeRetrieve('user');
        if (isNil(data?.accessToken) || isNil(data?.user)) {
          // no data
          set({ isLoading: false, isLoaded: true });
        } else {
          set({
            isLoading: false,
            isLoaded: true,
            user: data.user,
            accessToken: data.accessToken,
          });
        }
        get();
      }),

      signIn: async (body: Partial<User>) => {
        try {
          const { accessToken, user: userData } = await fetchLogin(body);

          if (userData) {
            set({ user: userData, accessToken });
            storeSave('user', {
              accessToken,
              user: userData,
            });
            return userData;
          } else {
            storeRemove('user');
            throw new Error('Invalid credentials');
          }
        } catch (error) {
          await storeRemove('user');
          throw error;
        }
      },

      register: async (body: Partial<User>) => {
        try {
          const { accessToken, user: userData } = await fetchRegister(body);

          set({ user: userData, accessToken });

          storeSave('user', {
            accessToken,
            user: userData,
          });

          return userData;
        } catch (error) {
          await storeRemove('user');
          throw error;
        }
      },

      refetch: async () => {
        const current = get();

        set({
          user: current.user,
          accessToken: current.accessToken,
          isLoading: false,
          isLoaded: true,
        });

        const user = await fetchMe();

        set({
          user: user ?? current.user,
          accessToken: current.accessToken,
          isLoading: false,
          isLoaded: true,
        });

        storeSave('user', {
          user,
          accessToken: current.accessToken,
        });
        return user;
      },

      signOut: async () => {
        set({ user: undefined });
        storeRemove('user');
      },
    })
  )
);
