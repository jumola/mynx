import { isNil } from 'lodash';
import { useEffect, useState } from 'react';

export default function useStickyState(key: string, defaultValue: any = null) {
  const [value, setValue] = useState(defaultValue);

  // this updates the state
  useEffect(() => {
    // get the current stored value for this key
    const stickyValue = window.localStorage.getItem(key);
    // if not null, update state
    if (!isNil(stickyValue)) {
      setValue(JSON.parse(stickyValue));
    }
  }, [key]);

  // takes care of saving to local storage if state is updated
  useEffect(() => {
    // const toSave = isNil(value) ? value :
    if (isNil(value)) {
      window.localStorage.removeItem(key);
    } else {
      window.localStorage.setItem(key, JSON.stringify(value));
    }
  }, [key, value]);

  return [
    value,
    setValue,
    () => {
      window.localStorage.removeItem(key);
    },
  ];
}

export const storeRetrieve = (key: string) => {
  const data = window.localStorage.getItem(key);
  return data ? JSON.parse(data) : undefined;
};

export const storeSave = (key: string, value: any) => {
  console.log('storeSave', key, value);
  if (value) {
    window.localStorage.setItem(key, JSON.stringify(value));
  }
};

export const storeRemove = (key: string) => {
  window?.localStorage?.removeItem(key);
};
