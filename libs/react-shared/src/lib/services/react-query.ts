import { QueryClient } from 'react-query';
import { persistQueryClient } from 'react-query/persistQueryClient-experimental';
import { createAsyncStoragePersistor } from 'react-query/createAsyncStoragePersistor-experimental';
import { get, set, del } from 'idb-keyval';
import prettyBytes from 'pretty-bytes';
import { debounce } from 'lodash';

// Create a client

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      cacheTime: 1000 * 60 * 60 * 24, // 24 hours
    },
  },
});

// https://react-query.tanstack.com/plugins/createAsyncStoragePersistor
const myAsyncStoragePersistor = createAsyncStoragePersistor({
  storage: {
    getItem: async (key) => {
      const value = await get(key);
      console.log(
        // assuming one character = 1 byte which may vary a lot, especially with Chinese text
        `get ${key}, retrieved ${prettyBytes(
          typeof value === 'string' ? value.length : 0
        )}`
      );
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return value as any;
    },
    setItem: debounce(
      async (key, value) => {
        const startAt = Date.now();
        await set(key, value);
        const finishAt = Date.now();
        console.log(
          `saved ${key} (${prettyBytes(
            typeof value === 'string' ? value.length : 0
          )}) (${finishAt - startAt} ms)`
        );
      },
      // TODO: determine if this is needed, try to reduce logging in any case for now
      5 * 1000
    ) as any,
    removeItem: async (key) => {
      await del(key);
      console.log(`delete ${key}`);
    },
  },
});

persistQueryClient({
  queryClient,
  persistor: myAsyncStoragePersistor,
  // https://react-query.tanstack.com/plugins/persistQueryClient#cache-busting
  // bump this number if the cache becomes incompatible:
  buster: '2',
});

export { queryClient };
