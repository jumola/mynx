import { useQuery } from 'react-query';
import { Coin } from '@mynx/data';
import { httpRequest } from '../http/api';

export const useCoin = (id: number) => {
  return useQuery(
    ['coin', id],
    async () => (await httpRequest.get(`coin/${id}`).json()) as Coin
  );
};

export const useCoins = () =>
  useQuery('coins', async () => {
    return (await httpRequest.get('coin', {}).json()) as Coin[];
  });

export const createCoin = async (body: Partial<Coin>) => {
  return await httpRequest.post('coin', { json: body }).json();
};

export const updateCoin = async (id: number, body: Partial<Coin>) => {
  return await httpRequest.put(`coin/${id}`, { json: body }).json();
};
