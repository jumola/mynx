export const Input = (props: {
  name: string;
  displayName?: string;
  type?: string;
  placeholder: string;
  required?: boolean;
  hookRegister?: any;
  value?: any;
  onChange?: any;
}) => {
  const {
    name,
    displayName,
    type,
    placeholder,
    hookRegister,
    required,
    value,
    onChange,
  } = props;

  return (
    <div className="mt-2 text-left">
      {!displayName ? null : <p>{displayName}</p>}

      <input
        name={name}
        type={type ?? 'text'}
        className="form-control text-left"
        placeholder={placeholder ?? ''}
        required={Boolean(required)}
        {...hookRegister}
        value={value}
        onChange={onChange ?? (() => null)}
      />
    </div>
  );
};
