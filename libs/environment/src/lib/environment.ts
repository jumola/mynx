import BigNumber from 'bignumber.js';

export const databaseConfig = {
  type: 'mysql',
  host: '127.0.0.1',
  port: 3306,
  username: 'root',
  password: '',
  database: 'mynx',
};

export const googleConfig = {
  clientId:
    '1018631965972-qu7upi69ahi1q9h09ulidnfg1edc731l.apps.googleusercontent.com',
  clientSecret: 'kdThpCQ2_Ern5THeqTRUeywZ',
  redirectURL: 'http://localhost:3333/api/auth/googled',
};

export const nxAuthConfig = {
  debug: false,
  secret: 'someSecret123',
  jwt: {
    signingKey: 'someSecret123',

    // You can also specify a public key for verification if using public/private key (but private only is fine)
    // verificationKey: process.env.JWT_SIGNING_PUBLIC_KEY,

    // If you want to use some key format other than HS512 you can specify custom options to use
    // when verifying (note: verificationOptions should include a value for maxTokenAge as well).
    // verificationOptions = {
    //   maxTokenAge: `${maxAge}s`, // e.g. `${30 * 24 * 60 * 60}s` = 30 days
    //   algorithms: ['HS512']
    // },
  },
};

export const app = {
  appName: 'malbux',
  clientUrl: 'http://localhost:4200',
  apiBaseUrl: 'http://localhost:4200/api',
  secureApiBaseUrl: 'http://localhost:3333/api',

  // gateways
  gameGateway: 'ws://localhost:3333/game',
  userGateway: 'ws://localhost:3333/me',
};

export const authConfig = {
  jwtSecret: '12345',
  cookieSecret: '12345',
};

export const coinbaseConfig = {
  key: 'CI20LFnVzjI0apVv',
  secret: 'PXo6f6sMBRqWZX06oEnivoITO8eBd8RR',
  URL: 'https://api.coinbase.com',
};

export const appMasterConfig = {
  upgradeCostUSD: new BigNumber(10),
  // user needs to refer 5 VIPs to break even
  vipROIRate: new BigNumber(0.2),
  regVsVipRewardRate: new BigNumber(0.1),
  regReferralBonusUSD: new BigNumber(0.01),
  lowestGameBuyInUSD: new BigNumber(0.001),
  rakeRate: new BigNumber(0.1),
  playerNumbers: [2, 3, 5, 10],
  welcomeBonusCoinCount: 4,
  welcomeBonusUsd: new BigNumber(1),
  claimFaucetBonusInterval: 4 * 60 * 60,
  claimFaucetBonusIntervalVip: 1 * 60 * 60,
};

export const mailerConfig = {
  transport: {
    host: 'smtp.mailtrap.io',
    secure: false,
    auth: {
      user: '4a6caf47c2b5d4',
      pass: '45d3e501efac1c',
    },
  },
};
