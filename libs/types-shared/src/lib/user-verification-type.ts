export enum UserVerificationType {
  AccountVerification = 'account-verification',
  PasswordReset = 'password-reset',
}
