export enum MailQueueType {
  AccountVerified = 'account-verified',
  NewPassword = 'new-password',
  PasswordReset = 'password-reset',
  WalletUpdate = 'wallet-update',
  Welcome = 'welcome',
}
