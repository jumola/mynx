export enum MatchStatus {
  // 4 - when a winner is determined
  Completed = 'completed',
  // 3 - when the requierd player count is reached and will be put on queue to be completed
  Concluding = 'concluding',
  // 2 - when the required player count is reached and it's now waiting for cron to complete it
  Ongoing = 'ongoing',
  // 1 - when the match is still waiting to complete the player count required
  Waiting = 'waiting',
}

export const MatchStatusText = {
  [MatchStatus.Completed]: 'Completed',
  [MatchStatus.Concluding]: 'Concluding',
  [MatchStatus.Waiting]: 'Waiting',
};
