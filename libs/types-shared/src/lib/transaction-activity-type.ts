export enum TransactionActivityType {
  AccountUpgrade = 'accountUpgrade',
  BuyIn = 'buyIn',
  Deposit = 'deposit',
  Faucet = 'faucet',
  Manual = 'manual',
  Open = 'open',
  Referral = 'referral',
  Refund = 'refund',
  WelcomeBonus = 'welcomeBonus',
  Winning = 'winning',
  Withdrawal = 'withdrawal',
}

export const TransactionActivityTypeText = {
  [TransactionActivityType.AccountUpgrade]: 'Account Upgrade',
  [TransactionActivityType.BuyIn]: 'Buy-In',
  [TransactionActivityType.Deposit]: 'Deposit',
  [TransactionActivityType.Faucet]: 'Faucet',
  [TransactionActivityType.Manual]: 'Manual',
  [TransactionActivityType.Open]: 'Open',
  [TransactionActivityType.Referral]: 'User Referral',
  [TransactionActivityType.Refund]: 'Refund',
  [TransactionActivityType.WelcomeBonus]: 'Welcome Bonus',
  [TransactionActivityType.Winning]: 'Game Winning',
  [TransactionActivityType.Withdrawal]: 'Withdrawal',
};
