export enum MailQueueStatus {
  Failed = 'failed',
  Queued = 'queued',
  Sent = 'sent',
}
