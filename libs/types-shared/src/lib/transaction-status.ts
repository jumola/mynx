export enum TransactionStatus {
  Canceled = 'canceled',
  Completed = 'completed',
  Exchanging = 'exchanging',
  Processing = 'processing',
  Rejected = 'rejected',
  Waiting = 'waiting',
}

export const TransactionStatusText = {
  [TransactionStatus.Canceled]: 'Canceled',
  [TransactionStatus.Completed]: 'Completed',
  [TransactionStatus.Exchanging]: 'Exchanging',
  [TransactionStatus.Processing]: 'Processing',
  [TransactionStatus.Rejected]: 'Rejected',
  [TransactionStatus.Waiting]: 'Waiting',
};
