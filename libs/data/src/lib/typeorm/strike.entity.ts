import { User } from './user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Strike {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  ip!: string;

  @Column({ nullable: true })
  url!: string;

  @Column({ nullable: true })
  reason!: string;

  // relationships
  @ManyToOne(() => User, (user) => user.strikes, { primary: true })
  user?: User;
  @Column({ nullable: false })
  userId!: number;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
