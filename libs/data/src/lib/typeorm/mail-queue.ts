import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { MailQueueStatus, MailQueueType } from '@mynx/types-shared';
import { ObjectJsonTransformer } from '../typeorm-shared/object-json';

@Entity()
export class MailQueue {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: MailQueueType,
    nullable: false,
  })
  type!: MailQueueType;

  @Column({
    type: 'enum',
    enum: MailQueueStatus,
    default: MailQueueStatus.Queued,
  })
  status!: MailQueueStatus;

  @Column({ nullable: false })
  to!: string;

  @Column({ nullable: false })
  subject!: string;

  @Column({ nullable: false })
  template!: string;

  @Column({
    type: 'longtext',
    nullable: false,
    transformer: ObjectJsonTransformer(),
  })
  context!: Record<string, unknown>;

  @Column({
    type: 'longtext',
    nullable: true,
  })
  notes!: string;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
