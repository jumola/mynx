import { Match } from './match.entity';
import { User } from './user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Player {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  isWinner?: boolean;

  // relationships
  @ManyToOne(() => User, (user) => user.plays)
  user?: User;
  @Column({ nullable: false })
  userId!: number;

  @ManyToOne(() => Match, (match) => match.players, {
    primary: true,
    onDelete: 'CASCADE',
  })
  match?: Match;
  @Column({ nullable: false })
  matchId!: number;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
