import { MatchStatus } from '@mynx/types-shared';
import BigNumber from 'bignumber.js';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Game } from './game.entity';
import { Player } from './player.entity';
import { User } from './user.entity';

@Entity()
export class Match {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  concludedAt!: Date;

  @Column({
    type: 'enum',
    enum: MatchStatus,
    default: MatchStatus.Waiting,
  })
  status!: MatchStatus;

  // non db properties
  socketId?: string;

  // relationships
  @ManyToOne(() => Game, (game) => game.matches, {
    primary: true,
  })
  game?: Game;
  @Column({ nullable: false })
  gameId!: number;

  @ManyToOne(() => User, (user) => user.wonMatches, {
    primary: true,
  })
  winner?: User;
  @Column({ nullable: true })
  winnerId!: number;

  @OneToMany(() => Player, (entity) => entity.match, { onDelete: 'CASCADE' })
  players!: Player[];

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
