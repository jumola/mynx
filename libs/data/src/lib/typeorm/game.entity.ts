import { Match } from './match.entity';
import BigNumber from 'bignumber.js';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BigNumberTransformer } from '../typeorm-shared/big-number';
import { Coin } from './coin.entity';

@Entity()
export class Game {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  name?: string;

  @Column({ nullable: false })
  players!: number;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('costAmount'),
    nullable: true,
  })
  costAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('prizeAmount'),
    nullable: true,
  })
  prizeAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('potAmount'),
    nullable: true,
  })
  potAmount!: BigNumber;

  // relationships
  @ManyToOne(() => Coin, (coin) => coin.games, { primary: true })
  coin?: Coin;
  @Column({ nullable: false })
  coinId!: number;

  @OneToMany(() => Match, (entity) => entity.game)
  matches: Match[];

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
