import { Coin } from './coin.entity';
import { User } from './user.entity';
import { BigNumberTransformer } from '../typeorm-shared/big-number';
import BigNumber from 'bignumber.js';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bank {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('amount'),
    nullable: false,
  })
  amount: BigNumber;

  @Column({ default: false })
  isFavorite: boolean;

  // relationships
  @ManyToOne(() => User, (user) => user.banks, { primary: true })
  user?: User;
  @Column({ nullable: false })
  userId!: number;

  @ManyToOne(() => Coin, (coin) => coin.banks, { primary: true })
  coin?: Coin;
  @Column({ nullable: false })
  coinId!: number;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
