import BigNumber from 'bignumber.js';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BigNumberTransformer } from '../typeorm-shared/big-number';
import { Bank } from './bank.entity';
import { Game } from './game.entity';
import { Transaction } from './transaction.entity';

@Entity()
export class Coin {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  longName: string;

  @Column({ nullable: true })
  unitName?: string;

  @Column({ nullable: true })
  coinbaseAccountId?: string;

  @Column({ nullable: true })
  addressRegex?: string;

  @Column({ nullable: true })
  color?: string;

  @Column({ nullable: true })
  coinbaseAssetId?: string;

  @Column({ nullable: true })
  icon?: string;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('unitDecimal'),
    nullable: true,
  })
  unitDecimal!: BigNumber;

  @Column({ default: 0 })
  exponent!: number;

  @Column({ default: false })
  isActive: boolean;

  @Column({ default: false })
  isFeatured: boolean;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('priceUSD'),
    nullable: true,
  })
  priceUSD!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('costToVipAmount'),
    nullable: true,
  })
  costToVipAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('bonusReferVipToVipAmount'),
    nullable: true,
  })
  bonusReferVipToVipAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('bonusReferVipToRegAmount'),
    nullable: true,
  })
  bonusReferVipToRegAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('bonusReferRegToVipAmount'),
    nullable: true,
  })
  bonusReferRegToVipAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('bonusReferRegToRegAmount'),
    nullable: true,
  })
  bonusReferRegToRegAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('welcomeBonusAmount'),
    nullable: true,
  })
  welcomeBonusAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('withdrawalMinimumAmount'),
    nullable: true,
  })
  withdrawalMinimumAmount!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('faucetAmountVip'),
    nullable: true,
  })
  faucetAmountVip!: BigNumber;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('faucetAmountReg'),
    nullable: true,
  })
  faucetAmountReg!: BigNumber;

  @Column({ nullable: true })
  coinbaseResourcePath?: string;

  // relationships
  @OneToMany(() => Game, (game) => game.coin)
  games: Game[];

  @OneToMany(() => Bank, (bank) => bank.coin)
  banks!: Bank[];

  @OneToMany(() => Transaction, (transaction) => transaction.coin)
  transactions!: Transaction[];

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
