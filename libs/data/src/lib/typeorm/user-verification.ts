import { User } from './user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserVerificationType } from '@mynx/types-shared';

@Entity()
export class UserVerification {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  secret!: string;

  @Column({ default: false })
  isUsed!: boolean;

  @Column({
    type: 'enum',
    enum: UserVerificationType,
    nullable: false,
  })
  type!: UserVerificationType;

  // relationships
  @ManyToOne(() => User, (user) => user)
  user?: User;
  @Column({ nullable: true })
  userId!: number;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
