import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity()
export class UserReferral {
  @PrimaryGeneratedColumn()
  id: number;

  // sender
  @ManyToOne(() => User, (user) => user.id)
  sender: User;
  @Column({ nullable: false })
  senderId!: number;

  // receiver
  @ManyToOne(() => User, (user) => user.id)
  receiver: User;
  @Column({ nullable: false })
  receiverId!: number;

  @Column({ default: false })
  isRewarded: boolean;

  @Column({ nullable: true })
  rewardedAt!: Date;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
