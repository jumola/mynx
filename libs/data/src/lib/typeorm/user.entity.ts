import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { UserReferral } from './user-referral.entity';
import { UserRole, UserType } from '@mynx/types-shared';
import { Match } from './match.entity';
import { Player } from './player.entity';
import { Bank } from './bank.entity';
import { Transaction } from './transaction.entity';
import { Strike } from './strike.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName!: string;

  @Column()
  lastName!: string;

  @Column({ unique: true })
  email!: string;

  @Column({ unique: true })
  username!: string;

  @Column()
  password!: string;

  @Column({ nullable: true })
  coinbaseEmail!: string;

  @Column({ default: false })
  isVip!: boolean;

  // tracks if the user is a bot or not
  @Column({ default: false })
  isB!: boolean;

  @Column({ nullable: true })
  isVipAt!: Date;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.User,
  })
  role!: UserRole;

  @Column({
    type: 'enum',
    enum: UserType,
    default: UserType.Normal,
  })
  type!: UserType;

  @Column({ default: false })
  isBanned!: boolean;

  @Column({ nullable: true })
  ip!: string;

  @Column({ default: false })
  isGifted!: boolean;

  @Column({ default: false })
  isEmailVerified!: boolean;

  @Column({
    type: 'longtext',
    nullable: true,
  })
  image!: string;

  @Column({ nullable: true })
  referralBannerAdFileName!: string;

  // relationships
  @OneToMany(() => UserReferral, (entity) => entity.sender)
  referrals: User[];

  @OneToMany(() => Match, (entity) => entity.winner)
  wonMatches: Match[];

  @OneToMany(() => Player, (player) => player.user)
  plays!: Player[];

  @OneToMany(() => Bank, (bank) => bank.user)
  banks!: Bank[];

  @OneToMany(() => Transaction, (transaction) => transaction.user)
  transactions!: Transaction[];

  @OneToMany(() => Strike, (entity) => entity.user)
  strikes!: Strike[];

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
