import { Coin } from './coin.entity';
import { User } from './user.entity';
import { BigNumberTransformer } from '../typeorm-shared/big-number';
import BigNumber from 'bignumber.js';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';

@Entity()
export class Transaction {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: TransactionType,
  })
  type!: TransactionType;

  @Column({
    type: 'enum',
    enum: TransactionActivityType,
  })
  activityType!: TransactionActivityType;

  @Column({
    type: 'enum',
    enum: TransactionStatus,
    default: TransactionStatus.Waiting,
  })
  status!: TransactionStatus;

  @Column({ nullable: true })
  coinbaseAddress!: string;

  @Column({ nullable: true })
  coinbaseResourceId!: string;

  @Column({ nullable: true })
  coinbaseTransactionId!: string;

  @Column({ nullable: true })
  coinbaseHash!: string;

  @Column({
    type: 'text',
    transformer: BigNumberTransformer('amount'),
    nullable: true,
  })
  amount?: BigNumber;

  @Column({ nullable: true })
  receivedAt!: Date;

  @Column({ nullable: true })
  canceledAt!: Date;

  @Column({ nullable: true })
  completedAt!: Date;

  @Column({ nullable: true })
  notes!: string;

  // relationships
  @ManyToOne(() => User, (user) => user.transactions, { primary: true })
  user?: User;
  @Column({ nullable: false })
  userId!: number;

  @ManyToOne(() => Coin, (coin) => coin.transactions, { primary: true })
  coin?: Coin;
  @Column({ nullable: false })
  coinId!: number;

  // meta data
  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
