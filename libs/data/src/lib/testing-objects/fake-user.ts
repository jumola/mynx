import { v4 as uuidv4 } from 'uuid';
import * as randomstring from 'randomstring';
import { User } from '../typeorm/user.entity';
import {
  uniqueNamesGenerator,
  adjectives,
  animals,
  colors,
  countries,
  languages,
  names,
  starWars,
} from 'unique-names-generator';

export const fakeUser = (): Partial<User> => {
  const firstName = uniqueNamesGenerator({
    dictionaries: [names],
  });

  const lastName = uniqueNamesGenerator({
    dictionaries: [names],
  });

  const username = uniqueNamesGenerator({
    dictionaries: [
      animals,
      colors,
      countries,
      languages,
      names,
      starWars,
      [
        randomstring.generate({
          length: 2,
          charset: 'numeric',
        }),
        firstName,
        lastName,
      ],
    ],
    separator: '',
    length: 2,
    style: 'lowerCase',
  });

  return {
    firstName,
    lastName,
    username: username.substr(0, 10),
    email: `test@${uuidv4()}.com`,
    password: uuidv4(),
  };
};
