import { User } from '../typeorm/user.entity';
import { fakeUser } from '../testing-objects/fake-user';
import { app } from '@mynx/environment';
// TODO: fix this fuckin circulare dependency issue
import { request } from '@mynx/nest-utils';

export const newFakeUserWithToken = async (): Promise<{
  user: Partial<User>;
  accessToken: string;
}> => {
  try {
    const userData = fakeUser();

    const responseRaw = await request(
      'POST',
      `${app.secureApiBaseUrl}/auth/register`,
      userData
    );

    const response = responseRaw?.data as { accessToken: string };
    const response2 = {
      user: userData,
      accessToken: response.accessToken,
    };

    return response2;
  } catch (error) {
    if (error === 'User already exists') {
      return await newFakeUserWithToken();
    } else {
      throw new Error(error);
    }
  }
};
