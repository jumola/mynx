import { BigNumber } from 'bignumber.js';
import { ValueTransformer } from 'typeorm';
import { isNil } from 'lodash';
import { oneLine } from 'common-tags';

/**
 * Standard Javascript numbers are binary floating point, so they have
 * issues, especially with decimal fractions which affects money.
 *
 * (They also have issues with really big integers, but that seems less
 * likely to affect us.)
 *
 * BigNumber lets us do math operations, and we save the number as type
 * 'string' to the sqlite database, then restore to 'BigNumber' for operations.
 *
 * BigNumber.js is also implemented by the node mysql client so it seems reasonable
 * to use it here too.
 *
 * Some useful info:
 * http://2ality.com/2012/04/number-encoding.html
 * http://2ality.com/2013/10/safe-integers.html
 * https://docs.mongodb.com/manual/tutorial/model-monetary-data/
 *
 * Also, we ask for the columnName so we can provide better error messages.
 */
export const BigNumberTransformer = (
  columnName?: string
): ValueTransformer => ({
  from: (value) => (value ? new BigNumber(value) : null),
  to: (value) => {
    try {
      if (isNil(value)) {
        // do not do anything to null or undefined
        return value;
      } else if (BigNumber.isBigNumber(value)) {
        return value.toFixed();
      }

      const tryParse = new BigNumber(value);

      if (tryParse.isFinite()) {
        console.warn(
          oneLine`
            Column '${columnName}': Preferred type 'BigNumber' but got type
            '${typeof value}' and value '${value}' This can be parsed as BigNumber
            but it would be recommended to provide BigNumber directly.
          `
        );
        return tryParse.toFixed();
      } else {
        throw new Error(
          `Column '${columnName}': Expected type 'BigNumber' but got type '${typeof value}' and value '${value}'.`
        );
      }
    } catch (err) {
      // We discover that errors thrown in the transformer do not seem to be logged! So we log this again just in case.
      console.error('BigNumberTransformer had an error:', err);
      throw err;
    }
  },
});
