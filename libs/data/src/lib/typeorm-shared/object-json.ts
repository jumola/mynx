import { ValueTransformer } from 'typeorm';
import { isEmpty } from 'lodash';

export const ObjectJsonTransformer = (): ValueTransformer => ({
  from: (value) => (value ? JSON.parse(value) : null),
  to: (value) => (isEmpty(value) ? null : JSON.stringify(value)),
});
