import './next-shared.module.scss';

/* eslint-disable-next-line */
export interface NextSharedProps {}

export function NextShared(props: NextSharedProps) {
  return (
    <div>
      <h1>Welcome to next-shared!</h1>
    </div>
  );
}

export default NextShared;
