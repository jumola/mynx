import nc from 'next-connect';
import * as Boom from '@hapi/boom';
import type { NextApiRequest, NextApiResponse } from 'next';

export const ncHandler = nc({
  onError: (err: Error, req: NextApiRequest, res: NextApiResponse) => {
    if (Boom.isBoom(err)) {
      res.status(err.output.payload.statusCode);
      res.send(err.output.payload.message);
    } else {
      res.status(500);
      res.send(err.toString());
    }
  },
  onNoMatch: (req: NextApiRequest, res: NextApiResponse) => {
    res.status(405).send('Method Not Allowed');
  },
});
