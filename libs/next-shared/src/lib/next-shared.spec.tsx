import { render } from '@testing-library/react';

import NextShared from './next-shared';

describe('NextShared', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<NextShared />);
    expect(baseElement).toBeTruthy();
  });
});
