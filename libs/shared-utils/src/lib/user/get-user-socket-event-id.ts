import { User } from '@mynx/data';

export const getUserSocketEventId = (user: User): string => {
  return `event_me_${user.id}`;
};
