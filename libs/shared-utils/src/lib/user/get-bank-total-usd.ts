import { User } from '@mynx/data';
import { convertCryptoToUSD } from '../coin/convert-crypto-to-usd';
import BigNumber from 'bignumber.js';

// returns cumulative amount of user's banks in usd
export const getBankTotalUsd = (user: User) => {
  return BigNumber.sum(
    ...(user?.banks ?? []).map((bank) =>
      convertCryptoToUSD(bank.coin, bank.amount)
    ),
    0
  );
};
