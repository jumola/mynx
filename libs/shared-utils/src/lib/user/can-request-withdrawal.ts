import { Bank, User } from '@mynx/data';

export const canRequestWithdrawal = (user: User, bank: Bank): boolean => {
  return (
    user.isVip ||
    (!user.isVip &&
      bank.amount.isGreaterThanOrEqualTo(bank.coin.withdrawalMinimumAmount))
  );
};
