import { first, floor } from 'lodash';
import { DateTime } from 'luxon';
import { timeSince } from './time-since';

export const prettifyDate = (date: Date, format = 'yyyy-MM-dd HH:mm:ss') => {
  if (format === 'time-since') {
    return timeSince(date).text;
  }

  return DateTime.fromJSDate(new Date(date)).toFormat(format);
};
