import { first, floor, isNil } from 'lodash';
import { DateTime } from 'luxon';

type OptionType =
  | 'years'
  | 'months'
  | 'weeks'
  | 'days'
  | 'hours'
  | 'minutes'
  | 'seconds';

interface TimeSinceResult {
  option?: OptionType;
  text: string;
  value?: number;
}

export const timeSince = (dateRaw: Date, limitByOption?: OptionType) => {
  const date2 = DateTime.fromJSDate(new Date(dateRaw));
  return date2.toRelativeCalendar();

  // const now = DateTime.now();
  // const date2 = DateTime.fromJSDate(new Date(dateRaw));

  // const options = [
  //   'years',
  //   'months',
  //   'weeks',
  //   'days',
  //   'hours',
  //   'minutes',
  //   'seconds',
  // ].filter((option) => (limitByOption ? limitByOption === option : true));

  // const diffValues = now.diff(date2, options).values;

  // const result = first(
  //   options
  //     .filter((option: OptionType) => !isNil(diffValues[option]))
  //     .map((option: OptionType) => ({
  //       option,
  //       text: diffValues[option] + ' ' + option + ' ago',
  //       value: diffValues[option],
  //     }))
  //     .filter((dateData) => dateData.value > 0)
  // );

  // return {
  //   option: result?.option ?? undefined,
  //   text: result?.text ?? 'a moment ago',
  //   value: result?.value,
  // };
};
