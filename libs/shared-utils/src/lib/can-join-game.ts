import { Bank, Game, Match, User } from '@mynx/data';

export const canJoinGame = (
  user: User,
  game: Game,
  bank: Bank,
  match: Match
): boolean | string => {
  // has funds to join game

  if (bank.amount.isLessThan(game.costAmount)) {
    return 'You must add more funds to join this game!';
  }

  // hasn't joined the next available match for this game yet
  // TODO: make special rule for vip users
  if (match?.players?.find((p) => p.userId === user.id)) {
    return 'You have an ongoing match for this game, you may not join again!';
  }

  return true;
};
