import { Match } from '@mynx/data';

export const getMatchSocketEventId = (match: Match): string => {
  return `event_match_${match.id}`;
};
