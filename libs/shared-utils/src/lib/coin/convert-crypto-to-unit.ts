import { Coin } from '@mynx/data';
import BigNumber from 'bignumber.js';

export const convertCryptoToUnit = (
  coin: Coin,
  amount: BigNumber
): BigNumber => {
  const bigZero = new BigNumber(0);
  const fromAmount: BigNumber = (amount && new BigNumber(amount)) || bigZero;

  const newAmount = fromAmount.dividedBy(new BigNumber(coin?.unitDecimal || 0));

  return newAmount.decimalPlaces(0);
};
