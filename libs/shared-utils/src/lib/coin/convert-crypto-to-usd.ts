import { Coin } from '@mynx/data';
import BigNumber from 'bignumber.js';

export const convertCryptoToUSD = (
  coin: Coin,
  amount: BigNumber
): BigNumber => {
  const bigZero = new BigNumber(0);
  const fromAmount: BigNumber = (amount && new BigNumber(amount)) || bigZero;

  return fromAmount
    .multipliedBy(new BigNumber(coin?.priceUSD || 0))
    .decimalPlaces(5);
};

export const convertCryptoToUSDText = (coin: Coin, amount: BigNumber) => {
  return convertCryptoToUSD(coin, amount)?.toFormat(5);
};
