import { Coin } from '@mynx/data';
import BigNumber from 'bignumber.js';

export const convertUSDToCrypto = (
  coin: Coin,
  amount: BigNumber
): BigNumber => {
  const bigZero = new BigNumber(0);
  const fromAmount: BigNumber = (amount && new BigNumber(amount)) || bigZero;
  return fromAmount.dividedBy(new BigNumber(coin?.priceUSD || 0));
};
