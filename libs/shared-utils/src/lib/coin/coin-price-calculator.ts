import { Coin } from '@mynx/data';
import BigNumber from 'bignumber.js';
import { appMasterConfig } from '@mynx/environment';

export const coinPriceCalculator = (coin: Coin, coins: Coin[]) => {
  // so first, we want to determing the cost in crypto to upgrade to vip
  // which is (hard-coded cost to upgrade in usd) divided by coin's price usd
  coin.costToVipAmount = appMasterConfig.upgradeCostUSD.dividedBy(
    coin.priceUSD
  );

  // we're dividing the bonus above as we have to distribute that into the number
  // of type of coins we're giving away in (referral package bonus)
  coin.bonusReferVipToVipAmount = coin.costToVipAmount
    // first, in this currency, we want to get the value that is x % of how much we only want to give out
    .multipliedBy(appMasterConfig.vipROIRate)
    // then
    .dividedBy(coins.length);

  // vip inviting a new user gets the default
  coin.bonusReferVipToRegAmount = appMasterConfig.regReferralBonusUSD
    .dividedBy(coin.priceUSD)
    .dividedBy(new BigNumber(coins.length));

  // for regular users who invites a vip, they only get x percent of vip-to-vip
  coin.bonusReferRegToVipAmount = coin.bonusReferVipToVipAmount.multipliedBy(
    appMasterConfig.regVsVipRewardRate
  );

  // reg invites a reg, gets x % of vip-to-reg
  coin.bonusReferRegToRegAmount = coin.bonusReferVipToRegAmount.multipliedBy(
    appMasterConfig.regVsVipRewardRate
  );

  coin.welcomeBonusAmount = getWelcomeBonusAmountByCoin(coin);

  // users can withdraw their welcome bonus right away
  coin.withdrawalMinimumAmount = coin.welcomeBonusAmount;

  // faucets: is equal to welcome bonus amount for vip users
  // and minimuzed for normal users
  coin.faucetAmountVip = coin.welcomeBonusAmount;
  coin.faucetAmountReg = coin.faucetAmountVip.multipliedBy(
    appMasterConfig.regVsVipRewardRate
  );

  // TODO: Handle sale
  return coin;
};

// this function calculates the welcome bonus amount for each coin
const getWelcomeBonusAmountByCoin = (coin: Coin): BigNumber => {
  return (
    appMasterConfig.welcomeBonusUsd
      // calculate welcome bonus for one coin in usd
      .dividedBy(appMasterConfig.welcomeBonusCoinCount)
      // convert that to this coin's currency
      .dividedBy(coin.priceUSD)
  );
};
