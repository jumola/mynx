import GameComponent from './game';
import { useGames, useMyPlays } from '@mynx/react-shared';
import { uniq, uniqBy } from 'lodash';
import { appMasterConfig } from '@mynx/environment';
import { useEffect, useState } from 'react';
import { Game } from '@mynx/data';
import Select from 'react-select';
import { components } from 'react-select';
import { Button } from 'react-bootstrap';

const { Option, SingleValue } = components;
const IconOption = (props) => (
  <Option {...props} className="relative">
    <img src={props.data.label} style={{ width: 25 }} alt="" />
    <span className="absolute top-2 ">{props.data.name}</span>
  </Option>
);
const ValueOption = (props) => (
  <SingleValue {...props} className="relative">
    <img src={props.data.label} style={{ width: 25 }} alt="" />
    <span className="absolute top-0 ">{props.data.name}</span>
  </SingleValue>
);

const Games = ({ ...props }) => {
  const { data } = useGames();

  const { data: playsData } = useMyPlays();

  const joinedGameIds: number[] = uniq(
    playsData?.map((p) => p.match.gameId) ?? []
  );

  const [playerNumber, setPlayerNumber] = useState<number | undefined>(
    undefined
  );

  const [selectedCoinId, setSelectedCoinId] = useState<number | undefined>(
    undefined
  );

  const [games, setGames] = useState<Game[]>([]);

  useEffect(() => {
    if (playerNumber || selectedCoinId) {
      setGames(
        data?.filter(
          (g) =>
            (playerNumber ? g.players === playerNumber : true) &&
            (selectedCoinId ? g.coinId === selectedCoinId : true)
        )
      );
    } else {
      setGames(data);
    }
  }, [playerNumber, data, selectedCoinId]);

  const coins = uniqBy(data?.map((g) => g.coin) ?? [], 'id');

  return (
    <div {...props} className="text-center p-2">
      <div>
        <h1 className="text-xl font-bold">Available Games</h1>
        <p>
          Register into any game below and once the total required number of
          players have joined, a random winner will be chosen to win the
          Jackpot.
        </p>
      </div>

      <div className="mt-2">
        <div className="grid grid-flow-col">
          {[
            { value: undefined, label: 'All' },
            ...appMasterConfig.playerNumbers.map((number) => ({
              value: number,
              label: number,
            })),
          ].map((button, index) => (
            <div key={index}>
              <Button
                className="m-2"
                onClick={() => setPlayerNumber(button.value)}
                size="sm"
                disabled={playerNumber === button.value}
              >
                {button.label} Players
              </Button>
            </div>
          ))}
        </div>
      </div>

      <div className="grid grid-cols-5">
        <div className="col-span-5">
          <Select
            isClearable
            onChange={(e) => setSelectedCoinId(e?.value)}
            options={coins?.map((c) => ({
              name: c.name,
              label: c.icon,
              value: c.id,
            }))}
            components={{ Option: IconOption, SingleValue: ValueOption }}
            className="w-full"
          />
        </div>
      </div>

      <div className="grid grid-cols-12 mt-2">
        {games?.map((game, i) => (
          <GameComponent
            data={game}
            key={i}
            isJoined={Boolean(joinedGameIds.includes(game.id))}
          />
        ))}
      </div>
    </div>
  );
};

export default Games;
