import { User } from '@mynx/data';
import UserImage from '../shared/user-image';

const GameUser = (props) => {
  const data: User = props.data;
  return (
    <div>
      <div>
        <UserImage data={data} />
      </div>
      <div>{data?.username}</div>
    </div>
  );
};

export default GameUser;
