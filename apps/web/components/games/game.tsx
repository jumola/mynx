import { Game } from '@mynx/data';
import AppNumber from '../shared/app-number';
import { useRouter } from 'next/dist/client/router';
import { Badge, Button } from 'react-bootstrap';

const GameItem = (props: { data: Game; isJoined?: boolean }) => {
  const { data, isJoined } = props;
  const router = useRouter();

  const onClick = () => {
    router.push(`/games/${data.id}`);
  };

  return !data ? null : (
    <div
      onClick={onClick}
      {...props}
      className="text-white text-center p-2 col-span-6"
    >
      <div className="rounded-lg bg-indigo-900 px-2 py-4">
        <img
          alt=""
          style={{
            height: '40px',
            width: '40px',
            display: 'inline',
            maxWidth: '100%',
          }}
          src={data.coin.icon}
        />

        <div className="mt-2">
          <p className="text-xs mt-2">JACKPOT</p>
          <h3>
            <AppNumber
              value={data.prizeAmount}
              currency="crypto-unit"
              coin={data.coin}
              convert="to-unit"
            />
          </h3>
          <p className="text-xs">
            [
            <AppNumber
              value={data.prizeAmount}
              currency="crypto"
              coin={data.coin}
            />
            ]
          </p>
        </div>

        <p className="mt-2">
          <small>BUY-IN</small>
        </p>

        <h4>
          <AppNumber
            value={data.costAmount}
            currency="crypto-unit"
            coin={data.coin}
            convert="to-unit"
          />
        </h4>

        <p className="text-xs">
          [
          <AppNumber
            value={data.costAmount}
            currency="crypto"
            coin={data.coin}
          />
          ]
        </p>

        <p className="mt-2">{data.players} players</p>

        {isJoined && (
          <a href={'__gameUrl'}>
            <Button block size="sm">
              Enter game
            </Button>
          </a>
        )}

        {!isJoined ? null : <Badge variant="info">Is Joined</Badge>}
      </div>
    </div>
  );
};

export default GameItem;
