import { Button, Modal } from 'react-bootstrap';
import { Player } from '@mynx/data';
import GameItem from './game';
import AppNumber from '../shared/app-number';

const PlayModal = (props: {
  play: Player;
  show: boolean;
  onClose: () => void;
}) => {
  const { play, show, onClose } = props;

  const onPlayAgain = () => {
    //
    console.warn('Not implemented yet');
  };
  return !play ? null : (
    <Modal size="lg" animation={false} show={show}>
      <Modal.Header>
        <Modal.Title>Play data</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="grid grid-flow-col grid-cols-2">
          <div>
            <GameItem data={play.match.game} />
          </div>
          <div className="text-center">
            <p>Winner</p>
            <p>{play.user.username}</p>
            <p>Total Prize</p>
            <p>
              <AppNumber
                value={play.match.game.prizeAmount}
                currency="crypto-unit"
                convert="to-unit"
                coin={play.match.game.coin}
              />
            </p>
          </div>
        </div>

        <div className="grid grid-flow-col grid-cols">
          {play.match.players.map((player) => (
            <div>{player.user.username}</div>
          ))}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="info" onClick={onClose}>
          Close
        </Button>

        <Button variant="info" onClick={onPlayAgain}>
          Play Again
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PlayModal;
