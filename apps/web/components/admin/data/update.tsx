import {
  alertError,
  alertSuccess,
  fetchAdminUpdate,
  getApiErrorMessage,
  updateCoin,
  useAdminDocument,
  useCoin,
} from '@mynx/react-shared';
import { useState } from 'react';
import Layout from '../../shared/layout';
import CrudCreateUpdate from './subs/create-update';

export default function DataUpdatePage({ tableName, id }) {
  const { data } = useAdminDocument(tableName, id);

  const onSubmit = async (formData) => {
    if (id) {
      fetchAdminUpdate(tableName, id, formData)
        .then(() => alertSuccess('Ok!'))
        .catch(async (error) => alertError(await getApiErrorMessage(error)));
    }
  };

  return (
    <Layout
      mainContent={
        <CrudCreateUpdate
          mode="update"
          tableName={tableName}
          defaultValues={data}
          onSubmit={onSubmit}
        />
      }
    />
  );
}
