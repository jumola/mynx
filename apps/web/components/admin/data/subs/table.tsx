import { first, keys, startCase } from 'lodash';
import { Button } from 'react-bootstrap';
import DataTable from 'react-data-table-component';
import { useMemo, useState } from 'react';
import { Input } from '@mynx/react-shared';
import { useRouter } from 'next/dist/client/router';
import CoinImage from '../../../shared/coin-image';

export default function Table({ data: dataRaw, tableName }) {
  const [keyword, setKeyword] = useState<string | undefined>(undefined);

  const router = useRouter();

  const columns = useMemo(
    () => [
      {
        cell: (row) => (
          <Button
            size="sm"
            onClick={() => router.push(`/admin/data/${tableName}/${row.id}`)}
          >
            View
          </Button>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
      {
        cell: (row) => <CoinImage data={row} />,
      },
      ...keys(first(dataRaw ?? [])).map((key) => ({
        name: startCase(key),
        selector: (row) => row[key],
      })),
    ],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dataRaw]
  );

  const data = dataRaw?.filter((d) =>
    JSON.stringify(d).includes(keyword ?? '')
  );

  return (
    <>
      <div className="grid grid-cols">
        <div>
          <h1 className="fs-1">{tableName} list</h1>
        </div>
        <div className="col d-flex justify-content-end">
          <a href={`/admin/data/${tableName}/create`}>
            <Button variant="sm">Create</Button>
          </a>
        </div>
      </div>

      <div className="grid grid-cols">
        <div className="w-full overflow-x-scroll">
          <div className="w-1/5">
            <Input
              displayName="Keyword search"
              name=""
              placeholder="keyword"
              onChange={(e) => setKeyword(e.target.value)}
            />
          </div>

          <DataTable columns={columns} data={data} />
        </div>
      </div>
    </>
  );
}
