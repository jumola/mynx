import { Input } from '@mynx/react-shared';
import { keys } from 'lodash';
import { useRouter } from 'next/dist/client/router';
import { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';

export default function CrudCreateUpdate({
  mode,
  tableName,
  defaultValues,
  onSubmit,
}) {
  const router = useRouter();
  const { register, handleSubmit, reset } = useForm();

  const lockedKeys = ['id', 'createdAt', 'updatedAt'];
  const objKeys = keys(defaultValues);

  useEffect(() => {
    reset(defaultValues);
  }, [reset, defaultValues]);

  return (
    <div>
      <h1 className="fs-1">
        <Button onClick={() => router.back()} size="sm">
          Back
        </Button>
        {mode} {tableName}
      </h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="col-6 offset-3">
          {objKeys
            .filter((k) => !lockedKeys.includes(k))
            .map((k) => ({ name: k }))
            .map((field, index) => (
              <Input
                key={index}
                {...field}
                placeholder={field.name}
                displayName={field.name}
                hookRegister={{
                  ...register(field.name),
                }}
              />
            ))}

          <div className="d-grid">
            <Button className="mt-2" block variant="primary" type="submit">
              Submit
            </Button>
          </div>
        </div>
      </form>
    </div>
  );
}
