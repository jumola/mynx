import { useForm } from 'react-hook-form';
import {
  alertError,
  alertSuccess,
  createCoin,
  fetchAdminCreate,
  getApiErrorMessage,
  Input,
} from '@mynx/react-shared';
import { useState } from 'react';
import CrudCreateUpdate from './subs/create-update';
import Layout from '../../shared/layout';
import { useRouter } from 'next/dist/client/router';

export default function DataCreatePage({ tableName, defaultValues }) {
  const router = useRouter();

  const onSubmit = async (formData) => {
    const request = tableName
      ? fetchAdminCreate(tableName, formData)
      : undefined;

    if (request) {
      request
        .then(() => alertSuccess('Ok!'))
        .catch(async (error) => alertError(await getApiErrorMessage(error)));
    }
  };

  return (
    <Layout
      mainContent={
        <CrudCreateUpdate
          tableName={tableName}
          mode="create"
          defaultValues={defaultValues}
          onSubmit={onSubmit}
        />
      }
    />
  );
}
