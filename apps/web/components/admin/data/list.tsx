import { useAdminDashboard, useAdminTable } from '@mynx/react-shared';
import Layout from '../../shared/layout';
import Table from './subs/table';

export default function DataListPage({ tableName, queryName }) {
  const { data } = useAdminTable(tableName);
  return <Layout mainContent={<Table tableName={tableName} data={data} />} />;
}
