import { Transaction } from '@mynx/data';
import { fetchAdminUserStats } from '@mynx/react-shared';
import { convertCryptoToUSD } from '@mynx/shared-utils';
import { TransactionStatus } from '@mynx/types-shared';
import BigNumber from 'bignumber.js';
import { useEffect, useState } from 'react';

const AdminUserStats = (props: { userId: number }) => {
  const { userId } = props;

  const [data, setData] = useState<any>(undefined);

  useEffect(() => {
    if (!data) {
      fetchAdminUserStats(userId).then((data) => setData(data));
    }
  }, [data, userId]);

  const transactionByStatus = (status: TransactionStatus): Transaction[] => {
    return data.withdrawals.filter((w: Transaction) => w.status === status);
  };

  const totalUsd = (): string => {
    const amountsInUsd: BigNumber[] = transactionByStatus(
      TransactionStatus.Processing
    ).map((i) =>
      convertCryptoToUSD(i.coin, new BigNumber(i.amount).absoluteValue())
    );

    return BigNumber.sum(...amountsInUsd).toFormat();
  };

  return !data ? null : (
    <div className="text-sm">
      <p>Same IP: {data.sameIP?.length}</p>
      <p>Same pass: {data.samePass?.length}</p>
      <p>Plays: {data.plays?.length}</p>

      <p>Withdrawals: {data.withdrawals?.length}</p>
      <p>
        - Pendings: {transactionByStatus(TransactionStatus.Processing).length}
      </p>
      <p>
        - Approveds: {transactionByStatus(TransactionStatus.Completed).length}
      </p>
      <p>
        - Rejecteds: {transactionByStatus(TransactionStatus.Rejected).length}
      </p>
      <p>- Total USD: {totalUsd()}</p>

      <p>Deposits: {data.deposits?.length}</p>

      <p>Faucets: {data.faucets?.length}</p>
      <p>Referrals: {data.referrals?.length}</p>
    </div>
  );
};

export default AdminUserStats;
