import {
  alertError,
  alertSuccess,
  fetchMeFavoriteBank,
  getApiErrorMessage,
  useAuth,
} from '@mynx/react-shared';
import { Bank } from '@mynx/data';

const BankList = (props: { banks: Bank[]; onClick: (bank: Bank) => void }) => {
  const { banks, onClick } = props;
  return (
    <div>
      {banks?.map((b) => (
        <div
          key={b.id}
          onClick={() => onClick(b)}
          className="inline-block m-2 cursor-pointer"
        >
          <img width="25" src={b.coin.icon} alt={b.coin.name} />
          <p>{b.coinId}</p>
        </div>
      ))}
    </div>
  );
};

const FavoriteBanks = () => {
  const { user, refetch: refetchUser } = useAuth();
  const { banks } = user;

  const findFavoritesOrNot = (isFavorite: boolean, data: Bank[]) => {
    return data.filter((b) => b.isFavorite === isFavorite);
  };

  const onBankSelect = async (bank: Bank) => {
    try {
      await fetchMeFavoriteBank(bank.coinId);
      await refetchUser();
      alertSuccess('Yay!');
    } catch (error) {
      alertError(await getApiErrorMessage(error));
    }
  };

  return !user ? null : (
    <div className="text-center">
      <p className="text-4xl">
        <b>Set bank favorites</b>
      </p>
      <div className="">
        <p>Current favorites</p>
        <BankList
          banks={findFavoritesOrNot(true, banks ?? [])}
          onClick={onBankSelect}
        />
      </div>

      <div>
        <p>All coins</p>
        <BankList
          banks={findFavoritesOrNot(false, banks ?? [])}
          onClick={onBankSelect}
        />
      </div>
    </div>
  );
};

export default FavoriteBanks;
