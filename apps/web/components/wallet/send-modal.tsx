import { alertError } from '@mynx/react-shared';
import { convertCryptoToUSDText, convertUSDToCrypto } from '@mynx/shared-utils';
import { useEffect, useState } from 'react';
import { Button, Form, Modal } from 'react-bootstrap';
import BigNumber from 'bignumber.js';
import { Bank } from '@mynx/data';
import AppNumber from '../shared/app-number';

const SendModal = (props: {
  bank: Bank;
  show: boolean;
  onClose: () => void;
}) => {
  const sendLimitError = 'You can not send more than your wallet amount.';
  const { bank, show, onClose } = props;

  const [amountUsd, setAmountUsd] = useState<string | undefined>(undefined);

  const [amountToSend, setAmountToSend] = useState<string | undefined>(
    undefined
  );
  const [amountToSendUsd, setAmountToSendUsd] = useState<string | undefined>(
    undefined
  );
  const [addressToSendTo, setAddressToSendTo] = useState<string | undefined>(
    undefined
  );

  useEffect(() => {
    setAmountUsd(convertCryptoToUSDText(bank.coin, bank.amount));
  }, [bank]);

  const canSendAmount = () => {
    return (
      !amountToSend ||
      (amountToSend &&
        new BigNumber(amountToSend).isLessThanOrEqualTo(bank.amount))
    );
  };

  const canSend = () => canSendAmount() && Boolean(addressToSendTo);

  const onSend = () => {
    if (canSend()) {
      // TODO: API to send
      alertError('Not implemented yet');
    } else {
      alertError(sendLimitError);
    }
  };

  return (
    <Modal animation={false} show={show}>
      <Modal.Header>
        <Modal.Title>Send {bank.coin.name}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div>
          <div className="grid grid-flow-col grid-cols-2">
            <div className="p-2">Available</div>
            <div className="p-2">
              <p>${amountUsd}</p>
              <p>
                <AppNumber currency="crypto" value={bank.amount} />
              </p>
            </div>
          </div>

          <div className="grid grid-flow-col grid-cols-1 p-2">
            <p>Amount to withdraw</p>
          </div>

          <div className="grid grid-flow-col grid-cols-2">
            <div className="p-2">
              <p>{bank.coin.name}</p>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="number"
                  placeholder={bank.coin.name}
                  value={amountToSend ?? ''}
                  onChange={(e) => {
                    setAmountToSend(String(e.target.value));
                    setAmountToSendUsd(
                      convertCryptoToUSDText(
                        bank.coin,
                        new BigNumber(e.target.value)
                      )
                    );
                  }}
                />
              </Form.Group>
            </div>

            <div className="p-2">
              <p>USD</p>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="number"
                  placeholder="USD"
                  value={amountToSendUsd ?? ''}
                  onChange={(e) => {
                    setAmountToSendUsd(String(e.target.value));
                    setAmountToSend(
                      String(
                        convertUSDToCrypto(
                          bank.coin,
                          new BigNumber(e.target.value)
                        )
                      )
                    );
                  }}
                />
              </Form.Group>
            </div>
          </div>

          <div>
            <div className="p-2">
              <Button
                variant="outline-info"
                size="sm"
                onClick={() => {
                  setAmountToSend(bank.amount.toString());
                  setAmountToSendUsd(amountUsd);
                }}
              >
                Send all
              </Button>
            </div>
          </div>

          <div className="grid grid-flow-col grid-cols-1 p-2">
            <p>Address</p>
          </div>

          <div className="text-center">
            <small className="text-muted">
              Please ensure this address is a valid {bank.coin.longName} (
              {bank.coin.name}) address
            </small>
          </div>

          <div className="grid grid-flow-col grid-cols-1">
            <div className="p-2">
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="text"
                  placeholder="Address"
                  onChange={(e) => setAddressToSendTo(e.target.value)}
                />
              </Form.Group>
            </div>
          </div>

          <div>
            {!canSendAmount() && (
              <div className="text-center text-danger">
                <small>{sendLimitError}</small>
              </div>
            )}
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          variant="info"
          onClick={() => {
            onClose();
            setAmountToSend(undefined);
            setAmountToSendUsd(undefined);
          }}
        >
          Close
        </Button>
        <Button variant="info" onClick={onSend} disabled={!canSend()}>
          Send
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default SendModal;
