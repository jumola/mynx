import { Transaction } from '@mynx/data';
import { prettifyDate } from '@mynx/shared-utils';
import {
  PaginationMeta,
  TransactionActivityTypeText,
  TransactionStatusText,
  TransactionType,
} from '@mynx/types-shared';
import AppNumber from '../shared/app-number';
import CustomPagination from '../shared/pagination';

const WalletHistory = (props: {
  data: Transaction[];
  meta: PaginationMeta;
  onPageChange: (data: any) => void;
}) => {
  const { data, meta, onPageChange } = props;
  return (
    <div>
      <div>
        {data?.map((t) => {
          const txnSign = t.type === TransactionType.In ? '+' : '-';
          return (
            <div className="grid grid-cols-12 border-t-2 border-l-2 border-r-2 border-gray-200 p-2">
              <div className="col-span-1 text-center">
                <p className="text-gray-400 text-sm">
                  {prettifyDate(t.createdAt, 'MMM').toUpperCase()}
                </p>
                <p>{prettifyDate(t.createdAt, 'dd')}</p>
              </div>
              <div className="col-span-7">
                <p className="font-medium">
                  {TransactionActivityTypeText[t.activityType]}
                </p>
                <p className="text-sm text-gray-400">
                  {TransactionStatusText[t.status]}
                </p>
              </div>
              <div className="col-span-4 text-right">
                {!t.amount ? null : (
                  <>
                    <p>
                      {txnSign}
                      <AppNumber
                        coin={t.coin}
                        currency="crypto"
                        value={t.amount}
                      />
                    </p>
                    <p className="text-sm text-gray-400">
                      {txnSign}
                      <AppNumber
                        coin={t.coin}
                        convert="to-usd"
                        value={t.amount}
                      />
                    </p>
                  </>
                )}
              </div>
            </div>
          );
        })}
      </div>

      <CustomPagination meta={meta} onPageChange={onPageChange} />
    </div>
  );
};

export default WalletHistory;
