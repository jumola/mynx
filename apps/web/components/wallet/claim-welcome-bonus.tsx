import { alertError, useAuth } from '@mynx/react-shared';
import { MeService } from '../../services/me-service';
import { Button } from 'react-bootstrap';
import { useMemo } from 'react';
import WelcomeBonus from '../shared/welcome-bonus';

const ClaimWelcomeBonus = () => {
  const meService = new MeService();
  const { user, refetch: refetchUser } = useAuth();

  const canClaim: boolean = useMemo(
    () => user?.isEmailVerified && !user?.isGifted,
    [user]
  );

  const onClaim = async () => {
    await refetchUser();
    if (canClaim) {
      await meService.claimWelcomeBonus();
      await refetchUser();
    } else {
      alertError(
        'You need to verify your email address first to claim your welcome bonus!'
      );
    }
  };

  return user?.isGifted ? null : (
    <>
      <WelcomeBonus />
      <Button onClick={onClaim}>Claim Welcome Bonus</Button>
    </>
  );
};

export default ClaimWelcomeBonus;
