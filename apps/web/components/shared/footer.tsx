import { app } from '@mynx/environment';
import { Button } from 'react-bootstrap';
import { DateTime } from 'luxon';

const Footer = () => {
  return (
    <div className="text-xs p-4">
      <hr className="opacity-10 my-4" />
      <div className="grid grid-cols">
        <div className="col-span-10 col-start-2">
          <div>
            <div>
              {/* <img
                alt=""
                style={{
                  height: '40px',
                }}
                src="/malbux-logo.png"
              /> */}

              <p className="font-special font-bold mt-2">{app.appName}</p>

              <p className="mt-2">
                The only multi-cryptocurrency raffle game with a 100% fair
                algorithm which gives all players equal odds for unlimited
                rewards.
              </p>
              <p className="mt-2">
                Copyright © 2018 - {DateTime.local().toFormat('yyyy')}
              </p>
            </div>
            <div className="mt-2">
              {[
                // {
                //   path: '/partnership',
                //   title: 'Become a partner',
                // },
                {
                  path: '/winners',
                  title: 'Latest winners',
                },
                {
                  path: '/affiliates',
                  title: 'Affiliate program',
                },
                {
                  path: '/faq',
                  title: 'Frequently asked questions',
                },
                {
                  path: '/privacy',
                  title: 'Privacy policy',
                },
                {
                  path: '/terms',
                  title: 'Terms and conditions',
                },
                {
                  path: 'https://twitter.com',
                  title: 'Follow us on Twitter',
                },
              ].map(({ path, title }) => (
                <a href={path}>
                  <p>{title}</p>
                </a>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
