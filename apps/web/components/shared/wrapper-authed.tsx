import { fetchMe, storeRetrieve, storeRemove } from '@mynx/react-shared';
import { useRouter } from 'next/dist/client/router';
import { useState, useEffect } from 'react';
import Layout from './layout';

const WrapperAuthed = ({ mainContent }: { mainContent: JSX.Element }) => {
  const router = useRouter();
  const [verified, setVerified] = useState(false);

  useEffect(() => {
    const accessToken = storeRetrieve('accessToken');
    console.log('accessToken', accessToken);
    // if no accessToken was found,then we redirect to "/" page
    if (!accessToken) {
      router.replace('/login');
    } else {
      // we call the api that verifies the token
      fetchMe()
        .then(() => {
          setVerified(true);
        })
        .catch((e) => {
          // if the token was fraud we first remove it
          // from local storage then redirect to '/'
          storeRemove('accessToken');
          router.replace('/');
        });
    }
  }, []);

  return verified ? <Layout mainContent={mainContent} /> : null;
};

export default WrapperAuthed;
