import { PaginationMeta } from '@mynx/types-shared';
import ReactPaginate from 'react-paginate';

const CustomPagination = (props: {
  onPageChange: (data: any) => void;
  meta: PaginationMeta;
}) => {
  const { meta, onPageChange } = props;
  return !meta ? null : (
    <ReactPaginate
      onPageChange={onPageChange}
      pageCount={meta.totalPages}
      pageRangeDisplayed={1}
      marginPagesDisplayed={1}
      breakClassName={'page-item'}
      breakLinkClassName={'page-link'}
      containerClassName={'pagination'}
      pageClassName={'page-item'}
      pageLinkClassName={'page-link'}
      previousClassName={'page-item'}
      previousLinkClassName={'page-link'}
      nextClassName={'page-item'}
      nextLinkClassName={'page-link'}
      activeClassName={'active'}
    />
  );
};

export default CustomPagination;
