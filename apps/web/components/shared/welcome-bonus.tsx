import { useCoins } from '@mynx/react-shared';
import AppNumber from './app-number';

const WelcomeBonus = () => {
  const { data: coinsData } = useCoins();
  const coins = coinsData?.filter((c) => c.isFeatured);

  return (
    <div className="w-full text-center container">
      <div className="flex items-stretch">
        {coins?.map((coin, i) => (
          <div key={i} className="flex-1 p-2">
            <div className="bg-purple-900 text-white p-2 rounded-lg h-full ">
              <div className="centered-y relative">
                <img
                  alt={coin.name}
                  src={coin.icon}
                  className="animate-pulse w-2/4 h-auto inline-block"
                />
                <AppNumber
                  className="text-xs mt-2 block"
                  value={coin.welcomeBonusAmount}
                  currency="crypto-unit"
                  convert="to-unit"
                  coin={coin}
                />
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default WelcomeBonus;
