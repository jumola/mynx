import { useAuth } from '@mynx/react-shared';
import { Container, Nav, Navbar } from 'react-bootstrap';

const links = [
  {
    href: '/home',
    title: 'Home',
  },
  {
    href: '/winners',
    title: 'Winners',
  },
  {
    href: '/games',
    title: 'Games',
  },
  {
    href: '/wallet',
    title: 'Wallet',
  },
  {
    href: '/plays',
    title: 'Plays',
  },
  {
    href: '/account',
    title: 'Account',
  },
  {
    href: '/faucets',
    title: 'Faucets',
  },
];

const NavLink = ({ href, title }) => {
  return (
    <Nav.Link className="text-gray-400 hover:text-purple-900" href={href}>
      {title}
    </Nav.Link>
  );
};

const NavBar = () => {
  const auth = useAuth();
  return (
    <Navbar expand="sm" collapseOnSelect>
      <Container>
        <Navbar.Brand href="/" className="font-special ">
          <span className="text-purple-900">malbux</span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav border-none text-purple-900" />
        <Navbar.Collapse id="responsive-navbar-nav">
          {links.map(({ href, title }) => (
            <NavLink key={href} href={href} title={title} />
          ))}
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
