import { Coin } from '@mynx/data';
import {
  convertCryptoToUnit,
  convertCryptoToUSD,
  convertUSDToCrypto,
} from '@mynx/shared-utils';
import BigNumber from 'bignumber.js';
import { isNil } from 'lodash';

const AppNumber = (props: {
  value: BigNumber;
  currency?: 'usd' | 'crypto' | 'crypto-unit';
  convert?: 'to-usd' | 'to-crypto' | 'to-unit';
  coin?: Coin;
  decimalPoint?: number;
  [key: string]: any;
}) => {
  const { value, currency, convert, coin, decimalPoint, ...rest } = props;

  const defaultDecimalPt = 5;
  const decPt =
    currency === 'crypto-unit'
      ? 0
      : decimalPoint
      ? decimalPoint
      : coin?.exponent
      ? coin.exponent
      : defaultDecimalPt;

  let finalValue: BigNumber | undefined;

  if (!isNil(value)) {
    finalValue = !BigNumber.isBigNumber(value) ? new BigNumber(value) : value;
  }

  if (!isNil(finalValue) && coin) {
    if (convert === 'to-usd') {
      finalValue = convertCryptoToUSD(coin, value);
    } else if (convert === 'to-crypto') {
      finalValue = convertUSDToCrypto(coin, value);
    } else if (convert === 'to-unit') {
      finalValue = convertCryptoToUnit(coin, value);
    }
  }

  let result: string | undefined;

  if (!isNil(finalValue)) {
    result = finalValue.isEqualTo(0) ? '0' : finalValue.toFormat(decPt);
  }

  if (currency === 'usd' || convert === 'to-usd') {
    result = `$${result}`;
  } else if (coin && (currency === 'crypto' || convert === 'to-crypto')) {
    result = `${result} ${coin.name}`;
  } else if (coin && (currency === 'crypto-unit' || convert === 'to-unit')) {
    result = `${result} ${coin?.unitName ?? `${coin?.name} satoshi`}`;
  }

  return isNil(finalValue) ? null : <span {...rest}>{result}</span>;
};

export default AppNumber;
