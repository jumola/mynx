import Footer from './footer';
import NavBar from './navbar';
import { ToastContainer } from 'react-toastify';

export default function Layout({ mainContent }: { mainContent: JSX.Element }) {
  return (
    <div className="font-custom">
      <NavBar />
      <div className="p-2 min-h-screen relative container">{mainContent}</div>
      <Footer />
      <ToastContainer />
    </div>
  );
}
