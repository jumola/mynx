import { User } from '@mynx/data';

const UserImage = (props) => {
  const data: User = props?.data;
  const size: number = props?.size;
  const dimension = size ?? '40';
  return (
    <img
      className="rounded"
      alt=""
      style={{
        height: `${dimension}px`,
        width: `${dimension}px`,
        display: 'inline',
        maxWidth: '100%',
      }}
      src={
        data.image ??
        'data:image/svg+xml;utf8,%3Csvg%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%205%205%22%20shape-rendering%3D%22crispEdges%22%20width%3D%2260%22%20height%3D%2260%22%3E%3Cmetadata%3E%3Crdf%3ARDF%3E%3Ccc%3AWork%3E%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%3Cdc%3Atype%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%2F%3E%3Cdc%3Atitle%3EIdenticon%3C%2Fdc%3Atitle%3E%3Cdc%3Acreator%3E%3Ccc%3AAgent%3E%3Cdc%3Atitle%3EFlorian%20K%C3%B6rner%3C%2Fdc%3Atitle%3E%3C%2Fcc%3AAgent%3E%3C%2Fdc%3Acreator%3E%3Cdc%3Asource%3Ehttps%3A%2F%2Fgithub.com%2Fdicebear%2Fdicebear%3C%2Fdc%3Asource%3E%3Ccc%3Alicense%20rdf%3Aresource%3D%22https%3A%2F%2Fcreativecommons.org%2Fpublicdomain%2Fzero%2F1.0%2F%22%2F%3E%3C%2Fcc%3AWork%3E%3Ccc%3ALicense%20rdf%3Aabout%3D%22https%3A%2F%2Fcreativecommons.org%2Fpublicdomain%2Fzero%2F1.0%2F%22%3E%3Ccc%3Apermits%20rdf%3Aresource%3D%22https%3A%2F%2Fcreativecommons.org%2Fns%23Reproduction%22%2F%3E%3Ccc%3Apermits%20rdf%3Aresource%3D%22https%3A%2F%2Fcreativecommons.org%2Fns%23Distribution%22%2F%3E%3Ccc%3Apermits%20rdf%3Aresource%3D%22https%3A%2F%2Fcreativecommons.org%2Fns%23DerivativeWorks%22%2F%3E%3C%2Fcc%3ALicense%3E%3C%2Frdf%3ARDF%3E%3C%2Fmetadata%3E%3Cmask%20id%3D%22avatarsRadiusMask%22%3E%3Crect%20width%3D%225%22%20height%3D%225%22%20rx%3D%220%22%20ry%3D%220%22%20x%3D%220%22%20y%3D%220%22%20fill%3D%22%23fff%22%2F%3E%3C%2Fmask%3E%3Cg%20mask%3D%22url(%23avatarsRadiusMask)%22%3E%3Cpath%20d%3D%22M0%204h1v1H0V4zm4%200h1v1H4V4z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22%237CB342%22%2F%3E%3Cpath%20d%3D%22M0%203h5v1H0V3z%22%20fill%3D%22%237CB342%22%2F%3E%3Cpath%20d%3D%22M0%202h1v1H0V2zm2%200h1v1H2V2zm2%200h1v1H4V2z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22%237CB342%22%2F%3E%3Cpath%20d%3D%22M0%201h1v1H0V1zm4%200h1v1H4V1z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22%237CB342%22%2F%3E%3Cpath%20d%3D%22M0%200h1v1H0V0zm2%200h1v1H2V0zm2%200h1v1H4V0z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22%237CB342%22%2F%3E%3C%2Fg%3E%3C%2Fsvg%3E'
      }
    />
  );
};

export default UserImage;
