import { Coin, User } from '@mynx/data';

const CoinImage: React.FC<{
  data: Coin;
  size?: string;
}> = (props) => {
  const { data, size } = props;
  const dimension = size ?? '40px';
  return (
    <img
      alt={data.name}
      style={{
        height: `${dimension}`,
        width: `${dimension}`,
        display: 'inline',
        maxWidth: '100%',
      }}
      src={data.icon}
    />
  );
};

export default CoinImage;
