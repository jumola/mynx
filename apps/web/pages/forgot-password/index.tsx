import { useForm } from 'react-hook-form';
import Layout from '../../components/shared/layout';
import {
  alertError,
  getApiErrorMessage,
  Input,
  useAuth,
} from '@mynx/react-shared';
import { Button } from 'react-bootstrap';

const ForgotPasswordPage = () => {
  const auth = useAuth();
  const { register, handleSubmit } = useForm<any>();

  const onSubmit = (formData) => {
    auth.signIn(formData).catch(async (error) => {
      alertError(await getApiErrorMessage(error));
    });
  };

  return (
    <div className="centered-y text-center ">
      <div className="text-4xl font-bold font-special text-purple-900">
        <p>We got ya!</p>
        <p className="text-lg">Type in your email and we'll send you a link</p>
      </div>

      <div className="container">
        <form>
          <Input
            displayName="Email address"
            name="email"
            type="email"
            placeholder="Your email address"
            required={true}
            hookRegister={{
              ...register('email', { required: true }),
            }}
          />

          <Button onClick={handleSubmit(onSubmit)} className="mt-2">
            Send recovery link
          </Button>
          <a href="/login">
            <p className="mt-2">Remember your password?</p>
          </a>
        </form>
      </div>
    </div>
  );
};

export default ForgotPasswordPage;
