import { AppProps } from 'next/app';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-toastify/dist/ReactToastify.css';
import './styles.css';
import { QueryClientProvider } from 'react-query';
import { queryClient, useAuth } from '@mynx/react-shared';
import { useEffect } from 'react';
import dynamic from 'next/dynamic';
import Layout from '../components/shared/layout';
import WrapperAuthed from '../components/shared/wrapper-authed';
import { useRouter } from 'next/dist/client/router';

const ALL_PAGES: {
  name: string;
  path: string;
  Component: React.ComponentType<any>;
  isProtected?: boolean;
}[] = [
  {
    name: 'Landing',
    path: '/',
    Component: dynamic(() => import('./landing')),
  },
  {
    name: 'Home',
    path: '/home',
    Component: dynamic(() => import('./home')),
    isProtected: true,
  },
  {
    name: 'Latest Winners',
    path: '/winners',
    Component: dynamic(() => import('./winners')),
    isProtected: false,
  },
  {
    name: 'Wallets',
    path: '/wallet',
    Component: dynamic(() => import('./wallet')),
    isProtected: true,
  },
  {
    name: 'Wallet',
    path: '/wallet/:bankId',
    Component: dynamic(() => import('./wallet/[bankId]')),
    isProtected: true,
  },
  {
    name: 'Register',
    path: '/register',
    Component: dynamic(() => import('./register')),
    isProtected: false,
  },
  {
    name: 'Plays',
    path: '/plays',
    Component: dynamic(() => import('./plays')),
    isProtected: true,
  },
  {
    name: 'Login',
    path: '/login',
    Component: dynamic(() => import('./login')),
    isProtected: false,
  },
  {
    name: 'Games',
    path: '/games',
    Component: dynamic(() => import('./games')),
    isProtected: false,
  },
  {
    name: 'Game',
    path: '/games/[id]',
    Component: dynamic(() => import('./games/[id]')),
    isProtected: false,
  },
  {
    name: 'Match',
    path: '/games/:id/match/:matchId',
    Component: dynamic(() => import('./games/[id]/match/[matchId]')),
    isProtected: false,
  },
  {
    name: 'Forgot Password',
    path: '/forgot-password',
    Component: dynamic(() => import('./forgot-password')),
    isProtected: false,
  },
  {
    name: 'Faucets',
    path: '/faucets',
    Component: dynamic(() => import('./faucets')),
    isProtected: true,
  },
  {
    name: 'Account',
    path: '/account',
    Component: dynamic(() => import('./account')),
    isProtected: true,
  },
  {
    name: 'Affiliates',
    path: '/affiliates',
    Component: dynamic(() => import('./affiliates')),
    isProtected: false,
  },
  {
    name: 'FAQ',
    path: '/faq',
    Component: dynamic(() => import('./faq')),
    isProtected: false,
  },
  {
    name: 'Privacy Policy',
    path: '/privacy',
    Component: dynamic(() => import('./privacy')),
    isProtected: false,
  },
  {
    name: 'Terms and Conditions',
    path: '/terms',
    Component: dynamic(() => import('./terms')),
    isProtected: false,
  },
];

const AppShell: React.FC = () => {
  const router = useRouter();
  return (
    <>
      {ALL_PAGES.filter(({ path }) => path === router.pathname).map(
        ({ path, Component, isProtected }) =>
          isProtected ? (
            <WrapperAuthed key={path} mainContent={<Component />} />
          ) : (
            <Layout key={path} mainContent={<Component />} />
          )
      )}
    </>
  );
};

const App = ({ Component, pageProps }: AppProps) => {
  useEffect(() => {
    useAuth.getState().init();
  }, []);
  return (
    <QueryClientProvider client={queryClient}>
      <AppShell>
        <Component {...pageProps} />
      </AppShell>
    </QueryClientProvider>
  );
};

export default App;
