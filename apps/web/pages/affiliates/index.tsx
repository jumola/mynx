import { useCoins } from '@mynx/react-shared';
import AppNumber from '../../components/shared/app-number';
import { Image } from 'react-bootstrap';
import Layout from '../../components/shared/layout';
import LatestWinners from '../landing/latest-winners';
import { Coin } from '@mynx/data';
import clsx from 'clsx';
import { app } from '@mynx/environment';

const Bonuses = ({
  coins,
  from,
  to,
}: {
  coins?: Coin[];
  from: 'Reg' | 'Vip';
  to: 'Reg' | 'Vip';
}) => {
  return (
    <div className="">
      <div className="w-full text-center container">
        <div className="flex items-stretch">
          {coins?.map((coin, i) => (
            <div key={`refer-coin-${coin.name}-${i}`} className="flex-1 p-2">
              <div
                className={clsx([
                  'p-2 rounded-lg h-full',
                  from === 'Reg'
                    ? 'bg-purple-900 text-white '
                    : 'bg-yellow-400 text-black ',
                ])}
              >
                <div className="centered-y relative">
                  <img
                    alt={coin.name}
                    src={coin.icon}
                    className="animate-pulse w-2/4 h-auto inline-block"
                  />
                  <AppNumber
                    className="text-xs mt-2 block"
                    value={coin[`bonusRefer${from}To${to}Amount`]}
                    currency="crypto-unit"
                    convert="to-unit"
                    coin={coin}
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default function AffiliateProgramPage() {
  const { data: coinsData } = useCoins();
  const coins = coinsData?.filter((c) => c.isFeatured);

  return (
    <div className="text-center">
      <Image className="inline h-80" src="/images/ud-friends.png" />
      <p className="text-xl font-bold">Affiliates</p>
      <p>Stop earning pennies and start earning real money</p>

      <div className="my-4">
        <p>Get all these when you invite a friend:</p>
        <Bonuses coins={coins} from="Reg" to="Reg" />

        <p>Plus another bonus if your friend goes VIP.</p>
        <Bonuses coins={coins} from="Reg" to="Vip" />
      </div>

      <p className="font-bold">But wait, there's more for our VIP's'!</p>

      <div className="my-4">
        <p>Get all these when you invite a friend:</p>
        <Bonuses coins={coins} from="Vip" to="Reg" />

        <p>Plus a bigger bonus if your friend goes VIP too!</p>
        <Bonuses coins={coins} from="Vip" to="Vip" />
      </div>

      <p>
        Rewards are instantly deposited into your {app.appName} wallet as soon
        as they sign up and may already used to play games or be withdrawn to
        your external cryptocurrency wallet.
      </p>
    </div>
  );
}
