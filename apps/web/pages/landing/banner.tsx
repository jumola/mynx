import { app } from '@mynx/environment';

const Banner = ({ ...props }) => {
  return (
    <div {...props} className="text-center p-4">
      <img src="/malbux-logo.png" className="inline h-20 w-auto" alt="" />
      <p className="font-special text-2xl mt-2 text-purple-900">
        {app.appName}
      </p>
      <p className="mt-2">
        The only multi-cryptocurrency raffle game with a 100% fair algorithm
        which gives all players equal odds for unlimited rewards.
      </p>
    </div>
  );
};

export default Banner;
