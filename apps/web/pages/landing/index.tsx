import Games from '../../components/games';
import Banner from './banner';
import HowItWorks from './how-it-works';
import LatestWinners from './latest-winners';

const Landing = () => (
  <>
    <Banner />
    <Games />
    <HowItWorks />
    <LatestWinners />
  </>
);

export default Landing;
