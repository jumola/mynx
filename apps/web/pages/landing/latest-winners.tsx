import { Match } from '@mynx/data';
import { useAuth, useLatestWinners } from '@mynx/react-shared';
import UserImage from '../../components/shared/user-image';
import { Button, Image } from 'react-bootstrap';
import AppNumber from '../../components/shared/app-number';
import CoinImage from '../../components/shared/coin-image';
import { timeSince } from '@mynx/shared-utils';

const LatestWinners = ({ ...props }) => {
  const { user } = useAuth();
  const { data } = useLatestWinners();

  return (
    <div {...props} className="text-center mt-4">
      <Image className="inline h-80" src="/images/ud-winners.png" />
      <p className="text-xl font-bold">Latest Winners</p>

      <div className="grid grid-cols-12">
        {data?.map((match, index) => (
          <div key={index} className="col-span-6 py-2">
            <div className="flex">
              <div className="flex-2 p-2 text-right">
                <CoinImage data={match.game.coin} />
              </div>
              <div className="flex-auto p-2 text-sm text-left">
                <div>
                  <UserImage
                    className="rounded-full shadow"
                    size={15}
                    data={match.winner}
                  />{' '}
                  <span className="text-gray-400">{match.winner.username}</span>
                </div>
                <div>
                  <AppNumber
                    value={match.game.potAmount}
                    currency="crypto-unit"
                    convert="to-unit"
                    coin={match.game.coin}
                  />
                </div>
                <div className="text-xs text-gray-400">
                  {timeSince(match.concludedAt)}
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>

      <div className="p-4 mt-2 text-center row">
        {user ? (
          <a href="/games">
            <Button>Play games</Button>
          </a>
        ) : (
          <a href="/register">
            <Button>Join the fun now</Button>
          </a>
        )}
      </div>
    </div>
  );
};

const WinnerRow = (props: { data: Match }) => {
  const { data } = props;
  return (
    <tr>
      <td>
        <div className="w-full text-center">
          <img
            alt=""
            className="rounded-full"
            style={{
              height: '25px',
              display: 'inline-block',
            }}
            src={data.game.coin.icon}
          />
        </div>
      </td>
      <td>
        <img
          alt=""
          className="rounded-full"
          style={{
            height: '25px',
            display: 'inline-block',
          }}
          src={data.winner.image}
        />

        <span className="ml-2">{data.winner.username}</span>
      </td>
      <td>
        <AppNumber
          value={data.game.potAmount}
          currency="crypto-unit"
          convert="to-unit"
          coin={data.game.coin}
        />
      </td>
      <td>{timeSince(data.concludedAt)}</td>
    </tr>
  );
};

export default LatestWinners;
