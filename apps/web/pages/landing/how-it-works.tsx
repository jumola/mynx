import { Button } from 'react-bootstrap';

const contents: {
  title: string;
  text: string;
  image: string;
}[] = [
  {
    title: 'Join',
    text: 'Create a new account for FREE! To get paid, enter the email address associated with your Coinbase account. This does NOT let us have access to your Coinbase account.',
    image: '/images/ud-join.png',
  },
  {
    title: 'Bonus',
    text: `Claim your FREE welcome bonus after registering for an account. You may choose to either play with it or cash it out instantly!`,
    image: '/images/ud-gift-box.png',
  },
  {
    title: 'Play',
    text: 'Using the balance in your account, buy-in to any available game of any coin of your choice. All your winnings are instantly deposited into your __appName account.',
    image: '/images/ud-select-player.png',
  },
  {
    title: 'Cash Out',
    text: `Once your're happy with all your winnings, you may either join the higher stakes games for bigger prizes or cash it all out to your Coinbase account.`,
    image: '/images/ud-savings.png',
  },
];

const HowItWorks = ({ ...props }) => {
  return (
    <div {...props} className="text-center py-4">
      <h2 className="fs-1 font-bold">How It Works</h2>

      <div className="mt-4 grid grid-cols-12">
        {contents.map((c, i) => (
          <div key={i} className="col-span-12 p-2">
            <h3 className="mt-4 fs-3">{c.title}</h3>
            <img
              alt=""
              style={{
                width: '100%',
              }}
              src={c.image}
            />

            <p className="mt-2">{c.text}</p>
          </div>
        ))}
      </div>

      <div className="text-center mt-4">
        <a href="/register">
          <Button>Sign up now</Button>
        </a>
      </div>
    </div>
  );
};

export default HowItWorks;
