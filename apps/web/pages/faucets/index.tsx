import { Bank, Coin, User } from '@mynx/data';
import {
  alertError,
  alertSuccess,
  fetchMeFaucetBonus,
  getApiErrorMessage,
  useAuth,
  useCoins,
  useUserSocket,
} from '@mynx/react-shared';
import { convertCryptoToUSD, getUserSocketEventId } from '@mynx/shared-utils';
import AppNumber from '../../components/shared/app-number';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import Layout from '../../components/shared/layout';
import CoinImage from '../../components/shared/coin-image';
import BigNumber from 'bignumber.js';
import { Button, NavLink } from 'react-bootstrap';

type BonusPackType = 'faucetAmountReg' | 'faucetAmountVip';

const BonusPack = (props: { type: BonusPackType; coins: Coin[] }) => {
  const { type, coins } = props;

  const getTotal = (key: BonusPackType) =>
    BigNumber.sum(...coins.map((c) => convertCryptoToUSD(c, c[key])), 0);

  return (
    <div className="py-4">
      {/* <p>
        <AppNumber value={getTotal(type)} currency="usd" />
      </p> */}

      <div className="grid grid-cols-4">
        {coins?.map((coin) => (
          <div className="p-2">
            <CoinImage data={coin} />
            <p>{coin.name}</p>

            <p>
              <AppNumber
                value={coin.faucetAmountReg}
                convert="to-unit"
                coin={coin}
                currency="crypto-unit"
              />
            </p>

            {/* <p>
              <AppNumber
                value={coin.faucetAmountReg}
                convert="to-usd"
                coin={coin}
                currency="usd"
              />
            </p> */}
          </div>
        ))}
      </div>
    </div>
  );
};

const FaucetsPage = () => {
  const { user, accessToken } = useAuth();
  const { banks } = user;
  const coins = (banks ?? []).filter((b) => b.isFavorite).map((b) => b.coin);

  const onClaim = async () => {
    try {
      await fetchMeFaucetBonus();
      alertSuccess('Yay!');
    } catch (error) {
      alertError(await getApiErrorMessage(error));
    }
  };

  return (
    <div>
      <div className="text-center">
        <div>
          <h2 className="text-2xl">Faucets</h2>
          <p>Claim a Faucet Bonus Package every hour 24/7</p>
        </div>

        <div className="grid grid-cols-12 gap-20 p-2">
          <div className="col-start-5 col-span-4 p-2">
            <h2 className="text-2xl">Non-VIP Faucet Bonus</h2>
            {!coins ? null : <BonusPack type="faucetAmountReg" coins={coins} />}
          </div>

          <div className="col-start-5 col-span-4 p-2">
            <h2 className="text-2xl">VIP Faucet Bonus</h2>
            {!coins ? null : <BonusPack type="faucetAmountVip" coins={coins} />}
          </div>
        </div>

        <div>
          <Button onClick={onClaim}>
            Claim {user?.isVip ? null : 'Non-'}VIP Faucet Bonus
          </Button>
        </div>

        <div className="mt-2">
          <a href="/vip">
            <Button>Learn more about VIP perks</Button>
          </a>
        </div>
      </div>
    </div>
  );
};

export default FaucetsPage;
