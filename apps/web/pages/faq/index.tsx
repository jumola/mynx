import { useCoins } from '@mynx/react-shared';
import AppNumber from '../../components/shared/app-number';
import { Image } from 'react-bootstrap';
import Layout from '../../components/shared/layout';
import LatestWinners from '../landing/latest-winners';
import { Coin } from '@mynx/data';
import clsx from 'clsx';
import { app } from '@mynx/environment';

export default function AffiliateProgramPage() {
  const { data: coinsData } = useCoins();
  const coins = coinsData?.filter((c) => c.isFeatured);

  const coinNames: string[] = coins?.map((c) => c.name);

  const questions: { q: string; a: string }[] = [
    {
      q: `What is ${app.appName}?`,
      a: 'The only multi-cryptocurrency raffle game with a 100% fair algorithm which gives all players equal odds for unlimited rewards.',
    },
    {
      q: `Which coins do I get as a Welcome Bonus?`,
      a: `You get a bonus for each of ALL the featured coins which currently are ${coinNames?.join(
        ', '
      )}. You may immediately use it to play. `,
    },
    {
      q: `What's the game mechanics?`,
      a: `In every game, a specific number pool of players is required to start. Once complete, our system then instantly chooses a random winner from the pool of players. The winner then automatically receives the whole prize pot into their account. And that's it, you may either enter other games to play again or withdraw your winnings.`,
    },
    {
      q: 'Once registered into a game, can I register again into the same game to increase my winning odds?',
      a: `Yes. All you have to do is upgrade your account to VIP status and you will then be able to register again into any game. You may learn more about the VIP perks on the VIP page.`,
    },
    {
      q: 'Do I need a Coinbase account to play?',
      a: `You don't need a Coinbase account to play. But if you're Non-VIP, you need a Coinbase account to withdraw from your ${app.appName} wallets. Please make sure you enter the email address linked to your Coinbase account in the account page. IMPORTANT: This does NOT give us access to your Coinbase account. It just enables us to send funds to it.`,
    },
    {
      q: 'How to withdraw my winnings?',
      a: `To withdraw your earnings, go to the Wallet page of your chosen currency and click on withdraw. Our system then sends your ${app.appName} wallet funds to your Coinbase account. Note: Withdrawal requests may take from a minute to a day depending on our queue.`,
    },
    {
      q: `What if I don't have a Coinbase account, can I still withdraw my funds?`,
      a: `Absolutely! Just upgrade your account to VIP status and you will be able to withdraw funds from your ${app.appName} wallet to any external wallet address. Go to the VIP page to learn more about our VIP program.`,
    },
    {
      q: `Is there a withdrawal fee?`,
      a: `No. It's totally FREE!`,
    },
    {
      q: `Can I add deposit funds into my ${app.appName} wallets, to either join higher-stake games or upgrade to join the VIP program?`,
      a: `Yes! Open your selected ${app.appName} wallet to get the blockchain address you can use to send funds to. Please be aware that deposits may take from a few minutes to an hour to complete processing, depending on the currency. Once we receive the complete verification for your deposit, the desposited amount will be instantly added to your corresponding ${app.appName} wallet.`,
    },
    {
      q: `Do I need a Coinbase account to deposit funds?`,
      a: `No. You may use any third-party wallet of your choice. Hint: It will likely be cheapest and fastest if you deposit funds from your Coinbase account.`,
    },
    {
      q: `Do you have an affiliate program?`,
      a: `Yes. You will be instantly rewarded as soon as someone registers through your referral link or through the link of a game you're playing. Go to the Affiliates page to learn more about our affiliate program bonuses.`,
    },
  ];

  return (
    <div>
      <div className="text-center">
        <Image className="inline h-80" src="/images/ud-faq.png" />
        <p className="text-xl font-bold">F.A.Q.</p>
        <p>Got some questions?</p>
      </div>

      <div className="my-4">
        {questions.map(({ q, a }, index) => (
          <TextItem key={`faq-${index}`} q={q} a={a} />
        ))}

        <TextItem
          q="I have other questions and need help. How do I contact you?"
          a="Please do! Go to our Contact Us page to send us a message and someone from our team will pick it up and reach out to you within 24 hours."
        />
      </div>
    </div>
  );
}

const TextItem = ({ q, a }: { q: string; a: string }) => {
  return (
    <div className="my-4">
      <p className="font-bold">{q}</p>
      <p>{a}</p>
    </div>
  );
};
