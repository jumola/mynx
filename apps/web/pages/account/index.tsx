import { User } from '@mynx/data';
import { Input, useAuth } from '@mynx/react-shared';
import { keys, pickBy } from 'lodash';
import { useEffect } from 'react';
import { Badge, Button } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import Layout from '../../components/shared/layout';

const profileInputs = [
  {
    displayName: 'First name',
    name: 'firstName',
  },
  {
    displayName: 'Last name',
    name: 'lastName',
  },
  {
    displayName: 'Email address',
    name: 'email',
  },
  {
    displayName: 'Username',
    name: 'username',
  },
  {
    displayName: 'Password',
    name: 'password',
    type: 'password',
  },
];

const AccountPage = () => {
  const { user } = useAuth();
  const { register, handleSubmit, reset } = useForm();

  const onSubmit = (data: Partial<User>) => {
    //
  };

  useEffect(() => {
    reset(user ?? {});
  }, [user]);

  return (
    <div className="grid grid-flow-col grid-cols-3">
      <div className="p-2">
        {user && <p>{!user.isVip ? 'non-vip' : 'vip'}</p>}

        <div className="py-1">
          <Button variant="info">Learn more about VIP</Button>
        </div>
      </div>

      <div className="p-2">
        <p className="text-2xl">My profile</p>

        <img width="100" src={user?.image} alt={user?.username} />

        <form onSubmit={handleSubmit(onSubmit)}>
          {user &&
            profileInputs.map((field, index) => (
              <div key={index} className="py-1">
                <Input
                  name={field.name}
                  displayName={field.displayName}
                  placeholder={field.displayName}
                  hookRegister={{
                    ...register(field.name),
                  }}
                  type={field?.type}
                />
              </div>
            ))}

          {/* <div className="py-1">
          <Input
            name="Password"
            displayName={field.displayName}
            placeholder={field.displayName}
            hookRegister={{
              ...register(field.name),
            }}
          />
        </div> */}

          <div className="py-1">
            <Button type="submit" variant="info">
              Submit
            </Button>
          </div>
        </form>
      </div>

      <div className="p-2">
        <p className="text-2xl">Payment</p>
        <p>
          We pay via Coinbase by default. Enter the email address linked to your
          Coinbase account below to withdraw your earnings.
        </p>
        <div className="py-1">
          <Input
            name="coinbaseEmail"
            displayName="Coinbase email"
            placeholder="Your Coinbase email"
          />
        </div>

        <div className="py-1">
          <Button variant="info">Request change</Button>
        </div>
      </div>
    </div>
  );
};

export default AccountPage;
