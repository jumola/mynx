import { useForm } from 'react-hook-form';
import Layout from '../../components/shared/layout';
import {
  alertError,
  alertSuccess,
  centeredY,
  getApiErrorMessage,
  Input,
  useAuth,
} from '@mynx/react-shared';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import { isNil } from 'lodash';
import { Button, Image } from 'react-bootstrap';
import { app, appMasterConfig } from '@mynx/environment';

const LoginPage = () => {
  const auth = useAuth();
  const router = useRouter();

  const { register, handleSubmit } = useForm<any>();

  const onSubmit = (formData) => {
    auth
      .signIn(formData)
      .then(() => alertSuccess('Ok!'))
      .catch(async (error) => {
        alertError(await getApiErrorMessage(error));
      });
  };

  const { message } = router.query;
  const [messageShown, setMessageShown] = useState(false);
  useEffect(() => {
    if (!messageShown && !isNil(message)) {
      alertSuccess(message as string);
      setMessageShown(true);
    }
  }, [message, messageShown]);

  return (
    <div className="centered-y text-center">
      <div className="text-4xl font-bold font-special text-purple-900">
        <p>Hey Stranger!</p>
        <p className="text-lg">Login please</p>
      </div>

      <form className="container">
        <Input
          displayName="Email address"
          name="email"
          type="email"
          placeholder="Your email address"
          required={true}
          hookRegister={{
            ...register('email', { required: true }),
          }}
        />

        <Input
          displayName="Password"
          name="password"
          type="password"
          placeholder="Your password"
          required={true}
          hookRegister={{
            ...register('password', { required: true }),
          }}
        />

        <Button className="mt-2" type="button" onClick={handleSubmit(onSubmit)}>
          Login
        </Button>

        <div className="mt-2">
          <a href="/register">
            <p>Don't have an account yet?</p>
          </a>

          <a href="/forgot-password">
            <p className="mt-2">Forgot your password?</p>
          </a>
        </div>

        {/* <div className="d-grid">
                <Button type="button" block onClick={() => signIn('google')}>Login via Google</Button>
              </div> */}
      </form>
    </div>
  );
};

export default LoginPage;
