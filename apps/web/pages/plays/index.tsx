import { useAuth, useMyPlays } from '@mynx/react-shared';
import { MatchStatusText } from '@mynx/types-shared';
import AppNumber from '../../components/shared/app-number';
import { Table } from 'react-bootstrap';
import Layout from '../../components/shared/layout';
import { prettifyDate } from '@mynx/shared-utils';
import { Player } from '@mynx/data';
import PlayModal from '../../components/games/play-modal';
import { useState } from 'react';

const PlaysPage = () => {
  const { user } = useAuth();

  const { data: playsData } = useMyPlays();

  const [playData, setPlayData] = useState<Player | undefined>(undefined);
  const [showPlay, setShowPlay] = useState<boolean>(false);

  const onClick = (play: Player) => {
    setPlayData(play);
    setShowPlay(true);
  };

  return (
    <div className="grid grid-cols-12">
      <div className="col-span-6 col-start-4 offset-1 py-2">
        <div className="overflow-auto">
          <Table striped hover responsive>
            <thead>
              <tr>
                <td>Status</td>
                <td>Coin</td>
                <td>Buy-in</td>
                <td>Prize</td>
                <td>Players</td>
                <td>Played</td>
                <td>Result</td>
              </tr>
            </thead>
            <tbody>
              {playsData?.map((play) => (
                <tr key={`play${play.id}`} onClick={() => onClick(play)}>
                  <td>{MatchStatusText[play.match.status]}</td>
                  <td>{play.match.game.coin.name}</td>
                  <td>
                    <AppNumber
                      value={play.match.game.costAmount}
                      currency="crypto-unit"
                      coin={play.match.game.coin}
                      convert="to-unit"
                    />
                  </td>
                  <td>
                    <AppNumber
                      value={play.match.game.prizeAmount}
                      currency="crypto-unit"
                      coin={play.match.game.coin}
                      convert="to-unit"
                    />
                  </td>
                  <td>{play.match.game.players}</td>
                  <td>{prettifyDate(play.match.concludedAt)}</td>

                  <td>{play.match.winnerId === user?.id ? 'WON' : 'LOST'}</td>
                </tr>
              ))}
            </tbody>
          </Table>

          <div>
            {playData && (
              <PlayModal
                play={playData}
                show={showPlay}
                onClose={() => setShowPlay(false)}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlaysPage;
