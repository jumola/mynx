import WelcomeBonus from '../../components/shared/welcome-bonus';
import { useForm } from 'react-hook-form';
import Layout from '../../components/shared/layout';
import {
  alertError,
  centeredY,
  getApiErrorMessage,
  Input,
  useAuth,
} from '@mynx/react-shared';
import { Button, Image } from 'react-bootstrap';

export default function RegisterPage() {
  const auth = useAuth();
  const { register, handleSubmit } = useForm<any>();

  const onSubmit = (formData) => {
    auth.register(formData).catch(async (error) => {
      alertError(await getApiErrorMessage(error));
    });
  };

  return (
    <div className="centered-y">
      <div className=" text-center">
        <div className="text-4xl font-bold font-special text-purple-900">
          <p>Hey Stranger!</p>
          <p className="text-lg">
            Create a FREE account and claim a Welcome Bonus package
          </p>
        </div>

        <WelcomeBonus />

        <div className="container">
          <form onSubmit={handleSubmit(onSubmit)}>
            <Input
              displayName="First name"
              name="firstName"
              placeholder="Satoshi"
              required={true}
              hookRegister={{
                ...register('firstName', { required: true }),
              }}
            />

            <Input
              displayName="Last name"
              name="lastName"
              placeholder="Nakamoto"
              required={true}
              hookRegister={{
                ...register('lastName', { required: true }),
              }}
            />

            <Input
              displayName="Username"
              name="username"
              placeholder="satoshibtc10"
              required={true}
              hookRegister={{
                ...register('username', { required: true }),
              }}
            />

            <Input
              displayName="Email address"
              type="email"
              name="email"
              placeholder="satoshi@nakamoto.com"
              required={true}
              hookRegister={{
                ...register('email', { required: true }),
              }}
            />

            <Input
              displayName="Password"
              type="password"
              name="password"
              placeholder="Your password"
              required={true}
              hookRegister={{
                ...register('password', { required: true }),
              }}
            />

            <Button className="mt-2" type="submit">
              Submit
            </Button>

            <a href="/login">
              <p className="mt-2">Already have an account?</p>
            </a>
          </form>
        </div>
      </div>
    </div>
  );
}
