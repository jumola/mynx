import Layout from '../../components/shared/layout';
import Games from '../../components/games';

export default function GamesPage() {
  return <Games />;
}
