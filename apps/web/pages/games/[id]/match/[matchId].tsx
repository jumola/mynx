import Layout from '../../../../components/shared/layout';
import GameComponent from '../../../../components/games/game';
import { useRouter } from 'next/dist/client/router';
import { useAuth, useGame, useGameSocket, useMatch } from '@mynx/react-shared';
import { getMatchSocketEventId } from '@mynx/shared-utils';
import { Match } from '@mynx/data';
import { useEffect, useState } from 'react';
import { MatchStatusText } from '@mynx/types-shared';
import GameUser from '../../../../components/games/game-user';

export default function MatchPage() {
  const auth = useAuth();
  const router = useRouter();
  const { id, matchId } = router.query;
  const { socket, socketError } = useGameSocket(auth?.accessToken);

  const [match, setMatch] = useState<Match | undefined>(undefined);
  const [matchSocketId, setMatchSocketId] = useState<string | undefined>(
    undefined
  );

  const { data: gameData } = useGame(Number(id));
  const { data: matchData } = useMatch(Number(matchId));

  useEffect(() => {
    if (matchData) {
      setMatchSocketId(getMatchSocketEventId(matchData));
      setMatch(matchData);

      socket.on(matchSocketId, (data: Match) => setMatch(data));
    }
  }, [matchData, socket, matchSocketId]);

  if (!match) {
    return null;
  }

  return (
    <div className="">
      <div className="grid grid-cols-12 xs:">
        <div className="col-span-2 col-start-6 ">
          <GameComponent data={gameData} />
        </div>
      </div>

      <div className="text-center">
        <p>Players ({matchData?.players?.length}):</p>
        <div className="flex justify-center">
          {match.players.map((p, i) => (
            <GameUser key={i} data={p.user} />
          ))}
        </div>

        <p>Status: {MatchStatusText[match.status]}</p>
      </div>
    </div>
  );
}
