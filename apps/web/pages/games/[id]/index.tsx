import GameComponent from '../../../components/games/game';
import { useRouter } from 'next/dist/client/router';
import {
  useAuth,
  useGame,
  useGameSocket,
  useMyLiveMatches,
} from '@mynx/react-shared';
import { Button } from 'react-bootstrap';

export default function GamePage() {
  const router = useRouter();
  const { id } = router.query;
  const { data: gameData } = useGame(Number(id));
  const auth = useAuth();
  const { socket, socketError } = useGameSocket(auth?.accessToken);

  const { data: matchesData } = useMyLiveMatches();
  const liveGameIds = matchesData?.map((m) => m.gameId) ?? [];
  const isJoined = gameData?.id && liveGameIds.includes(gameData.id);

  return (
    <div className="centered-y">
      {socketError && (
        <div className="alert alert-danger" role="alert">
          {socketError}
        </div>
      )}

      <div className="grid grid-cols-12">
        <div className="col-span-6 col-start-4">
          <GameComponent data={gameData} isJoined={isJoined} />
        </div>
      </div>

      <div className="flex justify-center ">
        <Button
          onClick={() => {
            socket.emit('join', { id }, async (matchId) => {
              router.push(`/games/${id}/match/${matchId}`);
            });
          }}
        >
          Join
        </Button>
      </div>
    </div>
  );
}
