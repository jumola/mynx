import DataUpdatePage from '../../../../components/admin/data/update';
import { useRouter } from 'next/dist/client/router';

// update
export default function Page(data) {
  const router = useRouter();
  const { tableName, id } = router.query;
  if (!tableName || !id) {
    return null;
  }
  return <DataUpdatePage tableName={tableName} id={id} />;
}
