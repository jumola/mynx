import { useRouter } from 'next/dist/client/router';
import { filter, isObject, keys, omit, reduce } from 'lodash';
import DataCreatePage from '../../../../components/admin/data/create';

// create
export default function Page(data) {
  const router = useRouter();
  const { tableName } = router.query;
  if (!tableName) {
    return null;
  }

  let objKeys = [];

  switch (tableName) {
    case 'coin':
      objKeys = ['name', 'longName', 'unitName', 'coinbaseAssetId'];
      break;
    case 'game':
      objKeys = ['name', 'players', 'coinId'];
      break;
    default:
  }

  const keysArrayToObject = (keysArr: string[], defaultValue = undefined) =>
    reduce(keysArr, (sum, value, index, array) => {
      const obj = isObject(sum) ? sum : {};
      if (index === 1) {
        obj[sum] = defaultValue;
      }
      obj[value] = defaultValue;
      return obj;
    });

  return (
    <DataCreatePage
      defaultValues={keysArrayToObject(objKeys)}
      tableName={tableName}
    />
  );
}
