import DataListPage from '../../../../components/admin/data/list';
import { useRouter } from 'next/dist/client/router';

// list
export default function Page(data) {
  const router = useRouter();
  const { tableName } = router.query;
  if (!tableName) {
    return null;
  }
  return <DataListPage tableName={tableName} queryName={`${tableName}s`} />;
}
