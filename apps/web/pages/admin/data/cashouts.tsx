import CustomPagination from '../../../components/shared/pagination';
import Layout from '../../../components/shared/layout';
import { useAdminCashouts } from '@mynx/react-shared';
import { convertCryptoToUSDText, prettifyDate } from '@mynx/shared-utils';
import AdminUserStats from '../../../components/admin/user/user-stats';

const AdminCashoutsPage = () => {
  const { data } = useAdminCashouts();

  const onPageChange = () => {
    console.log('onPageChange');
  };

  return (
    <div>
      <p className="text-lg">
        <b>Cashout Requests</b>
      </p>

      <p>Total requests: {data?.meta?.totalItems}</p>

      <div className="table-responsive">
        <table className="table table-striped table-bordered">
          <thead>
            <tr>
              <th>User</th>
              <th>Stats</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            {data?.items?.map((t) => (
              <tr key={t.id}>
                <td>
                  <div className="text-sm">
                    <p>{t.user.username}</p>
                    <p>
                      Registered: {prettifyDate(t.user.createdAt, 'yyyy-MM-dd')}
                    </p>
                  </div>
                </td>
                <td>
                  <AdminUserStats userId={t.userId} />
                </td>
                <td>
                  <p>
                    {t.amount} {t.coin.name}
                  </p>
                  <p>${convertCryptoToUSDText(t.coin, t.amount)}</p>
                </td>
              </tr>
            ))}
          </tbody>
        </table>

        <CustomPagination meta={data?.meta} onPageChange={onPageChange} />
      </div>
    </div>
  );
};

export default AdminCashoutsPage;
