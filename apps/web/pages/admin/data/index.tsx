import { useAdminDashboard, useAuth } from '@mynx/react-shared';
import { Button } from 'react-bootstrap';
import Layout from '../../../components/shared/layout';

export default function Index() {
  const auth = useAuth();

  const { data } = useAdminDashboard();

  const dataObj = [
    {
      title: 'Users',
      total: data?.users,
      path: '/admin/data/user',
    },
    {
      title: 'Games',
      total: data?.games,
      path: '/admin/data/game',
    },
    {
      title: 'Coins',
      total: data?.coins,
      path: '/admin/data/coin',
    },
    {
      title: 'Winners today',
      total: data?.winners,
      path: '/admin/data/winner',
    },
    {
      title: 'Payable Today',
      total: 4,
      path: '/admin/data/cashout',
    },
  ];

  return (
    <Layout
      mainContent={
        <div className="p-2">
          <div className="grid grid-cols-12">
            {dataObj.map((d, i) => (
              <div key={i} className="col">
                <div className="card">
                  <div className="card-body">
                    <h5 className="card-title">{d.total}</h5>
                    <p className="card-text">{d.title}</p>

                    <a href={d.path}>
                      <Button>View</Button>
                    </a>
                  </div>
                </div>
              </div>
            ))}

            <div className="col">
              <div className="card">
                <div className="card-body">
                  <h5 className="card-title">View</h5>
                  <p className="card-text">Cashouts</p>

                  <a href="/admin/data/cashouts">
                    <Button>View</Button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      }
    />
  );
}
