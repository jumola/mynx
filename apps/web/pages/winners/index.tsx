import Layout from '../../components/shared/layout';
import LatestWinners from '../landing/latest-winners';

const Winners = () => <LatestWinners />;

export default Winners;
