import { Image } from 'react-bootstrap';
import { app } from '@mynx/environment';
import { DateTime } from 'luxon';

export default function TermsPage() {
  return (
    <div>
      <div className="text-center">
        <Image className="inline h-80" src="/images/ud-privacy.png" />
        <p className="text-xl font-bold">F.A.Q.</p>
        <p>Your privacy is important to us!</p>
      </div>

      <div className="my-4">
        <div className="my-2">
          <p className="font-bold">Terms</p>
          <ul className="list-inside list-disc">
            <li>
              By accessing the {app.appName} website, you are agreeing to be
              bound by these terms of service, all applicable laws and
              regulations, and agree that you are responsible for compliance
              with any applicable local laws. If you do not agree with any of
              these terms, you are prohibited from using or accessing this site.
              The materials contained in this website are protected by
              applicable copyright and trademark law.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Use License</p>
          <ul className="list-inside list-disc">
            <li>
              This license shall automatically terminate if you violate any of
              these restrictions and may be terminated by {app.appName} at any
              time. Upon terminating your viewing of these materials or upon the
              termination of this license, you must destroy any downloaded
              materials in your possession whether in electronic or printed
              format.
            </li>
            <li>
              Permission is granted to temporarily download one copy of the
              materials (information or software) on {app.appName}' website for
              personal, non-commercial transitory viewing only. This is the
              grant of a license, not a transfer of title, and under this
              license you may not:
            </li>
            <ul className="list-inside list-disc ml-2">
              <li>modify or copy the materials;</li>
              <li>
                use the materials for any commercial purpose, or for any public
                display (commercial or non-commercial);
              </li>
              <li>
                attempt to decompile or reverse engineer any software contained
                on {app.appName}' website;
              </li>
              <li>
                remove any copyright or other proprietary notations from the
                materials; or
              </li>
              <li>
                transfer the materials to another person or "mirror" the
                materials on any other server.
              </li>
            </ul>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Disclaimer</p>
          <ul className="list-inside list-disc">
            <li>
              The materials on {app.appName}' website are provided on an 'as is'
              basis. {app.appName} makes no warranties, expressed or implied,
              and hereby disclaims and negates all other warranties including,
              without limitation, implied warranties or conditions of
              merchantability, fitness for a particular purpose, or
              non-infringement of intellectual property or other violation of
              rights.
            </li>
            <li>
              Further, {app.appName} does not warrant or make any
              representations concerning the accuracy, likely results, or
              reliability of the use of the materials on its website or
              otherwise relating to such materials or on any sites linked to
              this site.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Limitations</p>
          <ul className="list-inside list-disc">
            <li>
              In no event shall {app.appName} or its suppliers be liable for any
              damages (including, without limitation, damages for loss of data
              or profit, or due to business interruption) arising out of the use
              or inability to use the materials on {app.appName}' website, even
              if
              {app.appName} or a {app.appName} authorized representative has
              been notified orally or in writing of the possibility of such
              damage. Because some jurisdictions do not allow limitations on
              implied warranties, or limitations of liability for consequential
              or incidental damages, these limitations may not apply to you.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Accuracy of materials</p>
          <ul className="list-inside list-disc">
            <li>
              The materials appearing on {app.appName}' website could include
              technical, typographical, or photographic errors. {app.appName}{' '}
              does not warrant that any of the materials on its website are
              accurate, complete or current. {app.appName} may make changes to
              the materials contained on its website at any time without notice.
              However
              {app.appName} does not make any commitment to update the
              materials.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Links</p>
          <ul className="list-inside list-disc">
            <li>
              {app.appName} has not reviewed all of the sites linked to its
              website and is not responsible for the contents of any such linked
              site. The inclusion of any link does not imply endorsement by{' '}
              {app.appName} of the site. Use of any such linked website is at
              the user's own risk.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Modifications</p>
          <ul className="list-inside list-disc">
            <li>
              {app.appName} may revise these terms of service for its website at
              any time without notice. By using this website you are agreeing to
              be bound by the then current version of these terms of service.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p className="font-bold">Governing Law</p>
          <ul className="list-inside list-disc">
            <li>
              These terms and conditions are governed by and construed in
              accordance with the laws of United States and you irrevocably
              submit to the exclusive jurisdiction of the courts in that State
              or location.
            </li>
          </ul>
        </div>

        <div className="my-2">
          <p>Last update: 1 November {DateTime.local().toFormat('yyyy')}</p>
        </div>
        {/* end */}
      </div>
    </div>
  );
}
