import { Bank, Coin, User } from '@mynx/data';
import { Input, useAuth, useUserSocket } from '@mynx/react-shared';
import { getUserSocketEventId } from '@mynx/shared-utils';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState, useMemo } from 'react';
import Layout from '../../components/shared/layout';
import { first, keys, orderBy, startCase } from 'lodash';
import DataTable from 'react-data-table-component';
import { Button } from 'react-bootstrap';
import CoinImage from '../../components/shared/coin-image';
import AppNumber from '../../components/shared/app-number';

export default function WalletPage() {
  const { user, accessToken } = useAuth();
  const router = useRouter();

  const { socket } = useUserSocket(accessToken);

  const [banks, setBanks] = useState<Bank[]>([]);

  useEffect(() => {
    if (socket && user?.id) {
      socket.on(getUserSocketEventId(user), (user: User) => {
        setBanks(user.banks);
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [socket]);

  useEffect(() => {
    if (user?.banks) {
      setBanks(user.banks);
    }
  }, [user]);

  const [keyword, setKeyword] = useState<string | undefined>(undefined);
  const table = {
    columns: [
      {
        name: 'Asset',
        cell: (row) => <AssetCell row={row} />,
      },
      {
        name: 'Balance',
        cell: (row) => <BalanceCell row={row} />,
      },
      {
        cell: (row) => (
          <Button size="sm" onClick={() => router.push(`/wallet/${row.id}`)}>
            View
          </Button>
        ),
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
    ],
    data: orderBy(
      banks.filter((d) => JSON.stringify(d).includes(keyword ?? '')),
      ['amount'],
      'desc'
    ),
  };

  return (
    <div className="w-2/4 mx-auto">
      <div className="grid grid-cols">
        <div className="w-full overflow-x-scroll">
          <div className="w-1/5">
            <Input
              name=""
              placeholder="Search coin"
              onChange={(e) => setKeyword(e.target.value)}
            />
          </div>
          <DataTable columns={table.columns} data={table.data} />
        </div>
      </div>
    </div>
  );
}

const AssetCell = ({ row }: { row: Bank }) => {
  return (
    <div className="w-full">
      <div className="grid grid-cols-12 p-2 ">
        <div className="col-span-3">
          <CoinImage data={row.coin} />
        </div>
        <div className="col-span-9">
          <p>{row.coin.longName}</p>
          <p className="text-lg text-gray-400">{row.coin.name}</p>
        </div>
      </div>
    </div>
  );
};

const BalanceCell = ({ row }: { row: Bank }) => {
  return (
    <div className="py-2">
      <div className="text-lg">
        <AppNumber
          value={row.amount}
          currency="usd"
          convert="to-usd"
          coin={row.coin}
        />
      </div>
      <div>
        <AppNumber value={row.amount} currency="crypto" coin={row.coin} />
      </div>

      <div className="text-gray-400">
        <AppNumber
          value={row.amount}
          currency="crypto-unit"
          convert="to-unit"
          coin={row.coin}
        />
      </div>
    </div>
  );
};
