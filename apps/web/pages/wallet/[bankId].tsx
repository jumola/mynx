import {
  alertError,
  alertSuccess,
  fetchWithdraw,
  getApiErrorMessage,
  useAuth,
  useMyBankDepositAddress,
  useMyTransactions,
  useUserSocket,
} from '@mynx/react-shared';
import {
  convertCryptoToUSDText,
  getUserSocketEventId,
} from '@mynx/shared-utils';
import { useRouter } from 'next/dist/client/router';
import { useEffect, useState } from 'react';
import Layout from '../../components/shared/layout';
import { Button } from 'react-bootstrap';
import SendModal from '../../components/wallet/send-modal';
import WalletHistory from '../../components/wallet/wallet-history';
import { Bank, User } from '@mynx/data';
import BigNumber from 'bignumber.js';
import AppNumber from '../../components/shared/app-number';
import CoinImage from '../../components/shared/coin-image';

const Content = () => {
  const auth = useAuth();
  const router = useRouter();
  const { bankId } = router.query;

  const bankData = auth?.user?.banks?.find((b) => String(b.id) === bankId);

  const [bank, setBank] = useState<Bank | undefined>(bankData);

  const { socket } = useUserSocket(auth?.accessToken);

  const [page, setPage] = useState<number>(1);
  const [amountUsd, setAmountUsd] = useState<string | undefined>(undefined);

  const { data: transsactionsData, refetch } = useMyTransactions(page);

  const [showSend, setShowSend] = useState(false);

  const { data: bankDepositAddressData } = useMyBankDepositAddress(
    bank?.coinId
  );

  useEffect(() => {
    if (bankData && !bank) {
      setBank(bankData);
    }

    if (bank) {
      setAmountUsd(convertCryptoToUSDText(bank.coin, bank.amount));
    }

    if (page) {
      refetch();
    }

    if (socket && auth?.user && bank) {
      socket.on(getUserSocketEventId(auth.user), (user: User) => {
        setBank(user.banks.find((b) => b.id === bank.id));
      });
    }
  }, [bank, bankData, page, refetch, socket, auth]);

  const handlePageChange = ({ selected }) => {
    setPage(selected + 1);
  };

  const onWithdraw = () => {
    fetchWithdraw(bank.id)
      .then((data: Bank) => {
        setBank(data);
        alertSuccess('Success');
      })
      .catch(async (error) => alertError(await getApiErrorMessage(error)));
  };

  const canActOnFunds = () => new BigNumber(bank.amount).isGreaterThan(0);

  return !bank ? null : (
    <div>
      {/* <div className="grid grid-flow-col grid-cols-3">
        <div>
          <p>USD</p>
          <p className="text-4xl	">
            <sup className="text-sm">$</sup>
            <b>{amountUsd}</b>
          </p>

          <p>
            <AppNumber value={bank.amount} coin={bank.coin} />
          </p>
        </div>

        <div className="text-center">
          {bankDepositAddressData && (
            <>
              <p>Wallet address:</p>
              <p>{bankDepositAddressData.coinbaseAddress}</p>
              <img
                height="25"
                width="25"
                src="http://localhost:8000/assets/icon-coinbase.png"
                alt="Coinbase logo"
                className="inline"
              />
              <p>
                <small>Powered by Coinbase</small>
              </p>
            </>
          )}
        </div>
      </div> */}

      {transsactionsData && (
        <>
          <div className="flex border-t-2 border-l-2 border-r-2 border-gray-200 p-2">
            <div className="flex-1 p-2">
              <CoinImage size="100%" data={bank?.coin} />
            </div>
            <div className="flex-1">
              <div className="font-bold">
                Balance:{' '}
                <AppNumber
                  value={bank?.amount}
                  currency="usd"
                  convert="to-usd"
                  coin={bank?.coin}
                />{' '}
                (
                <AppNumber
                  value={bank?.amount}
                  currency="crypto"
                  coin={bank?.coin}
                />
                )
              </div>
              <div className="text-gray-400">
                <AppNumber
                  value={bank?.amount}
                  currency="crypto-unit"
                  convert="to-unit"
                  coin={bank?.coin}
                />
              </div>
            </div>
            <div className="flex-1 text-right">
              <Button
                size="sm"
                onClick={() => setShowSend(true)}
                disabled={!canActOnFunds()}
                className="m-1"
              >
                Send
              </Button>

              <Button
                size="sm"
                onClick={onWithdraw}
                disabled={!canActOnFunds()}
                className="m-1"
              >
                Withdraw
              </Button>
            </div>
          </div>

          <WalletHistory
            data={transsactionsData.items}
            meta={transsactionsData.meta}
            onPageChange={handlePageChange}
          />
        </>
      )}

      {bank && (
        <SendModal
          bank={bank}
          show={showSend}
          onClose={() => {
            setShowSend(false);
          }}
        />
      )}
    </div>
  );
};

export default function Page() {
  return (
    <div className="w-full md:w-2/4 mx-auto">
      <Content />
    </div>
  );
}
