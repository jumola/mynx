import {
  alertError,
  alertSuccess,
  fetchMeClaimBonus,
  getApiErrorMessage,
} from '@mynx/react-shared';

export class MeService {
  async claimWelcomeBonus() {
    try {
      await fetchMeClaimBonus();
      alertSuccess('Yay!');
    } catch (error) {
      alertError(await getApiErrorMessage(error));
    }
  }
}
