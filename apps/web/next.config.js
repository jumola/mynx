/* eslint-disable @typescript-eslint/no-unused-vars */
// eslint-disable-next-line @typescript-eslint/no-var-requires
const withNx = require('@nrwl/next/plugins/with-nx');
// const withPWA = require('next-pwa');
const withTM = require('next-transpile-modules')([
  '@ionic/react', // need to override some values for SSR support
  'ky', // in esm format only
]);
const withPlugins = require('next-compose-plugins');

/**
 * @type {import('next/dist/next-server/server/config').NextConfig}
 **/
const nextConfig = {
  future: {
    webpack5: true,
  },
  reactStrictMode: true,

  nx: {
    // Set this to false if you do not want to use SVGR
    // See: https://github.com/gregberge/svgr
    svgr: true,
  },

  // May be needed for twin.macro support? https://github.com/ben-rogerson/twin.examples/tree/master/next-emotion#add-the-next-config
  webpack: (
    /** @type {import('webpack').Configuration} */ config,
    { isServer, webpack: _webpack }
  ) => {
    /** @type {import('webpack')} */
    const webpack = _webpack;

    if (isServer) {
      // fix server execution of @ionic/react
      config.module.rules.push({
        test: require.resolve('@ionic/react'),
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: [
                [
                  'babel-plugin-transform-define',
                  {
                    HTMLElement: null,
                    window: undefined,
                  },
                ],
              ],
            },
          },
        ],
      });
    } else {
      // Unset client-side javascript that only works server-side
      // https://github.com/vercel/next.js/issues/7755#issuecomment-508633125
      // config.resolve.fallback = { fs: false, module: false };
    }

    // console.log(config.module.rules[3]);
    return config;
  },

  // exportPathMap: async () => {
  //   return {
  //     '/': { page: '/[[...all]]' },
  //   };
  // },
};

module.exports = withPlugins([withNx, withTM], nextConfig);

// withPWA,

// pwa: {
//   // For now, always disable the PWA
//   // I would like to be able to simulate an app-like experience really well
//   // from the browser. However, for testing purposes, we don't want old versions
//   // to be cached. So it may be better to force the PWA mode to always fetch files
//   // from the server. I think we can achieve this without next-PWA?
//   disable: true,
//   // disable: !BUILD_WITH_PWA,
//   dest: 'public',
//   skipWaiting: false,
// },

// const BUILD_WITH_PWA = Boolean(process.env.BUILD_WITH_PWA || false);

// console.log(`BUILD_WITH_PWA=${BUILD_WITH_PWA}`);
// if (BUILD_WITH_PWA) {
//   console.log(
//     `Will build with PWA features (for web preview or other testing)`
//   );
// } else {
//   console.log(`Will build without PWA features (for native app build)`);
// }
