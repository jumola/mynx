const { join } = require('path');
const { createGlobPatternsForDependencies } = require('@nrwl/next/tailwind');

module.exports = {
  presets: [require('../../tailwind-workspace-preset.js')],
  purge: [
    // join(__dirname, 'pages/**/*.{js,ts,jsx,tsx}'),
    // ...createGlobPatternsForDependencies(__dirname),
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    fontFamily: {
      custom: [
        'Lato',
        'Lato Italic',
        'Lato Bold',
        'Lato Bold Italic',
        'sans-serif',
      ],
      special: ['LeckerliOne', 'sans-serif'],
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
  prefix: '',
};
