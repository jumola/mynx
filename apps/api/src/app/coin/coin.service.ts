import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coin } from '@mynx/data';
import { Repository } from 'typeorm';

@Injectable()
export class CoinService {
  constructor(
    @InjectRepository(Coin)
    private readonly repo: Repository<Coin>
  ) {}

  count() {
    return this.repo.count();
  }

  findAll(): Promise<Coin[]> {
    return this.repo.find();
  }

  findOne(id: number): Promise<Coin> {
    return this.repo.findOne(id);
  }

  async findAllWithParams(params: Partial<Coin>): Promise<Coin[]> {
    return await this.repo.find({ where: params });
  }

  async findOneWithParams(params: Partial<Coin>): Promise<Coin> {
    return await this.repo.findOne(params);
  }

  create(body): Promise<Coin> {
    return this.repo.save(body);
  }

  update(id: number, body) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  // custom methods
  actives() {
    return this.findAllWithParams({
      isActive: true,
    });
  }

  featureds() {
    return this.findAllWithParams({
      isFeatured: true,
      isActive: true,
    });
  }
}
