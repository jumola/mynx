import { Module } from '@nestjs/common';
import { CoinService } from './coin.service';
import { CoinController } from './coin.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Coin } from '@mynx/data';

@Module({
  imports: [TypeOrmModule.forFeature([Coin])],
  controllers: [CoinController],
  providers: [CoinService],
  exports: [TypeOrmModule, CoinService],
})
export class CoinModule {}
