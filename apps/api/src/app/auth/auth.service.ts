import {
  ConflictException,
  HttpStatus,
  HttpException,
  Injectable,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { WsException } from '@nestjs/websockets';
import * as jwt from 'jsonwebtoken';
import randomstring from 'randomstring';
import { get, omit } from 'lodash';
import { User, UserVerification } from '@mynx/data';
import { getGoogleInfo, getGoogleTokenFromCode } from '@mynx/nest-utils';
import { app, authConfig } from '@mynx/environment';
import {
  GoogleAuthUser,
  UserType,
  UserVerificationType,
} from '@mynx/types-shared';
import { UserService } from '../user/user.service';
import { UserReferralService } from '../user-referral/user-referral.service';
import { MailService } from '../mail/mail.service';
@Injectable()
export class AuthService {
  constructor(
    private mailService: MailService,
    private userService: UserService,
    private userReferralService: UserReferralService
  ) {}

  async signInViaGoogle(code: string, req, res): Promise<User> {
    const data: any = await getGoogleTokenFromCode(code);
    if (!data?.data?.access_token) {
      throw new Error('no google access token');
    }
    const googleUser: GoogleAuthUser = await getGoogleInfo(
      data?.data?.access_token
    );

    if (!googleUser) {
      throw new Error('no user found');
    }

    // we can only accept verified-email users
    if (!googleUser?.verified_email) {
      throw new Error('unverified Gmail error');
    }

    // now we want to either sign up or in this user
    // the match is in the email address

    // first, let's find if there's an existing user with this email address
    let user: User = await this.userService.findOneWithParams({
      email: googleUser.email,
    });

    if (!user) {
      // the user doesn't exist  = sign the user up
      const userParams = {
        firstName: googleUser.given_name,
        lastName: googleUser.family_name,
        email: googleUser.email,
        username: googleUser.email.replace(/[^A-Za-z0-9]/g, ''),
        password: randomstring.generate(),
      };

      user = await this.signUp(req, userParams);
    }

    return user;
  }

  async signIn(user: User) {
    return {
      accessToken: this.jwtGenerateToken(user),
    };
  }

  async signUp(req, body: Partial<User>): Promise<User> {
    try {
      const userDoc = {
        ...body,
        password: await bcrypt.hash(body.password, 10),
        type: UserType.Normal,
        image: this.userService.generateAvatar(body as User),
      };

      userDoc.referralBannerAdFileName =
        this.userReferralService.getReferralBannerFilename(userDoc as User);

      const newUser = await this.userService.create(userDoc);

      // handle referral. this is always valid if set (?)
      const senderId = this.getReferredByIdCookie(req);
      if (senderId) {
        await this.userReferralService.createUserReferral(senderId, newUser.id);
      }

      await this.mailService.welcome(newUser);

      return newUser;
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('User already exists');
      }
      throw error;
    }
  }

  getReferredByIdCookie(
    // making optional for jest mocking
    req?: any
  ) {
    return get(req?.cookies ?? {}, 'referredById');
  }

  async validateUser(email: string, pass: string) {
    const user = await this.userService.findOneWithParams({
      email,
    });

    if (!user) {
      return null;
    }

    const valid = await bcrypt.compare(pass, user.password);

    if (valid) {
      return user;
    }

    return null;
  }

  jwtGenerateToken(user: User): string {
    const payload = omit({ ...user, sub: user.id }, 'image');
    return jwt.sign(payload, authConfig.jwtSecret);
  }

  async jwtVerifyToken(token: string, isWs: boolean): Promise<User | null> {
    try {
      const payload = <any>jwt.verify(token, authConfig.jwtSecret);
      const user = await this.userService.findOne(payload.sub);

      if (!user) {
        if (isWs) {
          throw new WsException('Unauthorized access');
        } else {
          throw new HttpException(
            'Unauthorized access',
            HttpStatus.BAD_REQUEST
          );
        }
      }

      return user;
    } catch (err) {
      if (isWs) {
        throw new WsException(err.message);
      } else {
        throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
      }
    }
  }

  async getUserFromHandshake(socket) {
    if (socket?.handshake?.query?.token) {
      const result = await this.jwtVerifyToken(
        socket?.handshake?.query?.token,
        true
      );
      return result;
    } else {
      return null;
    }
  }

  /**
   * allows user to reset his password
   * - user requests to be sent a recovery link to the email address he provided
   * - user gets link which API validates if a valid request
   *  - user's password is updated with a new ranomd one and is sent to his email
   *  TODO: future upgrade
   *  - if valid request, user is sent to a client-side page where he sets his new password
   *  - after setting new password, user is sent back to the login page
   * @param email
   */
  async sendRecoveryLink(email: string): Promise<void> {
    const user = await this.userService.findOneWithParams({
      email,
      isBanned: false,
      isEmailVerified: true,
    });

    if (user) {
      let userVerification: UserVerification =
        await this.userService.findUserVerification({
          userId: user.id,
          type: UserVerificationType.PasswordReset,
          isUsed: false,
        });

      if (!userVerification) {
        userVerification = await this.userService.createUserVerification(
          user.id,
          UserVerificationType.PasswordReset
        );
      }

      await this.mailService.passwordReset(user, userVerification);
    }
  }

  async handleVerify(res: any, secret: string, type: UserVerificationType) {
    const userVerification = await this.userService.findUserVerification({
      secret,
      type,
      isUsed: false,
    });

    if (!userVerification) {
      return res.redirect(app.clientUrl);
    }

    if (type === UserVerificationType.AccountVerification) {
      await this.userService.markAsVerified(
        userVerification.user,
        userVerification
      );
      return res.redirect(
        `${app.clientUrl}/login?message=Congratulations! You may now login to your account.`
      );
    } else if (type === UserVerificationType.PasswordReset) {
      await this.handleVerifiedPasswordReset(userVerification);
      return res.redirect(
        `${app.clientUrl}/login?message=Great! We sent you a new temporary password you can use to login.`
      );
    }
  }

  private async handleVerifiedPasswordReset(
    userVerification: UserVerification
  ) {
    await this.userService.updateUserVerification(userVerification.id, {
      isUsed: true,
    });

    await this.mailService.sendNewPassword(
      userVerification.user,
      await this.userService.setNewPassword(userVerification.userId)
    );
  }
}
