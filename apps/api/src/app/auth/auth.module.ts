import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './strategies/jwt-auth.strategy';
import { LocalStrategy } from './strategies/local.strategy';
import { authConfig } from '@mynx/environment';
import { UserModule } from '../user/user.module';
import { UserReferralModule } from '../user-referral/user-referral.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    PassportModule.register({
      defaultStrategy: 'jwt',
    }),
    JwtModule.register({
      secret: authConfig.jwtSecret,
      signOptions: { expiresIn: `1d` },
    }),
    MailModule,
    UserModule,
    UserReferralModule,
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
