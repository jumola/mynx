import {
  Body,
  Controller,
  Get,
  Res,
  Post,
  Request,
  UseGuards,
  ValidationPipe,
  UnauthorizedException,
  UsePipes,
  Req,
  Param,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { RolesGuard } from './guards/roles.guard';
import { getGoogleLoginURL } from '@mynx/nest-utils';
import { app } from '@mynx/environment';
import { Roles } from './decorators/roles.decorator';
import { AuthRegisterValidation } from '../pipes/auth/register';
import { UserRole, UserVerificationType } from '@mynx/types-shared';
import { AuthPasswordResetValidation } from '../pipes/auth/password-reset';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  // @Get('google')
  google(@Res() res) {
    return res.redirect(getGoogleLoginURL());
  }

  // @Get('googled')
  async googled(@Request() req, @Res() res) {
    const query = req.query;
    const { code } = query;
    const user = await this.authService.signInViaGoogle(code, req, res);
    const { accessToken } = await this.authService.signIn(user);
    const finalURL = `${app.clientUrl}/token/${accessToken}`;
    return res.redirect(finalURL);
  }

  @Post('register')
  @UsePipes(AuthRegisterValidation)
  async register(@Req() req, @Body(ValidationPipe) body) {
    return this.authService.signIn(await this.authService.signUp(req, body));
  }

  @UseGuards(LocalAuthGuard)
  @Post('login')
  signIn(@Request() req) {
    return this.authService.signIn(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('check-user')
  check(@Request() req) {
    if (!req.user) {
      throw new UnauthorizedException('not valid');
    }
    return Boolean(req?.user);
  }

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Post('check-admin')
  checkAdmin(@Request() req) {
    if (req.user?.role !== UserRole.Admin) {
      throw new UnauthorizedException('not valid');
    }
    return Boolean(req?.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('logout')
  logout() {
    return true;
  }

  @Get('verify/password-reset/:secret')
  verifyPasswordReset(@Param('secret') secret: string, @Res() res) {
    return this.authService.handleVerify(
      res,
      secret,
      UserVerificationType.PasswordReset
    );
  }

  @Get('verify/:secret')
  verify(@Param('secret') secret: string, @Res() res) {
    return this.authService.handleVerify(
      res,
      secret,
      UserVerificationType.AccountVerification
    );
  }

  @Post('send-recovery-link')
  @UsePipes(AuthPasswordResetValidation)
  sendRecoveryLink(@Body(ValidationPipe) body) {
    const { email } = body;
    return this.authService.sendRecoveryLink(email);
  }
}
