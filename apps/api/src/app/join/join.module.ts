import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User, UserReferral } from '@mynx/data';
import { JoinController } from './join.controller';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([User, UserReferral]), UserModule],
  controllers: [JoinController],
  providers: [],
  exports: [TypeOrmModule],
})
export class JoinModule {}
