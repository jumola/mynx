import { Controller, Get, Param, Res } from '@nestjs/common';
import { UserService } from '../user/user.service';

@Controller('join')
export class JoinController {
  constructor(private readonly userService: UserService) {}

  @Get(':username')
  async findOne(
    @Param('username') username: string,
    @Res({ passthrough: true }) response
  ) {
    return this.userService.join(response, username);
  }
}
