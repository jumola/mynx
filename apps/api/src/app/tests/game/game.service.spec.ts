import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from '../../game/game.service';
import { v4 as uuidv4 } from 'uuid';
import { GameModule } from '../../game/game.module';
import { AppModule } from '../../app.module';
import BigNumber from 'bignumber.js';
import { first } from 'lodash';
import { CoinService } from '../../coin/coin.service';
import { CoinModule } from '../../coin/coin.module';
import { Game } from '@mynx/data';

describe('GameService', () => {
  let service: GameService;
  let serviceCoin: CoinService;
  let app: AppModule;

  const random = uuidv4();
  const sharedParams = {
    name: random,
    players: 10,
    costAmount: new BigNumber(1),
    prizeAmount: new BigNumber(1),
    potAmount: new BigNumber(1),
  } as Partial<Game>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, GameModule, CoinModule],
      providers: [GameService, CoinService],
    }).compile();

    service = module.get<GameService>(GameService);
    serviceCoin = module.get<CoinService>(CoinService);
    app = module.get<AppModule>(AppModule);

    const newCoin = first(await serviceCoin.findAll());
    sharedParams.coinId = newCoin.id;
  });

  afterAll(() => {
    app.connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create and find', async () => {
    const createResponse = await service.create(sharedParams);
    const response = await service.findOne(createResponse.id);
    expect(response.id).toBeDefined();
  });

  it('should update and read', async () => {
    const createResponse = await service.create(sharedParams);
    const newName = `${createResponse}-updated`;
    await service.update(createResponse.id, {
      ...sharedParams,
      name: newName,
    });

    const findResponse = await service.findOne(createResponse.id);
    expect(findResponse.name).toEqual(newName);
  });

  it('should create and delete', async () => {
    const createResponse = await service.create(sharedParams);
    await service.remove(createResponse.id);
    const findResponse = await service.findOne(createResponse.id);
    expect(findResponse).toBeUndefined();
  });
});
