import { Coin, newFakeUserWithToken, Transaction, User } from '@mynx/data';
import { app } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { CoinbaseHookType } from '@mynx/types';
import { newPaymentData } from './test-data/new-payment';
import { v4 as uuidv4 } from 'uuid';
import { find, pick } from 'lodash';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import { Pagination } from 'nestjs-typeorm-paginate';

describe('hook tests', () => {
  let coins: Coin[];
  let user: User;
  let accessToken: string;

  beforeAll(async () => {
    coins = (await request('GET', `${app.secureApiBaseUrl}/coin`))
      ?.data as Coin[];

    coins = coins.filter((c) => c.isActive);
    expect(coins.length).toBeGreaterThan(0);
  });

  beforeEach(async () => {
    const { accessToken: token } = await newFakeUserWithToken();

    user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken: token,
      })
    )?.data;

    accessToken = token;
  });

  it('should send hook successfully', async () => {
    expect(async () => {
      await request(
        'POST',
        `${app.secureApiBaseUrl}/hook/cb-updates`,
        newPaymentData
      );
    }).not.toThrow();
  });

  it('should create an exchanging deposit', async () => {
    const btc = find(coins, (c) => c.name.toLowerCase() === 'btc');

    const selectedBank = find(user.banks, (b) => b.coinId === btc.id);
    const depositTxn: Transaction = (
      await request(
        'GET',
        `${app.secureApiBaseUrl}/me/deposit-address/${selectedBank.coinId}`,
        undefined,
        {
          accessToken,
        }
      )
    )?.data;

    expect(depositTxn.status).toEqual(TransactionStatus.Waiting);
    expect(depositTxn.activityType).toEqual(TransactionActivityType.Deposit);
    expect(depositTxn.coinId).toEqual(selectedBank.coinId);
    expect(depositTxn.type).toEqual(TransactionType.In);
    expect(depositTxn.userId).toEqual(user.id);
    expect(depositTxn.coinbaseAddress).toBeDefined();
    expect(depositTxn.coinbaseHash).toBeNull();

    newPaymentData.type = CoinbaseHookType.NewPayment;
    newPaymentData.data.address = depositTxn.coinbaseAddress;
    newPaymentData.account.id = btc.coinbaseAccountId;
    newPaymentData.additional_data.transaction.id = uuidv4();
    newPaymentData.additional_data.hash = uuidv4();
    newPaymentData.additional_data.amount.amount = '100';

    await request(
      'POST',
      `${app.secureApiBaseUrl}/hook/cb-updates`,
      newPaymentData
    );

    const transactions: Pagination<Transaction> = (
      await request(
        'GET',
        `${app.secureApiBaseUrl}/me/transactions?limit=9999`,
        undefined,
        {
          accessToken,
        }
      )
    )?.data;

    expect(transactions.items.length).toBeGreaterThan(0);

    const depositTxn2 = transactions.items.find((t) => t.id === depositTxn.id);
    expect(depositTxn2).toBeDefined();

    expect(
      pick(depositTxn2, [
        'coinbaseTransactionId',
        'coinbaseHash',
        'amount',
        'status',
      ])
    ).toEqual({
      coinbaseTransactionId: newPaymentData.additional_data.transaction.id,
      coinbaseHash: newPaymentData.additional_data.hash,
      amount: newPaymentData.additional_data.amount.amount,
      status: TransactionStatus.Exchanging,
    });
  });
});
