import { fakeUser, newFakeUserWithToken, User, UserReferral } from '@mynx/data';
import { Test, TestingModule } from '@nestjs/testing';
import { chunk, times } from 'lodash';
import { AppModule } from '../../app.module';
import { CoinService } from '../../coin/coin.service';
import { UserModule } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserReferralService } from '../../user-referral/user-referral.service';
import { app as appConfig } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import * as fs from 'fs';

describe('UserReferralService tests', () => {
  let userReferralService: UserReferralService;
  let userService: UserService;
  let coinService: CoinService;
  let app: AppModule;

  let user: User;
  let accessToken: string;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, UserModule, UserReferralModule],
      providers: [UserService, UserReferralService],
    }).compile();

    coinService = module.get<CoinService>(CoinService);
    userService = module.get<UserService>(UserService);
    userReferralService = module.get<UserReferralService>(UserReferralService);
    app = module.get<AppModule>(AppModule);

    const { accessToken: token } = await newFakeUserWithToken();
    accessToken = token;
    user = (
      await request('GET', `${appConfig.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data;
  });

  afterAll(() => {
    app.connection.close();
  });

  it('should be defined', () => {
    expect(userReferralService).toBeDefined();
  });

  it('should generate referral banner', async () => {
    expect(user.referralBannerAdFileName).toBeDefined();
    await userReferralService.generateReferralBanner(user);
    const result = userReferralService.getReferralBannerPath(user);
    const exists = fs.existsSync(result);
    expect(exists).toBe(true);
  });

  it.skip('asdad', async () => {
    const userCount = 10;
    const users: User[] = await Promise.all(
      times(userCount).map(() => userService.create(fakeUser()))
    );

    expect(users.length).toEqual(userCount);

    const userReferrals: UserReferral[] = await Promise.all(
      chunk(users, 2).map(([sender, receiver]) =>
        userReferralService.create({ sender, receiver })
      )
    );

    const senderIds: number[] = userReferrals.map((u) => u.senderId);

    await userReferralService.rewardReferralIfValid(senderIds);

    const coins = await coinService.findAll();

    for (const senderId of senderIds) {
      const sender = await userService.findOne(senderId);
      for (const senderBank of sender.banks) {
        const bankCoin = coins.find((c) => c.id === senderBank.coinId);
        expect(bankCoin?.bonusReferVipToRegAmount).toBeGreaterThan(0);
        expect(senderBank.amount).toEqual(bankCoin.bonusReferVipToRegAmount);
      }
    }
  });
});
