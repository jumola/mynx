import {
  Bank,
  Coin,
  fakeUser,
  Game,
  MailQueue,
  Match,
  newFakeUserWithToken,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
  UserVerification,
} from '@mynx/data';
import { app, appMasterConfig } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppModule } from '../../app.module';
import { AuthService } from '../../auth/auth.service';
import { UserReferralService } from '../../user-referral/user-referral.service';
import { UserService } from '../../user/user.service';
import * as sinon from 'sinon';
import BigNumber from 'bignumber.js';
import { first, slice } from 'lodash';
import { GameService } from '../../game/game.service';
import { MatchService } from '../../match/match.service';
import { BankService } from '../../bank/bank.service';
import { CoinService } from '../../coin/coin.service';
import { TransactionService } from '../../transaction/transaction.service';
import { PlayerService } from '../../player/player.service';
import { StrikeService } from '../../strike/strike.service';
import { MatchStatus } from '@mynx/types-shared';
import { gameCompleter } from '../../cron/lib/game-completer';
import { MailModule } from '../../mail/mail.module';

describe('game e2e tests', () => {
  const meBaseEndpoint = `${app.secureApiBaseUrl}/me`;
  let meResponse: Partial<User>;
  let headers: any;

  let authService: AuthService;
  let bankService: BankService;
  let coinService: CoinService;
  let gameService: GameService;
  let matchService: MatchService;
  let userService: UserService;
  let appModule: AppModule;

  let authStub;

  const getMe = async (customToken?: string): Promise<User> =>
    (
      await request('GET', `${meBaseEndpoint}`, undefined, {
        accessToken: customToken ?? headers.accessToken,
      })
    )?.data as User;

  const claimWelcomeBonus = async () =>
    await request(
      'POST',
      `${meBaseEndpoint}/claim-welcome-bonus`,
      undefined,
      headers
    );

  const favoriteBanksReq = async (coinIds: number[]) => {
    await Promise.all(
      coinIds.map((coinId) =>
        request(
          'POST',
          `${meBaseEndpoint}/favorite-bank/${coinId}`,
          {},
          headers
        )
      )
    );
  };

  const getReferralsReq = async (token) => {
    return await request('GET', `${meBaseEndpoint}/referrals`, undefined, {
      accessToken: token,
    });
  };

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          MailQueue,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
          UserVerification,
        ]),
        MailModule,
      ],
      providers: [
        AuthService,
        BankService,
        CoinService,
        GameService,
        MatchService,
        PlayerService,
        StrikeService,
        TransactionService,
        UserService,
        UserReferralService,
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    bankService = module.get<BankService>(BankService);
    coinService = module.get<CoinService>(CoinService);
    gameService = module.get<GameService>(GameService);
    matchService = module.get<MatchService>(MatchService);
    userService = module.get<UserService>(UserService);

    appModule = module.get<AppModule>(AppModule);

    userService.onModuleInit();
  });

  beforeEach(async () => {
    sinon.restore();

    const { accessToken } = await newFakeUserWithToken();

    meResponse = await getMe(accessToken);

    expect(meResponse.id).toBeDefined();
    expect(accessToken).toBeDefined();

    headers = {
      accessToken,
    };
  });

  afterEach(() => {
    authStub?.restore();
  });

  afterAll(() => {
    appModule.connection.close();
  });

  it('init check', () => {
    expect(authService).toBeDefined();
    expect(bankService).toBeDefined();
    expect(gameService).toBeDefined();
  });

  it('pre-liminary should and should nots', async () => {
    const isEmptyBanks = () => {
      expect(
        me.banks.every((bank) => new BigNumber(bank.amount).isEqualTo(0))
      ).toBeTruthy();
    };

    let me = await getMe();

    const bankTotal = me.banks.length;

    expect(me.id).toBeDefined();
    expect(bankTotal).toBeGreaterThan(0);
    isEmptyBanks();

    // idempotency test
    me = await getMe();
    expect(me.banks.length).toEqual(bankTotal);

    // set favorite banks
    expect(me.banks.filter((b) => b.isFavorite).length).toEqual(0);
    const favoriteBankIds: number[] = slice(
      me.banks,
      0,
      appMasterConfig.welcomeBonusCoinCount
    ).map((b) => b.coinId);

    expect(favoriteBankIds.length).toEqual(
      appMasterConfig.welcomeBonusCoinCount
    );

    // claiming BEFORE setting favorite banks should throw an error
    await expect(claimWelcomeBonus()).rejects.toEqual(
      `You must set ${appMasterConfig.welcomeBonusCoinCount} favorite coins to claim your welcome bonus.`
    );

    await favoriteBanksReq(favoriteBankIds);

    me = await getMe();
    // idempotency test
    expect(me.banks.length).toEqual(bankTotal);

    // favorites is stil the same count
    expect(me.banks.filter((b) => b.isFavorite).length).toEqual(
      appMasterConfig.welcomeBonusCoinCount
    );

    isEmptyBanks();

    // now, after setting the favorite banks, claim welcome bonus
    await claimWelcomeBonus();

    me = await getMe();

    expect(me.banks.length).toEqual(bankTotal);

    // favorite banks must have soemthing now
    expect(
      me.banks
        .filter((b) => b.isFavorite)
        .every((bank) => new BigNumber(bank.amount).isGreaterThan(0))
    ).toBeTruthy();

    // non favorite banks must still have zero
    expect(
      me.banks
        .filter((b) => !b.isFavorite)
        .every((bank) => new BigNumber(bank.amount).isEqualTo(0))
    ).toBeTruthy();

    // must be recorded as gifted
    expect(me.isGifted).toBeTruthy();

    // claiming AGAIN should throw an error
    await expect(claimWelcomeBonus()).rejects.toEqual(
      'You may only claim your welcome gift once!'
    );
  });

  it('should reward user for a successful referral', async () => {
    const me = await getMe();
    expect(me.id).toBeDefined();

    const referrals = (await getReferralsReq(headers.accessToken))
      ?.data as UserReferral[];

    expect(referrals?.length).toEqual(0);

    // mock referredById cookie in AuthService
    authStub = sinon.stub(authService, 'getReferredByIdCookie').returns(me.id);

    // create a new user
    const { accessToken: newUserToken } = await authService.signIn(
      await authService.signUp(undefined, fakeUser())
    );

    expect(newUserToken).toBeDefined();

    authStub.restore();

    const receiver = await getMe(newUserToken);

    const referralsList = (await getReferralsReq(headers.accessToken))
      ?.data as UserReferral[];

    expect(
      referralsList.find(
        (r) => r.senderId === me.id && receiver.id === r.receiverId
      )
    ).toBeDefined();
  });

  it('should reward sender if a receiver successfully ends a game', async () => {
    let me = await getMe();

    await bankService.openAll(me.id);

    me = await getMe();

    let bank: Bank = first(me.banks);

    const game: Game = await gameService.findOneWithParams({
      coinId: bank.coinId,
      players: 2,
    });

    expect(game).toBeDefined();

    await bankService.update(bank.id, {
      amount: game.costAmount,
    });

    bank = await bankService.findOne(bank.id);

    expect(bank.amount).toEqual(game.costAmount);

    const joinGame = async (g: Game, u: User): Promise<Match> =>
      gameService.join(g.id, u);

    let match = await joinGame(game, me);

    bank = await bankService.findOne(bank.id);

    expect(bank.amount.isEqualTo(0)).toBeTruthy();

    if (match.status === MatchStatus.Waiting) {
      // meaning there's not enough players in the match yet
      expect(matchService.hasEnoughPlayers(match)).toBeFalsy();

      // let's add one more player

      authStub = sinon
        .stub(authService, 'getReferredByIdCookie')
        .returns(me.id);
      // create a new user
      const { accessToken: player2Token } = await authService.signIn(
        await authService.signUp(undefined, fakeUser())
      );

      const player2 = await getMe(player2Token);

      expect(player2.id).toBeDefined();

      await bankService.open(player2.id, bank.coinId, game.costAmount);

      let player2Bank = await bankService.findOneWithParams({
        coinId: bank.coinId,
        userId: player2.id,
      });

      expect(player2Bank.amount.isEqualTo(game.costAmount)).toBeTruthy();

      await joinGame(game, player2);

      player2Bank = await bankService.findOne(player2Bank.id);

      expect(player2Bank.amount.isEqualTo(0)).toBeTruthy();

      // both users have already joined
      match = await matchService.findOne(match.id);

      expect(match.status).toEqual(MatchStatus.Concluding);
    }

    // For Concluding
    if (match.status === MatchStatus.Concluding) {
      // this means that enough users has been reached and now this is on "queue", to be processed by cron
      await gameCompleter(matchService);

      match = await matchService.findOne(match.id);

      expect(match.winnerId).toBeDefined();
      expect(match.status).toEqual(MatchStatus.Completed);
      expect(match.concludedAt).toBeDefined();

      const winnerBank = await bankService.findOneWithParams({
        userId: match.winnerId,
        coinId: bank.coinId,
      });
      expect(winnerBank.amount.isEqualTo(game.potAmount)).toBeTruthy();

      // since this is a referral pair, make sure that the sender is rewarded
      const player2Banks = await bankService.favorites(me.id);
      const player2FavoriteBanks = player2Banks.filter((b) => b.isFavorite);

      expect(
        player2FavoriteBanks.every((b) => {
          if (b.coinId === bank.coinId) {
            return b.amount.isEqualTo(
              BigNumber.sum(
                ...[game.potAmount, b.coin.bonusReferRegToRegAmount],
                0
              )
            );
          } else {
            return b.amount.isEqualTo(b.coin.bonusReferRegToRegAmount);
          }
        })
      ).toBeTruthy();
    }

    if (match.status === MatchStatus.Completed) {
      // means that a winner has already been determined
      // re-joining this game should be successful
      // expect(await joinGame(game, me)).rejects.toEqual(
      //   'You must add more funds to join this game!'
      // );
      const bank = await bankService.findOneWithParams({
        coinId: game.coinId,
        userId: me.id,
      });
      await bankService.update(bank.id, {
        amount: game.costAmount,
      });
      const match2 = await joinGame(game, me);
      expect(match2.id).toBeDefined();
      expect(match2.id !== match.id).toBeTruthy();
      expect(match2.status).toEqual(MatchStatus.Waiting);
    }

    expect(me).toBeTruthy();
  });
});
