import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../../app.module';
import { CoinService } from '../../coin/coin.service';
import { v4 as uuidv4 } from 'uuid';
import BigNumber from 'bignumber.js';
import { CoinModule } from '../../coin/coin.module';
import { Coin } from '@mynx/data';

describe('CoinService', () => {
  let service: CoinService;
  let app: AppModule;

  const random = uuidv4();
  const sharedParams: Partial<Coin> = {
    name: random,
    longName: random + random,
    unitName: random,
    coinbaseAccountId: random,
    addressRegex: random,
    color: random,
    coinbaseAssetId: random,
    icon: random,
    unitDecimal: new BigNumber(1),
    exponent: 10,
    isActive: false,
    priceUSD: new BigNumber(2),
    costToVipAmount: new BigNumber(2),
    bonusReferVipToVipAmount: new BigNumber(2),
    bonusReferVipToRegAmount: new BigNumber(2),
    bonusReferRegToVipAmount: new BigNumber(2),
    bonusReferRegToRegAmount: new BigNumber(2),
    welcomeBonusAmount: new BigNumber(2),
    withdrawalMinimumAmount: new BigNumber(2),
    faucetAmountVip: new BigNumber(2),
    faucetAmountReg: new BigNumber(2),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule, CoinModule],
      providers: [CoinService],
    }).compile();

    service = module.get<CoinService>(CoinService);
    app = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    app.connection.close();
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should create and find', async () => {
    const createResponse = await service.create(sharedParams);
    const response = await service.findOne(createResponse.id);
    expect(response.id).toBeDefined();
  });

  it('should create and get list', async () => {
    await service.create(sharedParams);
    const response = await service.findAll();
    expect(response.length).toBeGreaterThanOrEqual(0);
  });

  it('should update and read', async () => {
    const createResponse = await service.create(sharedParams);
    const newName = `${createResponse}-updated`;
    await service.update(createResponse.id, {
      ...sharedParams,
      name: newName,
    });

    const findResponse = await service.findOne(createResponse.id);
    expect(findResponse.name).toEqual(newName);
  });

  it('should create and delete', async () => {
    const createResponse = await service.create(sharedParams);
    await service.remove(createResponse.id);
    const findResponse = await service.findOne(createResponse.id);
    expect(findResponse).toBeUndefined();
  });
});
