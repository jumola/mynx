import { app } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { newFakeUserWithToken, User } from '@mynx/data';

describe('auth check user tests', () => {
  const baseEndpoint = `${app.secureApiBaseUrl}/auth`;
  let meResponse: Partial<User>;
  let accessToken: string;

  beforeEach(async () => {
    const { user: userData, accessToken: token } = await newFakeUserWithToken();
    accessToken = token;
    meResponse = userData;
    expect(meResponse).toBeDefined();
  });

  it('should fail token successfully after register if with wrong token', async () => {
    const response = (
      await request('POST', `${baseEndpoint}/check-user`, null, {
        accessToken,
      })
    )?.data as boolean;
    expect(response).toEqual(true);
  });

  it('should fail token successfully after register if with wrong token', async () => {
    await expect(
      request('POST', `${baseEndpoint}/check-user`, null, {
        accessToken: 'wrong',
      })
    ).rejects.toEqual('Unauthorized');
  });
});
