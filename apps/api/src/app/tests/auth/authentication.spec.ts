import {
  Bank,
  Coin,
  Game,
  MailQueue,
  Match,
  newFakeUserWithToken,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
  UserVerification,
} from '@mynx/data';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { pick } from 'lodash';
import { AppModule } from '../../app.module';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { CoinModule } from '../../coin/coin.module';
import { CoinService } from '../../coin/coin.service';
import { GameModule } from '../../game/game.module';
import { GameService } from '../../game/game.service';
import { MatchModule } from '../../match/match.module';
import { MatchService } from '../../match/match.service';
import { MeModule } from '../../me/me.module';
import { MeService } from '../../me/me.service';
import { PlayerModule } from '../../player/player.module';
import { MailModule } from '../../mail/mail.module';
import { MailService } from '../../mail/mail.service';
import { StrikeModule } from '../../strike/strike.module';
import { TransactionModule } from '../../transaction/transaction.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserModule } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { app } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { clone } from 'lodash';
import { UserVerificationType } from '@mynx/types-shared';
import { AuthModule } from '../../auth/auth.module';
import { AuthService } from '../../auth/auth.service';
import { UserReferralService } from '../../user-referral/user-referral.service';

describe('auth authentication tests', () => {
  let meResponse: Partial<User>;

  let appModule: AppModule;
  let authService: AuthService;
  let bankService: BankService;
  let coinService: CoinService;
  let gameService: GameService;
  let mailService: MailService;
  let matchService: MatchService;
  let meService: MeService;
  let userService: UserService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        AuthModule,
        BankModule,
        CoinModule,
        GameModule,
        MailModule,
        MatchModule,
        MeModule,
        PlayerModule,
        StrikeModule,
        TransactionModule,
        UserModule,
        UserReferralModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          MailQueue,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
          UserVerification,
        ]),
      ],
      providers: [
        AuthService,
        BankService,
        CoinService,
        GameService,
        MailService,
        MatchService,
        MeService,
        UserService,
        UserReferralService,
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    bankService = module.get<BankService>(BankService);
    coinService = module.get<CoinService>(CoinService);
    gameService = module.get<GameService>(GameService);
    mailService = module.get<MailService>(MailService);
    matchService = module.get<MatchService>(MatchService);
    meService = module.get<MeService>(MeService);
    userService = module.get<UserService>(UserService);

    userService.onModuleInit();

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  beforeEach(async () => {
    const { user: userData } = await newFakeUserWithToken();
    meResponse = userData;
    expect(meResponse).toBeDefined();
  });

  it('should login successfully after register after verification', async () => {
    await expect(
      request(
        'POST',
        `${app.secureApiBaseUrl}/auth/login`,
        pick(clone(meResponse), ['email', 'password'])
      )
    ).rejects.toEqual('You need to verify you email to login');

    const user = await userService.findOneWithParams({
      email: meResponse.email,
    });

    const userVerification = await userService.findUserVerification({
      userId: user.id,
      type: UserVerificationType.AccountVerification,
    });

    expect(userVerification.id).toBeDefined();

    const link = userService.generateUserVerificationLink(userVerification);

    console.log('link', link);

    await request('GET', link);

    const user1 = await userService.findOne(user.id);

    expect(user1.isEmailVerified).toBeTruthy();

    const userVerification1 = await userService.findUserVerification({
      id: userVerification.id,
    });

    expect(userVerification1.isUsed).toBeTruthy();

    const loginResponse = (
      await request(
        'POST',
        `${app.secureApiBaseUrl}/auth/login`,
        pick(clone(meResponse), ['email', 'password'])
      )
    )?.data;
    expect(loginResponse.accessToken).toBeDefined();
  });

  it('should fail login after register if wrong creds', async () => {
    const body = pick(clone(meResponse), ['email', 'password']);
    await expect(
      request('POST', `${app.secureApiBaseUrl}/auth/login`, {
        ...body,
        email: 'wrong@email.com',
      })
    ).rejects.toEqual('Invalid credentials');

    await expect(
      request('POST', `${app.secureApiBaseUrl}/auth/login`, {
        ...body,
        password: 'wrongPazz',
      })
    ).rejects.toEqual('Invalid credentials');
  });
});
