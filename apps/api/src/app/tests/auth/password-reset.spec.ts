import {
  Bank,
  Coin,
  Game,
  MailQueue,
  Match,
  newFakeUserWithToken,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
  UserVerification,
} from '@mynx/data';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { pick } from 'lodash';
import { AppModule } from '../../app.module';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { CoinModule } from '../../coin/coin.module';
import { CoinService } from '../../coin/coin.service';
import { GameModule } from '../../game/game.module';
import { GameService } from '../../game/game.service';
import { MatchModule } from '../../match/match.module';
import { MatchService } from '../../match/match.service';
import { MeModule } from '../../me/me.module';
import { MeService } from '../../me/me.service';
import { PlayerModule } from '../../player/player.module';
import { MailModule } from '../../mail/mail.module';
import { MailService } from '../../mail/mail.service';
import { StrikeModule } from '../../strike/strike.module';
import { TransactionModule } from '../../transaction/transaction.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserModule } from '../../user/user.module';
import { UserService } from '../../user/user.service';
import { app } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { clone } from 'lodash';
import {
  MailQueueStatus,
  MailQueueType,
  UserVerificationType,
} from '@mynx/types-shared';
import { AuthModule } from '../../auth/auth.module';
import { AuthService } from '../../auth/auth.service';
import { UserReferralService } from '../../user-referral/user-referral.service';
import { mailQueueSender } from '../../cron/lib/mail-queue-sender';

describe('password reset tests', () => {
  let fakeUser: Partial<User>;
  let accessToken: string;

  let appModule: AppModule;
  let authService: AuthService;
  let bankService: BankService;
  let coinService: CoinService;
  let gameService: GameService;
  let mailService: MailService;
  let matchService: MatchService;
  let meService: MeService;
  let userService: UserService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        AuthModule,
        BankModule,
        CoinModule,
        GameModule,
        MailModule,
        MatchModule,
        MeModule,
        PlayerModule,
        StrikeModule,
        TransactionModule,
        UserModule,
        UserReferralModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          MailQueue,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
          UserVerification,
        ]),
      ],
      providers: [
        AuthService,
        BankService,
        CoinService,
        GameService,
        MailService,
        MatchService,
        MeService,
        UserService,
        UserReferralService,
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    bankService = module.get<BankService>(BankService);
    coinService = module.get<CoinService>(CoinService);
    gameService = module.get<GameService>(GameService);
    mailService = module.get<MailService>(MailService);
    matchService = module.get<MatchService>(MatchService);
    meService = module.get<MeService>(MeService);
    userService = module.get<UserService>(UserService);

    userService.onModuleInit();

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  beforeEach(async () => {
    const { user: userData, accessToken: token } = await newFakeUserWithToken();
    fakeUser = userData;
    accessToken = token;
    expect(fakeUser).toBeDefined();
  });

  it('test module should be ok', () => {
    expect(appModule).toBeDefined();
  });

  it('should create a verification and mailQ if user provided a valid email', async () => {
    fakeUser = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data as User;

    const userVerificationAV: UserVerification =
      await userService.findUserVerification({
        userId: fakeUser.id,
        type: UserVerificationType.AccountVerification,
        isUsed: false,
      });

    await userService.markAsVerified(fakeUser as User, userVerificationAV);

    // first, let the user request for a recovery link
    await request(
      'POST',
      `${app.secureApiBaseUrl}/auth/send-recovery-link`,
      pick(fakeUser, ['email'])
    );

    const userVerification: UserVerification =
      await userService.findUserVerification({
        userId: fakeUser.id,
        type: UserVerificationType.PasswordReset,
        isUsed: false,
      });

    expect(userVerification.id).toBeDefined();

    const mailQueue: MailQueue = await mailService.findOneWithParams(
      {
        type: MailQueueType.PasswordReset,
        status: MailQueueStatus.Queued,
        to: fakeUser.email,
      },
      {
        order: {
          createdAt: 'DESC',
        },
      }
    );

    expect(
      pick((mailQueue.context as any)?.userVerification, ['id', 'userId'])
    ).toEqual(pick(userVerification, ['id', 'userId']));
  });
});
