import { find, first } from 'lodash';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import BigNumber from 'bignumber.js';
import { Logger } from '@nestjs/common';
import { BankService } from '../../bank/bank.service';
import { MeGateway } from '../../me/me.gateway';
import { UserService } from '../../user/user.service';

export const meBroadcastTest = async (
  bankService: BankService,
  meGateway: MeGateway,
  userService: UserService
) => {
  const logger: Logger = new Logger('meBroadcastTest');

  const userIdToTest = 106;
  const userIds: string[] = Array.from(meGateway.connectedUsers.keys());

  const users = await Promise.all(
    userIds.map((id) => userService.findOne(Number(id)))
  );

  const user = find(users, (u) => u.id === userIdToTest);

  if (!user) {
    logger.log('user not found');
    return;
  }

  const bank = first(user.banks);

  if (!bank) {
    await bankService.openAll(user.id);
    logger.log('No bank found');
    return;
  }

  await bankService.updateAndLog(
    user.id,
    bank.coinId,
    TransactionType.In,
    TransactionActivityType.WelcomeBonus,
    new BigNumber(100),
    TransactionStatus.Completed
  );
};
