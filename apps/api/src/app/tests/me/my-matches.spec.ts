import {
  Bank,
  Coin,
  Game,
  Match,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { MatchStatus } from '@mynx/types-shared';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { sample, times } from 'lodash';
import { AppModule } from '../../app.module';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { CoinModule } from '../../coin/coin.module';
import { CoinService } from '../../coin/coin.service';
import { botCreator } from '../../cron/lib/bot-creator';
import { GameModule } from '../../game/game.module';
import { GameService } from '../../game/game.service';
import { MatchModule } from '../../match/match.module';
import { MatchService } from '../../match/match.service';
import { MeModule } from '../../me/me.module';
import { MeService } from '../../me/me.service';
import { PlayerModule } from '../../player/player.module';
import { StrikeModule } from '../../strike/strike.module';
import { TransactionModule } from '../../transaction/transaction.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserModule } from '../../user/user.module';
import { UserService } from '../../user/user.service';

describe('my matches test', () => {
  let appModule: AppModule;
  let bankService: BankService;
  let coinService: CoinService;
  let gameService: GameService;
  let matchService: MatchService;
  let meService: MeService;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        BankModule,
        CoinModule,
        GameModule,
        MatchModule,
        MeModule,
        PlayerModule,
        StrikeModule,
        TransactionModule,
        UserModule,
        UserReferralModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
        ]),
      ],
      providers: [
        BankService,
        CoinService,
        GameService,
        MatchService,
        MeService,
        UserService,
      ],
    }).compile();

    bankService = module.get<BankService>(BankService);
    coinService = module.get<CoinService>(CoinService);
    gameService = module.get<GameService>(GameService);
    matchService = module.get<MatchService>(MatchService);
    meService = module.get<MeService>(MeService);
    userService = module.get<UserService>(UserService);

    userService.onModuleInit();

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  it('init', () => {
    expect(true).toBeTruthy();
  });

  it('should get my matches successfully', async () => {
    const game = await gameService.findOneWithParams({});

    expect(game.id).toBeDefined();

    const users: User[] = [
      await userService.createBot(),
      await userService.createBot(),
    ];

    expect(users.length).toEqual(2);
    expect(users.every((u) => u.id)).toBeTruthy();

    const [user1, user2] = users;

    // add funds to these users
    await Promise.all(
      users.map((user) =>
        userService.reward(
          user.id,
          'manual',
          game.coinId,
          game.costAmount,
          'my matches unit test'
        )
      )
    );

    const [bank1, bank2] = await Promise.all(
      users.map((user) =>
        bankService.findOneWithParams({
          userId: user.id,
          coinId: game.coinId,
        })
      )
    );

    const match = await matchService.create({
      gameId: game.id,
      status: MatchStatus.Waiting,
    });

    await gameService.joinMatch(match, user1, game, bank1);
    await gameService.joinMatch(match, user2, game, bank2);

    const matches1 = await meService.liveMatches(user1.id);
    const matches2 = await meService.liveMatches(user1.id);

    expect(matches1.length).toEqual(1);
    expect(matches2.length).toEqual(1);
    expect(matches1.find((m) => m.id === match.id)).toBeDefined();
    expect(matches2.find((m) => m.id === match.id)).toBeDefined();
  });
});
