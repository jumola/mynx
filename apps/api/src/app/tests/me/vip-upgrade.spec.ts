import {
  Bank,
  Coin,
  newFakeUserWithToken,
  User,
  UserReferral,
} from '@mynx/data';
import { app } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import BigNumber from 'bignumber.js';
import { slice } from 'lodash';
import { AppModule } from '../../app.module';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { CoinModule } from '../../coin/coin.module';
import { MailModule } from '../../mail/mail.module';
import { MatchModule } from '../../match/match.module';
import { MeModule } from '../../me/me.module';
import { MeService } from '../../me/me.service';
import { PlayerModule } from '../../player/player.module';
import { StrikeModule } from '../../strike/strike.module';
import { TransactionModule } from '../../transaction/transaction.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserReferralService } from '../../user-referral/user-referral.service';
import { UserModule } from '../../user/user.module';

describe('user vip upgrade tests', () => {
  let user: User;
  let accessToken: string;

  let appModule: AppModule;
  let bankService: BankService;
  let meService: MeService;
  let userReferralService: UserReferralService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        BankModule,
        CoinModule,
        MailModule,
        MatchModule,
        MeModule,
        PlayerModule,
        StrikeModule,
        TransactionModule,
        UserModule,
        UserReferralModule,
        TypeOrmModule.forFeature([Bank, Coin, User, UserReferral]),
      ],
      providers: [BankService, MeService, UserReferralService],
    }).compile();

    bankService = module.get<BankService>(BankService);
    meService = module.get<MeService>(MeService);
    userReferralService = module.get<UserReferralService>(UserReferralService);

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  beforeEach(async () => {
    const { accessToken: token } = await newFakeUserWithToken();
    accessToken = token;

    user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data;
  });

  it('should let user upgrade his account to vip', async () => {
    expect(user.isVip).toBe(false);
    expect(user.banks.length).toBeGreaterThan(0);

    const bank = user.banks.find((b) => b.id);

    // manually add funds to this user's bank, enough for upgrade
    await bankService.updateAndLog(
      user.id,
      bank.coinId,
      TransactionType.In,
      TransactionActivityType.AccountUpgrade,
      bank.coin.costToVipAmount,
      TransactionStatus.Completed
    );

    // time to upgrade!
    await request(
      'POST',
      `${app.secureApiBaseUrl}/me/upgrade/${bank.coinId}`,
      undefined,
      {
        accessToken,
      }
    );

    user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data;

    expect(user.isVip).toBe(true);
  });

  it('should NOT let user upgrade his account to vip if not enough funds', async () => {
    expect(user.isVip).toBe(false);
    expect(user.banks.length).toBeGreaterThan(0);

    const bank = user.banks.find((b) => b.id);

    expect(new BigNumber(bank.amount).isEqualTo(0)).toBe(true);

    // time to upgrade!
    await expect(
      request(
        'POST',
        `${app.secureApiBaseUrl}/me/upgrade/${bank.coinId}`,
        undefined,
        {
          accessToken,
        }
      )
    ).rejects.toEqual(
      'You do not have enough balance in your chosen wallet to upgrade to VIP.'
    );

    user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data;

    expect(user.isVip).toBe(false);
  });

  it('should reward referrer if referree upgrades to VIP', async () => {
    expect(user.isVip).toBe(false);
    expect(user.banks.length).toBeGreaterThan(0);

    const { accessToken: user2Token } = await newFakeUserWithToken();
    let sender: User = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken: user2Token,
      })
    )?.data;

    // set sender's bank favorites
    const user2Favoritebanks: Bank[] = slice(sender.banks, 0, 4);
    expect(user2Favoritebanks.length).toEqual(4);

    user2Favoritebanks.forEach((b) => {
      expect(new BigNumber(b.amount).isEqualTo(0)).toBeTruthy();
    });

    await Promise.all(
      user2Favoritebanks.map((b) =>
        meService.setFavoriteBank(b.userId, b.coinId)
      )
    );

    sender = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken: user2Token,
      })
    )?.data;

    expect(sender.banks.filter((b) => b.isFavorite).length).toEqual(4);

    let referral = await userReferralService.createUserReferral(
      sender.id,
      user.id
    );

    expect(referral).toBeDefined();

    const bank = user.banks.find((b) => b.id);

    // manually add funds to this user's bank, enough for upgrade
    await bankService.updateAndLog(
      user.id,
      bank.coinId,
      TransactionType.In,
      TransactionActivityType.AccountUpgrade,
      bank.coin.costToVipAmount,
      TransactionStatus.Completed
    );

    // time to upgrade!
    await request(
      'POST',
      `${app.secureApiBaseUrl}/me/upgrade/${bank.coinId}`,
      undefined,
      {
        accessToken,
      }
    );

    user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken,
      })
    )?.data;

    expect(user.isVip).toBe(true);

    referral = await userReferralService.findOne(referral.id);

    expect(referral.isRewarded).toEqual(true);

    // refetch sender
    sender = (
      await request('GET', `${app.secureApiBaseUrl}/me`, undefined, {
        accessToken: user2Token,
      })
    )?.data;

    // verify that his favorite banks have been rewarded
    sender.banks
      .filter((b) => b.isFavorite)
      .forEach((b) => {
        expect(
          new BigNumber(b.amount).isEqualTo(b.coin.bonusReferRegToVipAmount)
        ).toBeTruthy();
      });
  });
});
