import { app, appMasterConfig } from '@mynx/environment';
import { request } from '@mynx/nest-utils';
import { find, slice } from 'lodash';
import {
  Bank,
  Coin,
  fakeUser,
  Game,
  Match,
  newFakeUserWithToken,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { CoinService } from '../../coin/coin.service';
import { AppModule } from '../../app.module';
import { Test, TestingModule } from '@nestjs/testing';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { StrikeService } from '../../strike/strike.service';
import { TransactionService } from '../../transaction/transaction.service';
import { UserService } from '../../user/user.service';
import { MeGateway } from '../../me/me.gateway';
import { AuthService } from '../../auth/auth.service';
import BigNumber from 'bignumber.js';
import { UserModule } from '../../user/user.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoinModule } from '../../coin/coin.module';
import { TransactionModule } from '../../transaction/transaction.module';

describe('me claim welcome bonus tests', () => {
  let appModule: AppModule;
  let bankService: BankService;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        BankModule,
        CoinModule,
        TransactionModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
        ]),
      ],
      providers: [BankService, UserService],
    }).compile();

    bankService = module.get<BankService>(BankService);
    userService = module.get<UserService>(UserService);

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  it('generate avatar', async () => {
    const user = (await newFakeUserWithToken())?.user as User;
    expect(userService.generateAvatar(user)).toBeDefined();
  });

  it('should allow user to get a welcome bonus', async () => {
    expect(bankService).toBeDefined();

    // create a new user and get the accessToken
    const accessToken = (await newFakeUserWithToken())?.accessToken;
    expect(accessToken).toBeDefined();

    const user = (
      await request('GET', `${app.secureApiBaseUrl}/me`, null, {
        accessToken,
      })
    )?.data as User;
    expect(user?.id).toBeDefined();

    expect(user.image).toBeDefined();

    await bankService.openAll(user.id);

    const banksRaw: Bank[] = await bankService.findByUserId(user.id);

    banksRaw.forEach((bank) => {
      expect(bank.amount).toEqual(new BigNumber(0));
    });

    const banksToReward: Bank[] = slice(
      banksRaw,
      0,
      appMasterConfig.welcomeBonusCoinCount
    );

    expect(banksToReward.length).toEqual(appMasterConfig.welcomeBonusCoinCount);

    const coinIds: number[] = banksToReward.map((b) => b.coinId);

    // claim welcome bonus
    await request(
      'POST',
      `${app.secureApiBaseUrl}/me/claim-welcome-bonus`,
      { coinIds },
      {
        accessToken,
      }
    );

    // refetch banks and make sure they have the correct amount
    const banksRawRefetched: Bank[] = await bankService.findByUserId(user.id);

    const rewardedBanks: Bank[] = coinIds.map((coinId) =>
      find(banksRawRefetched, (b) => b.coinId === coinId)
    );

    expect(rewardedBanks.length).toEqual(appMasterConfig.welcomeBonusCoinCount);

    rewardedBanks.forEach((bank) => {
      expect(bank.amount).toEqual(bank.coin.welcomeBonusAmount);
    });
  }, 10000);
});
