import { request } from '@mynx/nest-utils';
import { app } from '@mynx/environment';
import { Match } from '@mynx/data';
import { isArray } from 'lodash';
import { MatchStatus } from '@mynx/types-shared';

describe('match tests', () => {
  const baseEndpoint = `${app.secureApiBaseUrl}/match`;

  it('shoudld get latest winners', async () => {
    const endpoint = `${baseEndpoint}/latest-winners`;
    const result = (await request('GET', `${endpoint}`))?.data as Match[];
    expect(isArray(result)).toBe(true);
    if (result.length) {
      expect(
        result.every((match) => match.status === MatchStatus.Completed)
      ).toBe(true);
    }
  });
});
