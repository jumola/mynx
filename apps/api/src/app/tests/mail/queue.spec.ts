import {
  Bank,
  Coin,
  Game,
  MailQueue,
  Match,
  newFakeUserWithToken,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import {
  MailQueueStatus,
  MailQueueType,
  MatchStatus,
} from '@mynx/types-shared';
import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { pick, sample, times } from 'lodash';
import { AppModule } from '../../app.module';
import { BankModule } from '../../bank/bank.module';
import { BankService } from '../../bank/bank.service';
import { CoinModule } from '../../coin/coin.module';
import { CoinService } from '../../coin/coin.service';
import { botCreator } from '../../cron/lib/bot-creator';
import { GameModule } from '../../game/game.module';
import { GameService } from '../../game/game.service';
import { MatchModule } from '../../match/match.module';
import { MatchService } from '../../match/match.service';
import { MeModule } from '../../me/me.module';
import { MeService } from '../../me/me.service';
import { PlayerModule } from '../../player/player.module';
import { MailModule } from '../../mail/mail.module';
import { MailService } from '../../mail/mail.service';
import { StrikeModule } from '../../strike/strike.module';
import { TransactionModule } from '../../transaction/transaction.module';
import { UserReferralModule } from '../../user-referral/user-referral.module';
import { UserModule } from '../../user/user.module';
import { UserService } from '../../user/user.service';

describe('mail queue tests', () => {
  let appModule: AppModule;
  let bankService: BankService;
  let coinService: CoinService;
  let gameService: GameService;
  let mailService: MailService;
  let matchService: MatchService;
  let meService: MeService;
  let userService: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        AppModule,
        BankModule,
        CoinModule,
        GameModule,
        MailModule,
        MatchModule,
        MeModule,
        PlayerModule,
        StrikeModule,
        TransactionModule,
        UserModule,
        UserReferralModule,
        TypeOrmModule.forFeature([
          Bank,
          Coin,
          Game,
          MailQueue,
          Match,
          Player,
          Strike,
          Transaction,
          User,
          UserReferral,
        ]),
      ],
      providers: [
        BankService,
        CoinService,
        GameService,
        MailService,
        MatchService,
        MeService,
        UserService,
      ],
    }).compile();

    bankService = module.get<BankService>(BankService);
    coinService = module.get<CoinService>(CoinService);
    gameService = module.get<GameService>(GameService);
    mailService = module.get<MailService>(MailService);
    matchService = module.get<MatchService>(MatchService);
    meService = module.get<MeService>(MeService);
    userService = module.get<UserService>(UserService);

    userService.onModuleInit();

    appModule = module.get<AppModule>(AppModule);
  });

  afterAll(() => {
    appModule.connection.close();
  });

  it('should queue mails', async () => {
    const { user } = await newFakeUserWithToken();
    const u = await userService.findOne(user.id);

    let mailQueue = await mailService.welcome(u);
    expect(pick(mailQueue, ['status', 'type', 'to', 'subject'])).toEqual({
      status: MailQueueStatus.Queued,
      type: MailQueueType.Welcome,
      to: u.email,
      subject: 'Welcome email here',
    });

    await mailService.send(mailQueue);

    mailQueue = await mailService.findOneWithParams({
      id: mailQueue.id,
    });

    expect(mailQueue.status).toEqual(MailQueueStatus.Sent);
  });
});
