import { Injectable } from '@nestjs/common';
import { TransactionService } from '../transaction/transaction.service';
import BigNumber from 'bignumber.js';
import { CoinbaseHookType } from '@mynx/types';
import { TransactionStatus, TransactionType } from '@mynx/types-shared';
import { getDateNow } from '@mynx/nest-utils';

@Injectable()
export class HookService {
  constructor(private transactionService: TransactionService) {}

  async cbUpdates(request: any) {
    const body = {
      type: request?.type,
      address: request?.data?.address,
      accountId: request?.account?.id,
      transactionId: request?.additional_data?.transaction?.id,
      hash: request?.additional_data?.hash,
      amount: request?.additional_data?.amount?.amount,
    };

    if (body.type === CoinbaseHookType.NewPayment) {
      // Docs: https://developers.coinbase.com/api/v2#notifications

      // find a Deposit with this address. if none, ignore
      const deposits = await this.transactionService.findAllWithParams({
        type: TransactionType.In,
        coinbaseAddress: body.address,
      });

      const deposit = deposits?.find((d) => d.coinbaseAddress === body.address);
      if (!deposit) {
        console.log('Deposit not found');
        return;
      }

      // make sure it's for the same coin
      if (deposit.coin.coinbaseAccountId !== body.accountId) {
        console.log(
          'Coin and address mismatch',
          deposit?.coin?.name,
          body.accountId
        );
        return;
      }

      await this.transactionService.update(deposit.id, {
        receivedAt: getDateNow(),
        coinbaseTransactionId: body.transactionId,
        coinbaseHash: body.hash,
        amount: new BigNumber(body.amount),
        status: TransactionStatus.Exchanging,
      });
    }
  }
}
