import { Request, Controller, Get, Post, Body } from '@nestjs/common';
import { HookService } from './hook.service';

@Controller('hook')
export class HookController {
  constructor(private readonly hookService: HookService) {}

  @Post('cb-updates')
  async cbUpdates(@Body() request) {
    await this.hookService.cbUpdates(request);
  }
}
