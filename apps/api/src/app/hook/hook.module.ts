import { Coin, Strike, Transaction } from '@mynx/data';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionModule } from '../transaction/transaction.module';
import { HookController } from './hook.controller';
import { HookService } from './hook.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Coin, Strike, Transaction]),
    TransactionModule,
  ],
  controllers: [HookController],
  providers: [HookService],
  exports: [TypeOrmModule, HookService],
})
export class HookModule {}
