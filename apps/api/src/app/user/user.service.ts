import { BadRequestException, Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bank, fakeUser, User, UserVerification } from '@mynx/data';
import { Repository } from 'typeorm';
import { app } from '@mynx/environment';
import { BankService } from '../bank/bank.service';
import { ModuleRef } from '@nestjs/core';
import BigNumber from 'bignumber.js';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
  UserRole,
  UserType,
  UserVerificationType,
} from '@mynx/types-shared';
import { createAvatar } from '@dicebear/avatars';
import * as style from '@dicebear/avatars-bottts-sprites';
import { getDateNow } from '@mynx/nest-utils';
import * as bcrypt from 'bcrypt';
import { v4 as uuidv4 } from 'uuid';
import { MailService } from '../mail/mail.service';
import { UserReferralService } from '../user-referral/user-referral.service';
import { CoinService } from '../coin/coin.service';

const defaultRelations = ['banks', 'banks.coin'];

@Injectable()
export class UserService implements OnModuleInit {
  private bankService: BankService;
  private coinService: CoinService;
  private mailService: MailService;
  private userReferralService: UserReferralService;

  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,

    @InjectRepository(UserVerification)
    private readonly userVerificationRepo: Repository<UserVerification>,

    private readonly moduleRef: ModuleRef
  ) {}

  onModuleInit() {
    this.bankService = this.moduleRef.get(BankService, { strict: false });
    this.coinService = this.moduleRef.get(CoinService, { strict: false });
    this.mailService = this.moduleRef.get(MailService, { strict: false });
    this.userReferralService = this.moduleRef.get(UserReferralService, {
      strict: false,
    });
  }

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(params: Partial<User>) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<User>) {
    return await this.repo.findOne(params, {
      relations: [],
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body: Partial<User>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  // custom methods
  async getRandomAi(): Promise<User> {
    const user = await this.repo
      .createQueryBuilder('user')
      .where('isB = :isB', {
        isB: true,
      })
      .orderBy('RAND()')
      .getOne();
    return user;
  }

  async join(response, username) {
    if (username) {
      // we're saving id's not usernames
      const referrerUser = await this.findOneWithParams({
        username,
      });

      if (referrerUser) {
        response.cookie('referredById', referrerUser.id);
      }
    }

    return response.redirect(app.clientUrl);
  }

  async reward(
    userId: number,
    type: 'referral' | 'manual' | 'faucet' | 'welcome-bonus',
    coinId?: number,
    amount?: BigNumber,
    notes?: string,
    userReferralId?: number
  ): Promise<void> {
    if (type === 'referral') {
      await this.rewardReferral(userReferralId);
    } else if (type === 'manual') {
      await this.rewardManual(userId, coinId, amount, notes);
    } else if (type === 'faucet') {
      await this.rewardFaucet(userId);
    } else if (type === 'welcome-bonus') {
      await this.rewardWelcomeBonus(userId);
    }
  }

  async rewardWelcomeBonus(userId: number): Promise<void> {
    const user = await this.findOne(userId);
    const coins = await this.coinService.featureds();

    await Promise.all(
      coins.map((coin) =>
        this.bankService.updateAndLog(
          user.id,
          coin.id,
          TransactionType.In,
          TransactionActivityType.WelcomeBonus,
          coin.welcomeBonusAmount
        )
      )
    );

    await this.update(user.id, {
      isGifted: true,
    });
  }

  async rewardFaucet(userId: number): Promise<void> {
    const user = await this.findOne(userId);
    const banks = await this.bankService.favorites(userId);
    await Promise.all(
      banks.map((bank) =>
        this.bankService.updateAndLog(
          bank.userId,
          bank.coinId,
          TransactionType.In,
          TransactionActivityType.Faucet,
          user.isVip ? bank.coin.faucetAmountVip : bank.coin.faucetAmountReg,
          TransactionStatus.Completed
        )
      )
    );
  }

  async rewardManual(
    userId: number,
    coinId: number,
    amount: BigNumber,
    notes?: string
  ) {
    await this.bankService.updateAndLog(
      userId,
      coinId,
      TransactionType.In,
      TransactionActivityType.Manual,
      amount,
      TransactionStatus.Completed,
      notes
    );
  }

  async rewardReferral(userReferralId: number) {
    const referral = await this.userReferralService.findOne(userReferralId);

    const banks: Bank[] = await this.bankService.favorites(referral.senderId);
    for (const bank of banks) {
      // scenarios [sender] [receiver]
      // vip to vip
      // vip to reg
      // reg to vip
      // reg to reg
      let rewardAmount: BigNumber;
      if (referral.sender.isVip && referral.receiver.isVip) {
        rewardAmount = bank.coin.bonusReferVipToVipAmount;
      } else if (referral.sender.isVip && !referral.receiver.isVip) {
        rewardAmount = bank.coin.bonusReferVipToRegAmount;
      } else if (!referral.sender.isVip && referral.receiver.isVip) {
        rewardAmount = bank.coin.bonusReferRegToVipAmount;
      } else {
        rewardAmount = bank.coin.bonusReferRegToRegAmount;
      }

      await this.bankService.updateAndLog(
        referral.senderId,
        bank.coinId,
        TransactionType.In,
        TransactionActivityType.Referral,
        rewardAmount,
        TransactionStatus.Completed
      );
    }

    await this.userReferralService.update(referral.id, {
      isRewarded: true,
    });
  }

  generateAvatar(user: User): string {
    return createAvatar(style, {
      seed: user.email,
      dataUri: true,
    });
  }

  async createBot() {
    const userDraft = fakeUser();

    // ai default config
    userDraft.isB = true;
    userDraft.isVip = true;
    userDraft.isGifted = true;
    userDraft.role = UserRole.User;
    userDraft.coinbaseEmail = userDraft.email;
    userDraft.isVipAt = getDateNow();
    userDraft.type = UserType.Ai;
    userDraft.password = await bcrypt.hash(userDraft.password, 10);
    userDraft.image = this.generateAvatar(userDraft as User);

    const user = await this.create(userDraft);

    await this.bankService.openAll(user.id);
    return user;
  }

  findUserVerification(params: Partial<UserVerification>) {
    return this.userVerificationRepo.findOne(params, {
      relations: ['user'],
    });
  }

  async createUserVerification(
    userId: number,
    type: UserVerificationType
  ): Promise<UserVerification> {
    return await this.userVerificationRepo.save({
      userId,
      type,
      secret: uuidv4(),
    } as any);
  }

  async updateUserVerification(
    userId: number,
    params: Partial<UserVerification>
  ) {
    return await this.userVerificationRepo.update(userId, params);
  }

  generateUserVerificationLink(userVerification: UserVerification): string {
    if (userVerification.type === UserVerificationType.AccountVerification) {
      return `${app.secureApiBaseUrl}/auth/verify/${userVerification.secret}`;
    } else if (userVerification.type === UserVerificationType.PasswordReset) {
      return `${app.secureApiBaseUrl}/auth/verify/password-reset/${userVerification.secret}`;
    } else {
      return app.clientUrl;
    }
  }

  async markAsVerified(
    user: User,
    userVerification: UserVerification
  ): Promise<void> {
    await this.update(user.id, {
      isEmailVerified: true,
    });
    await this.updateUserVerification(userVerification.id, {
      isUsed: true,
    });

    await this.mailService.accountVerified(user);
  }

  async setNewPassword(userId: number, newPassword?: string): Promise<string> {
    const password = newPassword ?? uuidv4();
    await this.update(userId, {
      password: await bcrypt.hash(password, 10),
    });
    return password;
  }

  async upgrade(userId: number, coinId: number) {
    const user = await this.findOne(userId);
    const bank: Bank = user.banks.find((b) => b.coinId === coinId);

    if (!bank) {
      throw new BadRequestException('Wallet not found!');
    }

    if (new BigNumber(bank.amount).isLessThan(bank.coin.costToVipAmount)) {
      throw new BadRequestException(
        `You do not have enough balance in your chosen wallet to upgrade to VIP.`
      );
    }

    await this.bankService.updateAndLog(
      user.id,
      bank.coinId,
      TransactionType.In,
      TransactionActivityType.AccountUpgrade,
      bank.coin.costToVipAmount.multipliedBy(-1),
      TransactionStatus.Completed
    );

    await this.update(user.id, {
      isVip: true,
    });

    // check if this user was referred by someone
    const referral = await this.userReferralService.findOneWithParams({
      receiverId: user.id,
    });

    if (referral) {
      await this.reward(
        referral.senderId,
        'referral',
        undefined,
        undefined,
        undefined,
        referral.id
      );
    }
  }
}
