import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  Bank,
  Coin,
  Game,
  Match,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { MatchService } from './match.service';
import { MatchController } from './match.controller';
import { BankModule } from '../bank/bank.module';
import { PlayerModule } from '../player/player.module';
import { UserReferralModule } from '../user-referral/user-referral.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Bank,
      Coin,
      Game,
      Match,
      Player,
      Strike,
      Transaction,
      User,
      UserReferral,
    ]),

    BankModule,
    PlayerModule,
    UserReferralModule,
  ],
  controllers: [MatchController],
  providers: [MatchService],
  exports: [TypeOrmModule, MatchService],
})
export class MatchModule {}
