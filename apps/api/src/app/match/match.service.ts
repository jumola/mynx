import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Match, Player, User } from '@mynx/data';
import { FindManyOptions, Repository } from 'typeorm';
import {
  MatchStatus,
  TransactionActivityType,
  TransactionType,
} from '@mynx/types-shared';
import { PlayerService } from '../player/player.service';
import { isArray, random } from 'lodash';
import { BankService } from '../bank/bank.service';
import { getDateNow } from '@mynx/nest-utils';
import BigNumber from 'bignumber.js';
import { convertCryptoToUSD } from '@mynx/shared-utils';
import { UserReferralService } from '../user-referral/user-referral.service';

@Injectable()
export class MatchService {
  defaultRelations = ['game', 'game.coin', 'players', 'players.user', 'winner'];

  constructor(
    @InjectRepository(Match)
    public readonly repo: Repository<Match>,
    private bankService: BankService,
    private playerService: PlayerService,
    private userReferralService: UserReferralService
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(
    params: Partial<Match>,
    options: FindManyOptions<Match> = {}
  ) {
    return await this.repo.find({
      where: params,
      relations: this.defaultRelations,
      ...options,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: this.defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<Match>) {
    return await this.repo.findOne(params, {
      relations: this.defaultRelations,
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  // CUSTOM methods

  /**
   * adds a user into a match as a player
   * @param match
   * @param user
   * @returns
   */
  async addPlayer(match: Match, user: User): Promise<Match> {
    const player: Player = await this.playerService.create({
      matchId: match.id,
      userId: user.id,
    });

    match.players = isArray(match?.players) ? match.players : [];
    match.players.push(player);

    if (this.hasEnoughPlayers(match)) {
      await this.update(match.id, {
        status: MatchStatus.Concluding,
      });
    }

    return await this.findOne(match.id);
  }

  /**
   * returns a waiting match for specified game id
   * @param gameId
   * @returns
   */
  async getAvailableMatch(gameId: number) {
    const match = await this.findOneWithParams({
      gameId,
      status: MatchStatus.Waiting,
    });

    if (!match) {
      await this.create({
        gameId,
        status: MatchStatus.Waiting,
      });
      return await this.getAvailableMatch(gameId);
    }
    return match;
  }

  /**
   * checks if a match has enough players
   * @param match
   * @returns
   */
  hasEnoughPlayers(match: Match): boolean {
    return Boolean(match?.players?.length === match?.game?.players);
  }

  /**
   * this sets a random winner and updates match doc
   * @param match
   * @returns
   */
  async conclude(match: Match) {
    const players = match.players;

    // should not happen, but just in case
    if (!players?.length) {
      console.log('concluding but not players. match id: ', match?.id);
      return;
    }

    const randomPos = random(0, players?.length - 1);

    const winningPlayer: Player = players[randomPos];

    // update match's winner id + set to Completed
    const params = {
      winnerId: winningPlayer?.user?.id,
      status: MatchStatus.Completed,
      concludedAt: getDateNow(),
    };

    await this.update(match.id, params);

    // reward winner
    await this.bankService.updateAndLog(
      winningPlayer.userId,
      match.game.coinId,
      TransactionType.Out,
      TransactionActivityType.BuyIn,
      match.game.potAmount
    );

    // update player doc
    await this.playerService.update(winningPlayer?.id, {
      isWinner: true,
    });

    await this.userReferralService.rewardReferralIfValid(
      players.map((p) => p.userId)
    );

    return await this.findOne(match.id);
  }

  /**
   * fills matches with bots
   * @returns
   */
  async getAiFillableMatches(params: Partial<Match> = {}) {
    const maxAmountInUSD = new BigNumber(0.005);

    const waitingMatches: Match[] = await this.findAllWithParams({
      status: MatchStatus.Waiting,
      ...params,
    });

    const matches: Match[] = [];

    for (const match of waitingMatches) {
      const buyInCostUSD: BigNumber = convertCryptoToUSD(
        match.game.coin,
        match.game.costAmount
      );

      const isWithinMaxAmount =
        buyInCostUSD.isLessThanOrEqualTo(maxAmountInUSD);

      const hasAMinOnePlayer = Boolean(match?.players?.length);

      if (isWithinMaxAmount) {
        matches.push(match);
      }
    }

    return matches;
  }

  async latestWinners() {
    return await this.findAllWithParams(
      {
        status: MatchStatus.Completed,
      },
      {
        order: {
          concludedAt: 'DESC',
        },
        take: 50,
      }
    );
  }
}
