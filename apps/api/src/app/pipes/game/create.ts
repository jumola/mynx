import { JoiValidationPipe } from '../joi-validation-pipe';
import * as Joi from 'joi';

export const GameCreateValidation = new JoiValidationPipe(
  Joi.object().keys({
    name: Joi.string().required(),
    players: Joi.number().required(),
    coinId: Joi.number().required(),
  })
);
