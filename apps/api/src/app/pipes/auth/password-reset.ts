import { JoiValidationPipe } from '../joi-validation-pipe';
import * as Joi from 'joi';

export const AuthPasswordResetValidation = new JoiValidationPipe(
  Joi.object().keys({
    email: Joi.string().required().email(),
  })
);
