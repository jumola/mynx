import { JoiValidationPipe } from '../joi-validation-pipe';
import * as Joi from 'joi';

export const AuthRegisterValidation = new JoiValidationPipe(
  Joi.object().keys({
    firstName: Joi.string().required().min(3),
    lastName: Joi.string().required().min(3),
    username: Joi.string().required().alphanum().min(3).max(10),
    email: Joi.string().required().email(),
    password: Joi.string().required().min(8),
  })
);
