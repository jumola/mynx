import { JoiValidationPipe } from '../joi-validation-pipe';
import * as Joi from 'joi';

export const CoinCreateValidation = new JoiValidationPipe(
  Joi.object().keys({
    name: Joi.string().required(),
    longName: Joi.string().required(),
    unitName: Joi.string().required(),
    coinbaseAssetId: Joi.string(),
  })
);
