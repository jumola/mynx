import { JoiValidationPipe } from '../joi-validation-pipe';
import * as Joi from 'joi';
import { appMasterConfig } from '@mynx/environment';

export const SetFavoriteBanksValidation = new JoiValidationPipe(
  Joi.object().keys({
    coinIds: Joi.array()
      .min(appMasterConfig.welcomeBonusCoinCount)
      .max(appMasterConfig.welcomeBonusCoinCount)
      .required(),
  })
);
