import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Player } from '@mynx/data';
import { FindManyOptions, Repository } from 'typeorm';

const defaultRelations = [
  'match',
  'match.game',
  'match.players',
  'match.players.user',
  'match.game.coin',
  'user',
];
@Injectable()
export class PlayerService {
  constructor(
    @InjectRepository(Player)
    public readonly repo: Repository<Player>
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  findOne(id: number) {
    return this.repo.findOne(id);
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  async findAllWithParams(
    params: Partial<Player>,
    options: FindManyOptions<Player> = {}
  ) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
      ...options,
    });
  }
}
