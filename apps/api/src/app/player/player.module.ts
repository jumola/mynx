import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Player } from '@mynx/data';
import { PlayerService } from './player.service';

@Module({
  imports: [TypeOrmModule.forFeature([Player])],
  controllers: [],
  providers: [PlayerService],
  exports: [TypeOrmModule, PlayerService],
})
export class PlayerModule {}
