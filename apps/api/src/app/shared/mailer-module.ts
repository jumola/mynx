import { mailerConfig } from '@mynx/environment';
import { MailerModule } from '@nestjs-modules/mailer';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

export const AppMailerModule = MailerModule.forRoot({
  ...mailerConfig,
  template: {
    // since this is being called by cron, via queue
    dir: process.cwd() + '/apps/api/src/templates/',
    adapter: new HandlebarsAdapter(),
    options: {
      strict: true,
    },
  },
});
