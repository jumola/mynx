import { Coin, Game } from '@mynx/data';
import { appMasterConfig } from '@mynx/environment';
import { CoinService } from '../../coin/coin.service';
import { GameService } from '../../game/game.service';
import BigNumber from 'bignumber.js';

/**
 *
 * @param coinService
 * @param gameService
 */
export const gameManager = async (
  coinService: CoinService,
  gameService: GameService
) => {
  const coins: Coin[] = await coinService.actives();

  const games: Game[] = await gameService.findAll();

  for (const coin of coins) {
    // for each coin, we want to maintain the games by number of players which are 2. 3. 5. 10
    for (const playersRequired of appMasterConfig.playerNumbers) {
      let game = games.find(
        (game) => game.coinId === coin.id && game.players === playersRequired
      );

      if (!game) {
        // if the game doesn't exist, create a new one
        game = await gameService.create({
          name: coin.name,
          players: playersRequired,
          coinId: coin.id,
        });
      }

      // now we're sure that the game exists,
      // we now want to update the
      game.costAmount = coin.welcomeBonusAmount ?? new BigNumber(0);
      game.potAmount = game.costAmount.multipliedBy(
        new BigNumber(playersRequired)
      );

      const rakeAmount = game.potAmount.multipliedBy(appMasterConfig.rakeRate);
      game.prizeAmount = game.potAmount.minus(rakeAmount);

      await gameService.update(game.id, game);
    }
  }
};
