import BigNumber from 'bignumber.js';
import { coinbaseSpotPrice } from '@mynx/coinbase';
import { CoinService } from '../../coin/coin.service';
import { Coin } from '@mynx/data';
import { coinPriceCalculator } from '@mynx/shared-utils';
import { gameManager } from './game-manager';
import { GameService } from '../../game/game.service';
import { isNil } from 'lodash';

export const priceTracker = async (
  coinService: CoinService,
  gameService: GameService
) => {
  const coins: Coin[] = await coinService.actives();

  for (let coin of coins) {
    try {
      const spotData = await coinbaseSpotPrice(coin);
      coin.priceUSD = new BigNumber(spotData.amount);

      coin = coinPriceCalculator(coin, coins);
    } catch (error) {
      console.warn(
        'priceTracker: coinbaseSpotPrice || coinPriceCalculator on',
        coin.name,
        error.message
      );
      continue;
    }

    // if no price, deactive this coin
    coin.isActive = !isNil(coin.priceUSD);

    await coinService.update(coin.id, coin);
  }

  await gameManager(coinService, gameService);
};
