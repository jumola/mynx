import { User } from '@mynx/data';
import { UserService } from '../../user/user.service';

export const botCreator = async (userService: UserService): Promise<User> => {
  return await userService.createBot();
};
