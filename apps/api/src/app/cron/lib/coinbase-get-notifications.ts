import { coinbaseNotificationsList } from '@mynx/coinbase';

export const coinbaseGetNotifications = async () => {
  try {
    await coinbaseNotificationsList();
  } catch (error) {
    console.log(error.message);
  }
};
