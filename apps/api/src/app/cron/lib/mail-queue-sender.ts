import { MailQueueStatus } from '@mynx/types-shared';
import { MailService } from '../../mail/mail.service';

export const mailQueueSender = async (
  mailService: MailService
): Promise<void> => {
  const queueds = await mailService.findAllWithParams(
    {
      status: MailQueueStatus.Queued,
    },
    {
      order: {
        createdAt: 'DESC',
      },
      take: 5,
    }
  );

  await Promise.all(
    queueds.map((q) =>
      mailService.send(q).catch((error) =>
        mailService.update(q.id, {
          notes: error.message,
          status: MailQueueStatus.Failed,
        })
      )
    )
  );
};
