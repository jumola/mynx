import { getMatchSocketEventId } from '@mynx/shared-utils';
import { MatchStatus } from '@mynx/types-shared';
import { GameGateway } from '../../game/game.gateway';
import { MatchService } from '../../match/match.service';

export const gameCompleter = async (
  matchService: MatchService,
  gameGateway?: GameGateway
) => {
  // look for matches that are in "concluding" status

  const matches = await matchService.repo
    .createQueryBuilder('match')
    .leftJoinAndSelect('match.game', 'game')
    .leftJoinAndSelect('game.coin', 'coin')
    .leftJoinAndSelect('match.players', 'players')
    .leftJoinAndSelect('players.user', 'user')
    .leftJoinAndSelect('match.winner', 'winner')
    .where('status = :status', {
      status: MatchStatus.Concluding,
    })
    .orderBy('RAND()')
    .getMany();

  if (!matches?.length) {
    return;
  }

  // generate a random winner on each match + broadcast latest match
  for (const match of matches) {
    const concludedMatch = await matchService.conclude(match);
    gameGateway?.server.emit(getMatchSocketEventId(match), {
      payload: concludedMatch,
    });
  }
};
