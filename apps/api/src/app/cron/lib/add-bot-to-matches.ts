import { Match } from '@mynx/data';
import { isNil } from 'lodash';
import { MatchService } from '../../match/match.service';
import { UserService } from '../../user/user.service';

export const addBotToMatches = async (
  matchService: MatchService,
  userService: UserService
) => {
  const matches: Match[] = await matchService.getAiFillableMatches();
  for (const match of matches) {
    const aiUser = await userService.getRandomAi();
    if (aiUser && isNil(match.players.find((p) => p.userId === aiUser.id))) {
      await matchService.addPlayer(match, aiUser);
    }
  }
};
