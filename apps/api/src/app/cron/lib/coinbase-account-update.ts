import BigNumber from 'bignumber.js';
import { coinbaseGetAccounts } from '@mynx/coinbase';
import { CoinService } from '../../coin/coin.service';
import { toLower } from 'lodash';
import { Coin } from '@mynx/data';

export const coinbaseAccountUpdate = async (coinService: CoinService) => {
  // get all coinbase accounts
  let finalResult = [];
  let result: any = {
    pagination: {
      next_uri: '/v2/accounts',
    },
  };

  while (result?.pagination?.next_uri) {
    result = await coinbaseGetAccounts(result?.pagination?.next_uri);
    finalResult = finalResult.concat(result?.data || []);
  }

  // get all coins
  const coins = await coinService.findAll();
  // now for each coin, check if we already have it on record:
  // yes = update data
  // no = create a new coin profile
  for (const account of finalResult) {
    // only accept coins that are allow_deposits && allow_withdrawals
    const isValidCoin = account.id !== account.currency.code;

    const isActive = Boolean(
      account?.allow_deposits && account?.allow_withdrawals && isValidCoin
    );

    // check if this coin exists
    const localCoin = coins?.find(
      (coin) => coin.coinbaseAccountId === account?.id
    );

    // per Coinbase's docs
    const params: Partial<Coin> = {
      name: account?.currency?.code,
      longName: account?.currency?.name,
      color: account?.currency?.color,
      exponent: account?.currency?.exponent,
      unitDecimal: new BigNumber(1).shiftedBy(account?.currency?.exponent * -1),
      addressRegex: account?.currency?.address_regex,
      coinbaseAccountId: account?.id,
      coinbaseAssetId: account?.currency?.asset_id,
      icon: `https://cryptoicon-api.vercel.app/api/icon/${toLower(
        account?.currency?.code
      )}`,
      isActive,
      coinbaseResourcePath: account?.resource_path,
    };

    if (!localCoin) {
      params.isActive = false;
      await coinService.create(params);
    } else {
      await coinService.update(localCoin.id, params);
    }
  }
};
