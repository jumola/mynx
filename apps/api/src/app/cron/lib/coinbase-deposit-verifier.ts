import { coinbaseGetTransaction } from '@mynx/coinbase';
import { Transaction } from '@mynx/data';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import { TransactionService } from '../../transaction/transaction.service';
import { getDateNow } from '@mynx/nest-utils';
import { BankService } from '../../bank/bank.service';

export const coinbaseDepositVerifier = async (
  bankService: BankService,
  transactionService: TransactionService
) => {
  // find all deposits in exchanging status
  const deposits: Transaction[] = await transactionService.findAllWithParams({
    type: TransactionType.In,
    status: TransactionStatus.Exchanging,
  });

  if (!deposits?.length) {
    return;
  }

  for (const deposit of deposits) {
    const cbTxn = await coinbaseGetTransaction(
      deposit?.coin,
      deposit?.coinbaseTransactionId
    );

    if (cbTxn?.status === 'canceled') {
      await transactionService.update(deposit.id, {
        status: TransactionStatus.Canceled,
        canceledAt: getDateNow(),
      });
    } else if (cbTxn?.status === 'completed') {
      await transactionService.update(deposit.id, {
        status: TransactionStatus.Completed,
        completedAt: getDateNow(),
      });

      await bankService.updateAndLog(
        deposit.userId,
        deposit.coinId,
        TransactionType.In,
        TransactionActivityType.Deposit,
        deposit.amount
      );
    }
  }
  return;
};
