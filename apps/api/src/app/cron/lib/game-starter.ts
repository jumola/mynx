import { GameService } from '../../game/game.service';
import { MatchService } from '../../match/match.service';

/**
 * maintains that there's at least one ongoing match for each game
 * @param matchService
 * @param gameService
 * @returns
 */
export const gameStarter = async (
  matchService: MatchService,
  gameService: GameService
) => {
  const games = await gameService.findAll();
  await Promise.all(
    games.map((game) => matchService.getAvailableMatch(game.id))
  );
};
