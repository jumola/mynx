import { Injectable, Logger } from '@nestjs/common';
import { CoinService } from '../coin/coin.service';
import { GameService } from '../game/game.service';
import { MatchService } from '../match/match.service';
import { GameGateway } from '../game/game.gateway';
import { BankService } from '../bank/bank.service';
import { TransactionService } from '../transaction/transaction.service';
import { UserService } from '../user/user.service';
import { addBotToMatches } from './lib/add-bot-to-matches';
import { botCreator } from './lib/bot-creator';
import { coinbaseDepositVerifier } from './lib/coinbase-deposit-verifier';
import { gameManager } from './lib/game-manager';
import { gameCompleter } from './lib/game-completer';
import { priceTracker } from './lib/price-tracker';
import { coinbaseAccountUpdate } from './lib/coinbase-account-update';
import { MeGateway } from '../me/me.gateway';
import { MailService } from '../mail/mail.service';
import { mailQueueSender } from './lib/mail-queue-sender';
import { Cron } from '@nestjs/schedule';
import { coinbaseGetNotifications } from './lib/coinbase-get-notifications';
import { gameStarter } from './lib/game-starter';

@Injectable()
export class CronService {
  private readonly logger = new Logger(CronService.name);

  constructor(
    private bankService: BankService,
    private coinService: CoinService,
    private gameService: GameService,
    private mailService: MailService,
    private matchService: MatchService,
    private transactionService: TransactionService,
    private userService: UserService,

    private gameGateway: GameGateway,
    private meGateway: MeGateway
  ) {}

  // cron schedule values from crontab.guru

  /**
   * updates which coins we're working with
   * this saves the latest coin information
   * runs: every 10 minutes
   */
  @Cron('*/10 * * * *')
  async coinbaseAccountUpdate() {
    this.logger.log('Cron: coinbaseAccountUpdate Start');
    await coinbaseAccountUpdate(this.coinService);
    this.logger.log('Cron: coinbaseAccountUpdate Done');
  }

  /**
   * gets the latest price and then updates all relative properties
   * we're calculating based on the current/latest price of every
   * coin we're offering
   * runs: every 1 minute
   */
  @Cron('* * * * *')
  async priceTracker() {
    this.logger.log('Cron: priceTracker Start');
    await priceTracker(this.coinService, this.gameService);
    this.logger.log('Cron: priceTracker Done');
  }

  /**
   * makes sure that there's an open game for each coin
   * for each variant (number of players) that we're offering
   * runs: every 1 hour
   */
  @Cron('0 * * * *')
  async gameManager() {
    this.logger.log('Cron: gameManager Start');
    await gameManager(this.coinService, this.gameService);
    this.logger.log('Cron: gameManager Done');
  }

  /**
   * makes sure that matches with a status of 'concluding'
   * are promoted to 'completed'
   * it also then starts a new match to keep a game player-variant
   * available to be joined
   * runs: every 5 seconds
   */
  @Cron('*/5 * * * * *')
  async gameCompleter() {
    this.logger.log('Cron: gameCompleter Start');
    await gameCompleter(this.matchService, this.gameGateway);
    this.logger.log('Cron: gameCompleter Done');
  }

  /**
   * makes sure that each game for each player-variant
   * has a match document that may be joined (match.status = 'waiting')
   * runs: every 1 minute
   */
  @Cron('* * * * *')
  async gameStarter() {
    this.logger.log('Cron: gameStarter Start');
    await gameStarter(this.matchService, this.gameService);
    this.logger.log('Cron: gameStarter Done');
  }

  /**
   * verifies deposits made, ensures they're valid,
   * and also monitors the status progression of the transaction
   * runs: every 1 minute
   */
  @Cron('* * * * *')
  async coinbaseDepositVerifier() {
    this.logger.log('Cron: coinbaseDepositVerifier Start');
    await coinbaseDepositVerifier(this.bankService, this.transactionService);
    this.logger.log('Cron: coinbaseDepositVerifier Done');
  }

  /**
   * adds bots to matches to improve perceived platform engagement
   * runs: every 2 minutes
   */
  @Cron('*/2 * * * *')
  async addBotToMatches() {
    this.logger.log('Cron: addBotToMatches Start');
    await addBotToMatches(this.matchService, this.userService);
    this.logger.log('Cron: addBotToMatches Done');
  }

  /**
   * creates a new bot to improve randomness of addBotToMatches
   * runs: every 10 minutes
   */
  @Cron('*/10 * * * *')
  async botCreator() {
    this.logger.log('Cron: botCreator Start');
    await botCreator(this.userService);
    this.logger.log('Cron: botCreator Done');
  }

  /**
   * processes / sends mail jobs in our queue
   * runs: every 1 minute
   */
  @Cron('* * * * *')
  async mailQueueSender() {
    this.logger.log('Cron: mailQueueSender Start');
    await mailQueueSender(this.mailService);
    this.logger.log('Cron: mailQueueSender Done');
  }

  /**
   * TODO: determine if we can get notifications
   * which may be useful for our platform
   * for example:
   * - when a transaction completes (vs polling Coinbase)
   */
  // @Cron('*/10 * * * * *')
  async coinbaseNotifications() {
    await coinbaseGetNotifications();
  }

  // for testing. Do NOT keep uncommented

  // @Cron('*/30 * * * * *')
  async dataDeleter() {
    await Promise.all(
      (
        await this.matchService.findAll()
      ).map((match) => this.matchService.remove(match.id))
    );
  }

  // @Cron('* * * * *')
  // async meBroadcastTest() {
  //   // this.logger.log('Cron: meBroadcastTest Start');
  //   await meBroadcastTest(this.bankService, this.meGateway, this.userService);
  //   // this.logger.log('Cron: meBroadcastTest Done');
  // }
}
