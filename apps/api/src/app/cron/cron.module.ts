import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  Bank,
  Coin,
  Game,
  MailQueue,
  Match,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { CronService } from './cron.service';
import { MeGateway } from '../me/me.gateway';
import { BankModule } from '../bank/bank.module';
import { CoinModule } from '../coin/coin.module';
import { GameModule } from '../game/game.module';
import { MatchModule } from '../match/match.module';
import { TransactionModule } from '../transaction/transaction.module';
import { UserModule } from '../user/user.module';
import { AuthModule } from '../auth/auth.module';
import { MeModule } from '../me/me.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Bank,
      Coin,
      Game,
      MailQueue,
      Match,
      Player,
      Strike,
      Transaction,
      User,
      UserReferral,
    ]),

    AuthModule,
    BankModule,
    CoinModule,
    GameModule,
    MatchModule,
    MailModule,
    MeModule,
    TransactionModule,
    UserModule,
  ],
  controllers: [],
  providers: [CronService],
  exports: [TypeOrmModule, CronService],
})
export class CronModule {}
