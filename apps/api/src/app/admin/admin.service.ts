import { Injectable } from '@nestjs/common';
import { AdminDashboardResponse } from '@mynx/types';
import { UserService } from '../user/user.service';
import { GameService } from '../game/game.service';
import { CoinService } from '../coin/coin.service';
import { TransactionService } from '../transaction/transaction.service';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
  UserType,
} from '@mynx/types-shared';
import { paginate } from 'nestjs-typeorm-paginate';
import { Transaction, User } from '@mynx/data';
import { keys } from 'lodash';
import { getRepository } from 'typeorm';

@Injectable()
export class AdminService {
  constructor(
    private coinService: CoinService,
    private gameService: GameService,
    private transactionService: TransactionService,
    private userService: UserService
  ) {}

  async dashboard(): Promise<AdminDashboardResponse> {
    return {
      coins: await this.coinService.count(),
      games: await this.gameService.count(),
      users: await this.userService.count(),
      winners: 0,
    };
  }

  async table(tableName: string) {
    let response;
    switch (tableName) {
      case 'coin':
        response = await this.coinService.findAll();
        break;
    }

    return response;
  }

  createCoin(body) {
    return this.coinService.create(body);
  }

  /**
   * retrives the list of processing transactions
   */
  async cashouts(paginationOptions) {
    const repo = this.transactionService.repository();
    const q = repo.createQueryBuilder('transaction');

    const transactionParams: Partial<Transaction> = {
      activityType: TransactionActivityType.Withdrawal,
      status: TransactionStatus.Processing,
    };

    const userParams: Partial<User> = {
      isB: false,
      type: UserType.Normal,
    };

    q.leftJoinAndSelect('transaction.coin', 'coin');
    q.leftJoinAndSelect('transaction.user', 'user');

    keys(transactionParams).forEach((paramKey) => {
      q.where(`transaction.${paramKey}`, transactionParams[paramKey]);
    });

    keys(userParams).forEach((paramKey) => {
      q.where(`transaction.${paramKey}`, userParams[paramKey]);
    });

    q.orderBy('transaction.createdAt', 'ASC');
    return paginate<Transaction>(q, paginationOptions);
  }

  async userStats(id: number) {
    const user = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.plays', 'plays')
      .leftJoinAndSelect('user.transactions', 'transactions')
      .leftJoinAndSelect('transactions.coin', 'coin')
      .leftJoinAndSelect('user.referrals', 'referrals')
      .where('user.id = :id', {
        id,
      })
      .andWhere('user.isBanned = :isBanned', {
        isBanned: false,
      })
      .getOne();

    const sameIP = await this.userService.findAllWithParams({
      ip: user.ip,
    });

    const samePass = await this.userService.findAllWithParams({
      password: user.password,
    });

    const plays = user?.plays || [];
    const referrals = user?.referrals || [];
    const withdrawals = (user?.transactions || []).filter(
      (t) => t.activityType === TransactionActivityType.Withdrawal
    );
    const deposits = (user?.transactions || []).filter(
      (t) => t.activityType === TransactionActivityType.Deposit
    );
    const faucets = (user?.transactions || []).filter(
      (t) => t.activityType === TransactionActivityType.Faucet
    );

    return {
      sameIP,
      samePass,
      plays,
      withdrawals,
      deposits,
      faucets,
      referrals,
    };
  }
}
