import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Query,
  DefaultValuePipe,
  ParseIntPipe,
} from '@nestjs/common';
import { Roles } from '../auth/decorators/roles.decorator';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { RolesGuard } from '../auth/guards/roles.guard';
import { CoinCreateValidation } from '../pipes/coin/create';
import { AdminService } from './admin.service';

@Controller('admin')
export class AdminController {
  constructor(private readonly service: AdminService) {}

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('dashboard')
  dashboard() {
    return this.service.dashboard();
  }

  /**
   * read
   * @param tableName
   * @returns
   */
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('table/:tableName')
  table(@Param('tableName') tableName: string) {
    return this.service.table(tableName);
  }

  /**
   * create
   * @param tableName
   * @returns
   */
  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @UsePipes(CoinCreateValidation)
  @Post('table/coin')
  createCoin(
    @Param('tableName') tableName: string,
    @Body(ValidationPipe) body
  ) {
    return this.service.createCoin(body);
  }

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('cashouts')
  cashouts(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number
  ) {
    return this.service.cashouts({
      page,
      limit,
    });
  }

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get('user-stats/:userId')
  userStats(@Param('userId') userId: string) {
    return this.service.userStats(Number(userId));
  }
}
