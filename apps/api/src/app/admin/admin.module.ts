import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Match } from '@mynx/data';
import { AdminController } from './admin.controller';
import { CoinModule } from '../coin/coin.module';
import { GameModule } from '../game/game.module';
import { TransactionModule } from '../transaction/transaction.module';
import { UserModule } from '../user/user.module';
import { AdminService } from './admin.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Match]),
    CoinModule,
    GameModule,
    TransactionModule,
    UserModule,
  ],
  controllers: [AdminController],
  providers: [AdminService],
  exports: [TypeOrmModule, AdminService],
})
export class AdminModule {}
