import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserReferral } from '@mynx/data';
import { UserReferralService } from './user-referral.service';
import { UserModule } from '../user/user.module';

@Module({
  imports: [TypeOrmModule.forFeature([UserReferral]), UserModule],
  controllers: [],
  providers: [UserReferralService],
  exports: [TypeOrmModule, UserReferralService],
})
export class UserReferralModule {}
