import { BadRequestException, Injectable, OnModuleInit } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User, UserReferral } from '@mynx/data';
import { getRepository, Repository } from 'typeorm';
import { UserService } from '../user/user.service';
import * as Canvas from 'canvas';
import * as fs from 'fs';
import * as path from 'path';
import { app } from '@mynx/environment';
import { ModuleRef } from '@nestjs/core';

const defaultRelations = ['sender', 'receiver'];

@Injectable()
export class UserReferralService implements OnModuleInit {
  private userService: UserService;

  constructor(
    @InjectRepository(UserReferral)
    private readonly repo: Repository<UserReferral>,

    private readonly moduleRef: ModuleRef
  ) {}

  onModuleInit() {
    this.userService = this.moduleRef.get(UserService, { strict: false });
  }

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(params: Partial<UserReferral>) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<UserReferral>) {
    return await this.repo.findOne(params, {
      relations: [],
    });
  }

  create(body: Partial<UserReferral>) {
    return this.repo.save(body);
  }

  update(id: number, body: Partial<UserReferral>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  getRepo() {
    return this.repo;
  }

  // custom methods

  async createUserReferral(senderId: number, receiverId: number) {
    return await this.create({
      senderId,
      receiverId,
    });
  }

  /**
   * find players that have been referred but has not been rewarded yet
   * @param userIds
   */
  async rewardReferralIfValid(userIds: number[]) {
    const unRewardedReferrals = await getRepository(UserReferral)
      .createQueryBuilder('uR')
      .leftJoinAndSelect('uR.receiver', 'receiver')
      .leftJoinAndSelect('uR.sender', 'sender')
      .where('uR.receiverId IN (:...receiverIds)', {
        receiverIds: userIds,
      })
      .andWhere('uR.isRewarded = :isRewarded', {
        isRewarded: false,
      })
      .getMany();

    for (const userReferral of unRewardedReferrals) {
      await this.userService.reward(userReferral.senderId, 'referral');
    }
  }

  generateReferralLink(user: User): string {
    return `${app.secureApiBaseUrl}/join/${user.username}`;
  }

  /**
   * returns user file name
   * @param user
   * @returns
   */
  getReferralBannerFilename(user: User): string {
    return `${user.username}.png`;
  }

  getReferralBannerPath(user: User): string {
    return path.join(
      __dirname,
      `/../../assets/banners/${user.referralBannerAdFileName}`
    );
  }

  async generateReferralBanner(user: User): Promise<void> {
    const adPath = __dirname + '/../../assets/ad/ad_user_2.png';
    const fontPath = __dirname + '/../../assets/fonts/Adumu.ttf';

    Canvas.registerFont(fontPath, { family: 'AdFont' });

    const result = fs.existsSync(adPath);

    if (!result) {
      console.warn('file does not exist', adPath);
      return;
    }

    const canvas = Canvas.createCanvas(800, 600);
    const ctx = canvas.getContext('2d');

    const Image = Canvas.Image;
    const img = new Image();
    img.src = adPath;

    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

    ctx.fillStyle = 'white';
    ctx.textAlign = 'center';

    ctx.font = '28px "AdFont"';
    ctx.fillText(this.generateReferralLink(user), 400, 275);

    canvas
      .createPNGStream()
      .pipe(fs.createWriteStream(this.getReferralBannerPath(user)));
  }
}
