import {
  BadRequestException,
  Injectable,
  Logger,
  OnModuleInit,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bank, Coin, Transaction, User } from '@mynx/data';
import { Repository } from 'typeorm';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import BigNumber from 'bignumber.js';
import { TransactionService } from '../transaction/transaction.service';
import { getDateNow, userBroadcaster } from '@mynx/nest-utils';
import { CoinService } from '../coin/coin.service';
import { UserService } from '../user/user.service';
import { MeGateway } from '../me/me.gateway';
import { canRequestWithdrawal, getUserSocketEventId } from '@mynx/shared-utils';
import { ModuleRef } from '@nestjs/core';
import { MailService } from '../mail/mail.service';

const defaultRelations = ['coin'];
const logger = new Logger('BankService');

@Injectable()
export class BankService implements OnModuleInit {
  private userService: UserService;
  private meGateway: MeGateway;

  constructor(
    @InjectRepository(Bank)
    private readonly repo: Repository<Bank>,

    private readonly moduleRef: ModuleRef,

    private coinService: CoinService,
    private mailService: MailService,
    private transactionService: TransactionService
  ) {}

  onModuleInit() {
    this.userService = this.moduleRef.get(UserService, { strict: false });
    this.meGateway = this.moduleRef.get(MeGateway, { strict: false });
  }

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<Bank>) {
    return await this.repo.findOne(params, {
      relations: defaultRelations,
    });
  }

  async findAllWithParams(params: Partial<Bank>) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
    });
  }

  create(body: Partial<Bank>) {
    return this.repo.save(body);
  }

  update(id: number, body: Partial<Bank>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  // custom methods

  /**
   * updates a bank document, logs it as a transaction, and emits a socket event for real time updates
   * @param id
   * @param type
   * @param activityType
   * @param amount
   * @param status
   * @returns
   */
  async updateAndLog(
    userId: number,
    coinId: number,
    type: TransactionType,
    activityType: TransactionActivityType,
    amount: BigNumber,
    status: TransactionStatus = TransactionStatus.Completed,
    notes?: string
  ): Promise<void> {
    let bank = await this.findOneWithParams({
      userId,
      coinId,
    });

    if (!bank) {
      bank = await this.open(userId, coinId);
      return;
    }

    logger.log(
      `updateAndLog: bank for coinId ${coinId} current amount ${bank.amount}`
    );

    const newAmount = BigNumber.sum(...[bank.amount, amount], 0);

    await this.update(bank.id, {
      amount: newAmount,
    });

    const transactionRaw: Transaction = await this.transactionService.create({
      userId: bank.userId,
      coinId: bank.coinId,
      type,
      activityType,
      status,
      amount,
      completedAt: getDateNow(),
      notes,
    });

    logger.log(
      `updateAndLog: userId ${userId} coinId ${coinId} type ${type} activityType ${activityType} amount ${amount} status ${status} newAmount ${newAmount}`
    );

    const transaction = await this.transactionService.findOne(
      transactionRaw.id
    );
    await this.mailService.walletUpdate(transaction);

    const user = await this.userService?.findOne(userId);

    if (user) {
      userBroadcaster(this.meGateway, user, getUserSocketEventId(user), user);
    }
  }

  async openAll(userId: number, amount: BigNumber = new BigNumber(0)) {
    const banks = await this.findByUserId(userId);

    const coins: Coin[] = await this.coinService.actives();

    const noAccountCoins: Coin[] = coins.filter(
      (c) => !banks.map((b) => b.coinId).includes(c.id)
    );

    await Promise.all(
      noAccountCoins.map((c) => this.open(userId, c.id, amount))
    );
  }

  async open(
    userId: number,
    coinId: number,
    amount: BigNumber = new BigNumber(0)
  ) {
    const bank = await this.create({
      coinId,
      userId,
      amount,
    });

    if (amount.isGreaterThan(0)) {
      await this.updateAndLog(
        bank.userId,
        bank.coinId,
        TransactionType.In,
        TransactionActivityType.Open,
        bank.amount,
        TransactionStatus.Completed
      );
    }

    return bank;
  }

  async findByUserId(userId: number) {
    return await this.findAllWithParams({
      userId,
    });
  }

  async favorites(userId: number) {
    return await this.findAllWithParams({
      userId,
      isFavorite: true,
    });
  }

  /**
   * allows user to request for a withdrawal from his account
   * @param id
   * @param user
   * @returns
   */
  async withdraw(id: number, user: User) {
    const bank = await this.findOne(id);

    if (!bank) {
      throw new BadRequestException('Bank not found');
    }

    if (!canRequestWithdrawal(user, bank)) {
      throw new BadRequestException(
        `Bank amount is less than the minimum withdrawal amount limit for ${bank.coin.name}`
      );
    }

    // deduct amount from user's bank
    await this.updateAndLog(
      user.id,
      bank.coinId,
      TransactionType.Out,
      TransactionActivityType.Withdrawal,
      bank.amount.multipliedBy(-1),
      TransactionStatus.Processing
    );

    return await this.findOne(id);
  }
}
