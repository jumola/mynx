import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  Bank,
  Coin,
  MailQueue,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { BankService } from './bank.service';
import { CoinModule } from '../coin/coin.module';
import { TransactionModule } from '../transaction/transaction.module';
import { AuthModule } from '../auth/auth.module';
import { MailModule } from '../mail/mail.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Bank,
      Coin,
      MailQueue,
      Strike,
      Transaction,
      User,
      UserReferral,
    ]),

    AuthModule,
    CoinModule,
    MailModule,
    TransactionModule,
  ],
  controllers: [],
  providers: [BankService],
  exports: [TypeOrmModule, BankService],
})
export class BankModule {}
