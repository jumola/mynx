import { MailQueue, Transaction, User, UserVerification } from '@mynx/data';
import { app } from '@mynx/environment';
import {
  MailQueueStatus,
  MailQueueType,
  UserVerificationType,
} from '@mynx/types-shared';
import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { InjectRepository } from '@nestjs/typeorm';
import { FindManyOptions, Repository } from 'typeorm';
import { UserService } from '../user/user.service';

@Injectable()
export class MailService implements OnModuleInit {
  private userService: UserService;

  constructor(
    @InjectRepository(MailQueue)
    private readonly repo: Repository<MailQueue>,
    private mailerService: MailerService,

    private readonly moduleRef: ModuleRef
  ) {}

  onModuleInit() {
    this.userService = this.moduleRef.get(UserService, { strict: false });
  }

  async findAllWithParams(
    params: Partial<MailQueue>,
    options: FindManyOptions<MailQueue> = {}
  ): Promise<MailQueue[]> {
    return await this.repo.find({
      where: params,
      ...options,
    });
  }

  async findOneWithParams(
    params: Partial<MailQueue>,
    options: FindManyOptions<MailQueue> = {}
  ) {
    return await this.repo.findOne(params, options);
  }

  update(id: number, body: Partial<MailQueue>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  private queue(params: Partial<MailQueue>) {
    params.template = this.getTemplateByType(params.type);
    return this.repo.save(params);
  }

  async send(mailQeue: MailQueue): Promise<void> {
    await this.mailerService.sendMail(mailQeue);
    await this.update(mailQeue.id, {
      status: MailQueueStatus.Sent,
      notes: 'Ok',
    });
  }

  accountVerified(user: User): Promise<MailQueue> {
    const subject = `Congratulations, ${user.firstName}! Account successfully verified!`;
    const type = MailQueueType.AccountVerified;
    return this.queue({
      type,
      to: user.email,
      subject,
      context: {
        user,
      },
    });
  }

  passwordReset(
    user: User,
    userVerification: UserVerification
  ): Promise<MailQueue> {
    const subject = `Recovery link for your password`;
    const type = MailQueueType.PasswordReset;

    const link =
      this.userService.generateUserVerificationLink(userVerification);

    return this.queue({
      type,
      to: user.email,
      subject,
      context: {
        user,
        userVerification,
        link,
      },
    });
  }

  sendNewPassword(user: User, password: string) {
    return this.queue({
      type: MailQueueType.NewPassword,
      to: user.email,
      subject: 'Your temporary password',
      context: {
        user,
        password,
      },
    });
  }

  async welcome(user: User): Promise<MailQueue> {
    const subject = 'Welcome email here';
    const type = MailQueueType.Welcome;

    const userVerification: UserVerification =
      await this.userService.createUserVerification(
        user.id,
        UserVerificationType.AccountVerification
      );

    const link =
      this.userService.generateUserVerificationLink(userVerification);

    return await this.queue({
      type,
      to: user.email,
      subject,
      context: {
        user,
        link,
      },
    });
  }

  walletUpdate(transaction: Transaction): Promise<MailQueue> {
    const subject = 'Transaction receipt';
    const type = MailQueueType.WalletUpdate;
    return this.queue({
      type,
      to: transaction.user.email,
      subject,
      context: {
        transaction,
      },
    });
  }

  private getTemplateByType(type: MailQueueType) {
    return `./${type}`;
  }
}
