import { Module } from '@nestjs/common';
import { MailService } from './mail.service';
import { AppMailerModule } from '../shared/mailer-module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailQueue, User, UserVerification } from '@mynx/data';
import { UserModule } from '../user/user.module';

@Module({
  imports: [
    AppMailerModule,
    TypeOrmModule.forFeature([MailQueue, User, UserVerification]),
    UserModule,
  ],
  providers: [MailService],
  exports: [MailService],
})
export class MailModule {}
