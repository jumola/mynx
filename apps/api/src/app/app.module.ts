import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Connection } from 'typeorm';
import { CoinModule } from './coin/coin.module';
import { getConnectionOptions } from 'typeorm';
import { GameModule } from './game/game.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { MeModule } from './me/me.module';
import { AdminModule } from './admin/admin.module';
import { MatchModule } from './match/match.module';
import { TransactionModule } from './transaction/transaction.module';
import { CronModule } from './cron/cron.module';
import { ScheduleModule } from '@nestjs/schedule';
import { StrikeModule } from './strike/strike.module';
import { JoinModule } from './join/join.module';
import { BankModule } from './bank/bank.module';
import { HookModule } from './hook/hook.module';
import { PlayerModule } from './player/player.module';
import { UserReferralModule } from './user-referral/user-referral.module';
import { MailModule } from './mail/mail.module';
import { AppMailerModule } from './shared/mailer-module';

@Module({
  imports: [
    AdminModule,
    AuthModule,
    BankModule,
    CoinModule,
    CronModule,
    GameModule,
    HookModule,
    JoinModule,
    MailModule,
    MatchModule,
    MeModule,
    PlayerModule,
    StrikeModule,
    TransactionModule,
    UserModule,
    UserReferralModule,

    TypeOrmModule.forRootAsync({
      useFactory: async () =>
        Object.assign(await getConnectionOptions(), {
          autoLoadEntities: true,
        }),
    }),

    ScheduleModule.forRoot(),
    AppMailerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(public connection: Connection) {}
}
