import { User } from '@mynx/data';
import { Logger } from '@nestjs/common';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { GameService } from './game.service';
import { getMatchSocketEventId } from '@mynx/shared-utils';
import { MatchService } from '../match/match.service';

const namespace = 'game';

@WebSocketGateway({ namespace })
export class GameGateway implements OnGatewayConnection, OnGatewayDisconnect {
  private logger: Logger = new Logger('GameGateway');

  constructor(
    private readonly authService: AuthService,
    private readonly gameService: GameService,
    private readonly matchService: MatchService
  ) {}

  @WebSocketServer() public server: Server;

  public connectedUsers = new Map();

  async handleConnection(socket: Socket, ...args: any[]) {
    this.logger.log('Gamer connected');
    try {
      const user: User = await this.authService.getUserFromHandshake(socket);
      this.addClientToMap({
        user,
        socket,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async handleDisconnect(socket) {
    this.logger.log('Gamer disconnected');
    try {
      const user: User = await this.authService.getUserFromHandshake(socket);
      this.removeClientFromMap({
        user,
        socket,
      });
    } catch (error) {
      console.log(error);
    }
  }

  addClientToMap({ user, socket }: { user: User; socket: Socket }) {
    const userId: string = user?.id?.toString();
    const socketId: string = socket?.id;
    if (!this.connectedUsers.has(userId)) {
      this.connectedUsers.set(userId, new Set([socketId]));
    } else {
      this.connectedUsers.get(userId).add(socketId);
    }
    // this.emitOnlineUsers();
  }

  removeClientFromMap({ user, socket }: { user: User; socket: Socket }) {
    const userId: string = user?.id?.toString();
    const socketId: string = socket?.id;
    if (this.connectedUsers.has(userId)) {
      const userSocketIdSet = this.connectedUsers.get(userId);
      userSocketIdSet.delete(socketId);
      if (userSocketIdSet.size === 0) {
        this.connectedUsers.delete(userId);
      }
    }
    // this.emitOnlineUsers();
  }

  @SubscribeMessage('join')
  async join(
    @ConnectedSocket() client: Socket,
    @MessageBody() data: { id: number }
  ) {
    try {
      const user: User = await this.authService.getUserFromHandshake(client);
      if (user) {
        const { id } = data;
        const game = await this.gameService.findOne(id);

        const match = await this.gameService.join(game.id, user);
        if (!match) {
          throw new Error('An error occurred. Please try again later.');
        }

        this.server.emit(getMatchSocketEventId(match), {
          payload: match,
        });

        return match.id;
      }
    } catch (err) {
      this.server.emit('error', err.message);
    }
  }
}
