import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Bank, Game, Match, User } from '@mynx/data';
import { FindManyOptions, Repository } from 'typeorm';
import { MatchService } from '../match/match.service';
import { BankService } from '../bank/bank.service';
import { TransactionActivityType, TransactionType } from '@mynx/types-shared';
import { canJoinGame } from '@mynx/shared-utils';
import { isString } from 'lodash';

const defaultRelations = ['coin'];
@Injectable()
export class GameService {
  constructor(
    @InjectRepository(Game)
    private readonly repo: Repository<Game>,
    private matchService: MatchService,
    private bankService: BankService
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find({
      relations: defaultRelations,
    });
  }

  async findAllWithParams(
    params: Partial<Game>,
    options: FindManyOptions<Game> = {}
  ) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
      ...options,
    });
  }

  async findOneWithParams(params: Partial<Game>) {
    return await this.repo.findOne(params, {
      relations: defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: defaultRelations,
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body: Partial<Game>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  // custom methods

  async all(userId?: number) {
    return await this.findAll();
  }

  /**
   * joins an available game match
   * @param id game id
   * @param user user doc
   */
  async join(id: number, user: User) {
    const game = await this.findOne(id);

    let bank = await this.bankService.findOneWithParams({
      coinId: game.coinId,
      userId: user.id,
    });

    if (!bank) {
      bank = await this.bankService.open(user.id, game.coinId);
    }

    const matchDoc = await this.matchService.getAvailableMatch(id);

    const match = await this.joinMatch(matchDoc, user, game, bank);

    // deduct amount from user's bank
    await this.bankService.updateAndLog(
      user.id,
      game.coinId,
      TransactionType.Out,
      TransactionActivityType.BuyIn,
      match.game.costAmount.multipliedBy(-1)
    );

    return match;
  }

  async joinMatch(
    match: Match,
    user: User,
    game: Game,
    bank: Bank
  ): Promise<Match> {
    const response = canJoinGame(user, game, bank, match);
    if (isString(response)) {
      throw new BadRequestException(response);
    }

    return await this.matchService.addPlayer(match, user);
  }
}
