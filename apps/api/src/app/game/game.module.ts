import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { GameController } from './game.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  Bank,
  Coin,
  Game,
  Match,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { GameGateway } from './game.gateway';
import { MatchModule } from '../match/match.module';
import { BankModule } from '../bank/bank.module';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Bank,
      Coin,
      Game,
      Match,
      Player,
      Strike,
      Transaction,
      User,
      UserReferral,
    ]),

    AuthModule,
    BankModule,
    MatchModule,
  ],
  controllers: [GameController],
  providers: [GameService, GameGateway],
  exports: [TypeOrmModule, GameService, GameGateway],
})
export class GameModule {}
