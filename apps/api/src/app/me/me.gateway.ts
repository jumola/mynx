import { User } from '@mynx/data';
import { Logger } from '@nestjs/common';
import {
  ConnectedSocket,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Socket, Server } from 'socket.io';
import { AuthService } from '../auth/auth.service';
import { getUserSocketEventId } from '@mynx/shared-utils';
import { UserService } from '../user/user.service';

const namespace = 'me';

@WebSocketGateway({ namespace })
export class MeGateway implements OnGatewayConnection, OnGatewayDisconnect {
  // private logger: Logger = new Logger('MeGateWay');

  constructor(private readonly authService: AuthService) {}

  @WebSocketServer() public server: Server;

  public connectedUsers = new Map();

  async handleConnection(socket: Socket, ...args: any[]) {
    // this.logger.log('User connected');
    try {
      const user: User = await this.authService.getUserFromHandshake(socket);
      this.addClientToMap({
        user,
        socket,
      });
    } catch (error) {
      console.log(error);
    }
  }

  async handleDisconnect(socket) {
    // this.logger.log('Gamer disconnected');
    try {
      const user: User = await this.authService.getUserFromHandshake(socket);
      this.removeClientFromMap({
        user,
        socket,
      });
    } catch (error) {
      console.log(error);
    }
  }

  addClientToMap({ user, socket }: { user: User; socket: Socket }) {
    const userId: string = user?.id?.toString();
    const socketId: string = socket?.id;
    if (!this.connectedUsers.has(userId)) {
      this.connectedUsers.set(userId, new Set([socketId]));
    } else {
      this.connectedUsers.get(userId).add(socketId);
    }
    // this.emitOnlineUsers();
  }

  removeClientFromMap({ user, socket }: { user: User; socket: Socket }) {
    const userId: string = user?.id?.toString();
    const socketId: string = socket?.id;
    if (this.connectedUsers.has(userId)) {
      const userSocketIdSet = this.connectedUsers.get(userId);
      userSocketIdSet.delete(socketId);
      if (userSocketIdSet.size === 0) {
        this.connectedUsers.delete(userId);
      }
    }
    // this.emitOnlineUsers();
  }

  /**
   * broadcasts latest user object updates to connected user
   * @param client
   * @returns
   */
  @SubscribeMessage('data')
  async connect(@ConnectedSocket() client: Socket): Promise<void> {
    try {
      const user: User = await this.authService.getUserFromHandshake(client);
      if (user) {
        this.server.emit(getUserSocketEventId(user), {
          payload: user,
        });
      }
    } catch (err) {
      this.server.emit('error', err.message);
    }
  }
}
