import {
  Controller,
  DefaultValuePipe,
  Get,
  Req,
  UseGuards,
  ParseIntPipe,
  Query,
  Param,
  Post,
} from '@nestjs/common';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { TransactionService } from '../transaction/transaction.service';
import { Pagination } from 'nestjs-typeorm-paginate';
import { Transaction } from '@mynx/data';
import { BankService } from '../bank/bank.service';
import { MeService } from './me.service';

@Controller('me')
export class MeController {
  constructor(
    private readonly bankService: BankService,
    private readonly meService: MeService,
    private readonly transactionService: TransactionService
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  get(@Req() req) {
    return this.meService.get(req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('transactions')
  transactions(
    @Req() req,
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number
  ): Promise<Pagination<Transaction>> {
    return this.transactionService.paginateWithParams(
      {
        userId: req.user.id,
      },
      {
        page,
        limit,
      }
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('withdraw/:bankId')
  withdraw(@Param('bankId', ParseIntPipe) bankId, @Req() req) {
    return this.bankService.withdraw(+bankId, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('deposit-address/:coinId')
  depositAddresses(@Param('coinId', ParseIntPipe) coinId: number, @Req() req) {
    return this.transactionService.getOrCreateDepositAddresses(
      req.user.id,
      coinId
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('claim-welcome-bonus')
  claimWelcomeBonus(@Req() req) {
    return this.meService.claimWelcomeBonusOnSelectedCoins(req);
  }

  @UseGuards(JwtAuthGuard)
  @Post('claim-faucet-bonus')
  claimFaucetBonus(@Req() req) {
    return this.meService.claimFaucetBonus(req);
  }

  @UseGuards(JwtAuthGuard)
  @Post('favorite-bank/:coinId')
  setFavoriteBank(@Req() req, @Param('coinId', ParseIntPipe) coinId: number) {
    return this.meService.setFavoriteBank(req.user.id, Number(coinId));
  }

  @UseGuards(JwtAuthGuard)
  @Get('referrals')
  referrals(@Req() req) {
    return this.meService.getReferrals(req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('plays')
  plays(@Req() req) {
    return this.meService.getPlays(req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('live-matches')
  liveMatches(@Req() req) {
    return this.meService.liveMatches(req.user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('upgrade/:coinId')
  upgrade(
    @Req() req,
    @Param('coinId', ParseIntPipe) coinId: number
  ): Promise<void> {
    return this.meService.upgrade(req.user.id, coinId);
  }
}
