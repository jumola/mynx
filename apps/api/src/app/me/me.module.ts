import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  Bank,
  Coin,
  Match,
  Player,
  Strike,
  Transaction,
  User,
  UserReferral,
} from '@mynx/data';
import { MeController } from './me.controller';
import { MeGateway } from './me.gateway';
import { MeService } from './me.service';
import { BankModule } from '../bank/bank.module';
import { CoinModule } from '../coin/coin.module';
import { StrikeModule } from '../strike/strike.module';
import { AuthModule } from '../auth/auth.module';
import { TransactionModule } from '../transaction/transaction.module';
import { UserModule } from '../user/user.module';
import { UserReferralModule } from '../user-referral/user-referral.module';
import { MatchModule } from '../match/match.module';
import { PlayerModule } from '../player/player.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Bank,
      Coin,
      Match,
      Player,
      Strike,
      Transaction,
      User,
      UserReferral,
    ]),

    AuthModule,
    BankModule,
    CoinModule,
    MatchModule,
    PlayerModule,
    StrikeModule,
    TransactionModule,
    UserModule,
    UserReferralModule,
  ],
  controllers: [MeController],
  providers: [MeService, MeGateway],
  exports: [TypeOrmModule, MeService, MeGateway],
})
export class MeModule {}
