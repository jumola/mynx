import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coin, Match, Transaction, User } from '@mynx/data';
import { Repository } from 'typeorm';
import { StrikeService } from '../strike/strike.service';
import { BankService } from '../bank/bank.service';
import { CoinService } from '../coin/coin.service';
import {
  MatchStatus,
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import { appMasterConfig } from '@mynx/environment';
import { UserReferralService } from '../user-referral/user-referral.service';
import { MatchService } from '../match/match.service';
import { PlayerService } from '../player/player.service';
import { UserService } from '../user/user.service';
import { ceil, isNil, uniqBy } from 'lodash';
import { TransactionService } from '../transaction/transaction.service';
import { timeSince } from '@mynx/shared-utils';

@Injectable()
export class MeService {
  defaultRelations = ['banks', 'banks.coin'];

  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,
    private bankService: BankService,
    private coinService: CoinService,
    private matchService: MatchService,
    private playerService: PlayerService,
    private strikeService: StrikeService,
    private transactionService: TransactionService,
    private userService: UserService,
    private userReferralService: UserReferralService
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(params: Partial<User>) {
    return await this.repo.find({
      where: params,
      relations: this.defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: this.defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<User>) {
    return await this.repo.findOne(params, {
      relations: this.defaultRelations,
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body: Partial<User>) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  /**
   * rewards the user of the welcome bonus which are in the isFeatured coins
   * @param req
   */
  async claimWelcomeBonusOnSelectedCoins(req): Promise<void> {
    const user = await this.findOne(req.user.id);

    // make sure that the user has
    // - verified his email address
    // - has not received a welcome bonus yet

    if (!user?.isEmailVerified || user?.isGifted) {
      if (!user?.isEmailVerified) {
        await this.strikeService.recordStrike(
          req,
          'claiming welcome bonus with unverified email'
        );
        throw new BadRequestException(
          `You must verify your email address first to claim your welcome bonus.`
        );
      }

      if (user?.isGifted) {
        await this.strikeService.recordStrike(req, 'claiming multiple gifts');
        throw new BadRequestException(
          `You may only claim your welcome gift once!`
        );
      }
    }

    await this.userService.reward(user.id, 'welcome-bonus');
  }

  /**
   * lets the user to claim a faucet bonus at certain intervals depending
   * on user role
   * @param req
   */
  async claimFaucetBonus(req: any): Promise<void> {
    const user = req.user as User;
    const claimIntervalSeconds: number = user.isVip
      ? appMasterConfig.claimFaucetBonusIntervalVip
      : appMasterConfig.claimFaucetBonusInterval;

    // find last faucet claim
    const lastFaucetTransaction: Transaction =
      await this.transactionService.findOneWithParams(
        {
          activityType: TransactionActivityType.Faucet,
          userId: user.id,
          status: TransactionStatus.Completed,
        },
        {
          order: {
            createdAt: 'DESC',
          },
        }
      );

    // check if can faucet
    const secondsAgo: number = !isNil(lastFaucetTransaction)
      ? timeSince(lastFaucetTransaction.createdAt, 'seconds')?.value ??
        claimIntervalSeconds
      : claimIntervalSeconds;

    if (secondsAgo >= claimIntervalSeconds) {
      // can faucet
      await this.userService.reward(user.id, 'faucet');
    } else {
      await this.strikeService.recordStrike(
        req,
        'claiming faucet bonus out of interval'
      );

      throw new BadRequestException(
        `You can claim your next faucet bonus in ${ceil(
          (claimIntervalSeconds - secondsAgo) / 60
        )} minutes.`
      );
    }
  }

  async get(userId: number) {
    await this.bankService.openAll(userId);
    const user = await this.findOne(userId);

    if (!user?.image) {
      user.image = this.userService.generateAvatar(user);
      await this.update(user.id, {
        image: user.image,
      });
    }

    return user;
  }

  async setFavoriteBank(userId: number, coinId: number) {
    const user = await this.findOne(userId);

    const favorites = user.banks.filter((b) => b.isFavorite);

    // if can add a new favorite
    const bank = user.banks.find((b) => b.coinId === coinId);

    if (!bank) {
      throw new BadRequestException('Wallet not found.');
    }

    if (
      favorites.length === appMasterConfig.welcomeBonusCoinCount &&
      !bank.isFavorite
    ) {
      throw new BadRequestException(
        `You may only set a max of ${appMasterConfig.welcomeBonusCoinCount} favorites.`
      );
    }

    await this.bankService.update(bank.id, {
      isFavorite: !bank.isFavorite,
    });
  }

  /**
   * returns a list of referrals user have + generates the user's custom referral banner image
   * @param userId
   * @returns
   */
  async getReferrals(userId: number) {
    const user = await this.userService.findOne(userId);
    if (!(await this.userReferralService.getReferralBannerPath(user))) {
      await this.userReferralService.generateReferralBanner(user);
    }

    return await this.userReferralService.findAllWithParams({
      senderId: userId,
    });
  }

  async getPlays(userId: number) {
    return await this.playerService.findAllWithParams({
      userId,
    });
  }

  async liveMatches(userId: number): Promise<Match[]> {
    const plays = await this.playerService.repo
      .createQueryBuilder('player')
      .leftJoinAndSelect('player.match', 'match')
      .leftJoinAndSelect('match.game', 'game')
      .leftJoinAndSelect('match.players', 'players')
      .leftJoinAndSelect('match.winner', 'winner')
      .leftJoinAndSelect('game.coin', 'coin')
      .leftJoinAndSelect('players.user', 'user')
      .where('player.userId = :userId', { userId })
      .andWhere('match.status IN (:...status)', {
        status: [
          MatchStatus.Waiting,
          MatchStatus.Concluding,
          MatchStatus.Ongoing,
        ],
      })
      .getMany();
    return uniqBy(
      plays.map((p) => p.match),
      'id'
    );
  }

  async upgrade(userId: number, coinId: number) {
    return await this.userService.upgrade(userId, coinId);
  }
}
