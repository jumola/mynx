import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Coin, Strike, Transaction } from '@mynx/data';
import { TransactionService } from './transaction.service';
import { CoinModule } from '../coin/coin.module';
import { StrikeModule } from '../strike/strike.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Coin, Strike, Transaction]),
    CoinModule,
    StrikeModule,
  ],
  controllers: [],
  providers: [TransactionService],
  exports: [TypeOrmModule, TransactionService],
})
export class TransactionModule {}
