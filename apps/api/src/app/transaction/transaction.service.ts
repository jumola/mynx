import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Coin, Transaction } from '@mynx/data';
import { FindManyOptions, Repository } from 'typeorm';
import {
  IPaginationOptions,
  paginate,
  Pagination,
} from 'nestjs-typeorm-paginate';
import { keys } from 'lodash';
import {
  TransactionActivityType,
  TransactionStatus,
  TransactionType,
} from '@mynx/types-shared';
import { coinbaseCreateAddress } from '@mynx/coinbase';
import { CoinService } from '../coin/coin.service';
import { StrikeService } from '../strike/strike.service';

const defaultRelations = ['coin', 'user'];

@Injectable()
export class TransactionService {
  constructor(
    @InjectRepository(Transaction)
    private readonly repo: Repository<Transaction>,
    private coinService: CoinService,
    private strikeService: StrikeService
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(params: Partial<Transaction>) {
    return await this.repo.find({
      where: params,
      relations: defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: defaultRelations,
    });
  }

  async findOneWithParams(
    params: Partial<Transaction>,
    options: FindManyOptions<Transaction> = {}
  ) {
    return await this.repo.findOne(params, {
      relations: defaultRelations,
      ...options,
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  repository() {
    return this.repo;
  }

  // custom methods
  async findByUserId(userId: number) {
    return await this.findAllWithParams({
      userId,
    });
  }

  async paginateWithParams(
    params: Partial<Transaction> = {},
    options: IPaginationOptions
  ): Promise<Pagination<Transaction>> {
    const q = this.repo.createQueryBuilder('transaction');

    q.leftJoinAndSelect('transaction.coin', 'coin');
    q.leftJoinAndSelect('transaction.user', 'user');

    const paramKeys: string[] = keys(params);
    paramKeys.forEach((paramKey) => {
      q.where(`transaction.${paramKey}`, params[paramKey]);
    });

    q.orderBy('transaction.createdAt', 'DESC');
    return paginate<Transaction>(q, options);
  }

  /**
   * get || create deposit addresses for the user
   * @param userId
   * @param coinId
   * @returns
   */
  async getOrCreateDepositAddresses(userId: number, coinId: number) {
    let transaction: Transaction = await this.findOneWithParams({
      type: TransactionType.In,
      userId,
      status: TransactionStatus.Waiting,
      activityType: TransactionActivityType.Deposit,
      coinId,
    });

    // in case there are user banks without deposit address yet, create
    if (!transaction) {
      const coin = await this.coinService.findOne(coinId);
      transaction = await this.createDepositAddress(userId, coin);
      transaction.coin = coin;
    }

    return transaction;
  }

  private async createDepositAddress(
    userId: number,
    coin: Coin
  ): Promise<Transaction> {
    const { address } = await coinbaseCreateAddress(coin);
    const newDoc = await this.create({
      activityType: TransactionActivityType.Deposit,
      type: TransactionType.In,
      userId: userId,
      coinId: coin.id,
      coinbaseAddress: address,
    });
    return await this.findOne(newDoc.id);
  }
}
