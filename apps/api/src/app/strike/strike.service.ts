import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Strike } from '@mynx/data';
import { getClientIP } from '@mynx/nest-utils';

@Injectable()
export class StrikeService {
  defaultRelations = ['user'];

  constructor(
    @InjectRepository(Strike)
    private readonly repo: Repository<Strike>
  ) {}

  count() {
    return this.repo.count();
  }

  findAll() {
    return this.repo.find();
  }

  async findAllWithParams(params: Partial<Strike>) {
    return await this.repo.find({
      where: params,
      relations: this.defaultRelations,
    });
  }

  findOne(id: number) {
    return this.repo.findOne(id, {
      relations: this.defaultRelations,
    });
  }

  async findOneWithParams(params: Partial<Strike>) {
    return await this.repo.findOne(params, {
      relations: this.defaultRelations,
    });
  }

  create(body) {
    return this.repo.save(body);
  }

  update(id: number, body) {
    return this.repo.update(id, body);
  }

  async remove(id: number): Promise<void> {
    await this.repo.delete(id);
  }

  recordStrike(
    req: any,
    reason:
      | 'idk'
      | 'claiming multiple gifts'
      | 'claiming faucet bonus out of interval'
      | 'claiming welcome bonus with unverified email'
  ) {
    return this.create({
      userId: req.user.id,
      ip: getClientIP(req),
      url: req?.url,
      reason,
    });
  }
}
