import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Strike } from '@mynx/data';
import { StrikeService } from './strike.service';

@Module({
  imports: [TypeOrmModule.forFeature([Strike])],
  controllers: [],
  providers: [StrikeService],
  exports: [TypeOrmModule, StrikeService],
})
export class StrikeModule {}
